<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('tahun_anggaran/tahun_anggaran_model', 'tahun_anggaran_model');
    }

    public function index()
    {
        if ($this->session->userdata('is_logged_in') == true) {
            redirect('dashboard');
        } else {
            $data['tahun'] = $this->tahun_anggaran_model->get(
                array(
                    "order_by" => array(
                        'tahun' => "ASC"
                    )
                )
            );
            $data['header_main'] = $this->load->view("template_home/header");
            $data['content_main'] = $this->load->view("login");
            $data['footer_main'] = $this->load->view("template_home/footer", $data);
        }
    }

    public function act_login()
    {
        $username = $this->input->post('username', true);
        $pass = $this->input->post('password');
        $tahun = decrypt_data($this->input->post('tahun'));
        $status = false;

        $data_user = $this->user_model->get(
            array(
                'where' => array(
                    'username' => $username
                )
            ),
            'row'
        );

        if ($data_user) {
            if (password_verify($pass, $data_user->password)) {
                $data_update = array(
                    "tahun_anggaran_id" => $tahun
                );

                $this->user_model->edit($data_user->id_user, $data_update);

                $label_tahun = $this->tahun_anggaran_model->get_by($tahun);

                $this->session->set_userdata('nama_lengkap', $data_user->nama_lengkap);
                $this->session->set_userdata('level_user_id', $data_user->level_user_id);
                $this->session->set_userdata('master_satker_id', $data_user->master_satker_id);
                $this->session->set_userdata('id_user', $data_user->id_user);
                $this->session->set_userdata('tahun_anggaran_id', $tahun);
                $this->session->set_userdata('tahun', $label_tahun->tahun);
                $this->session->set_userdata('is_logged_in', true);

                $status = true;
            } else {
                $status = false;
            }
        } else {
            $status = false;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function act_logout()
    {
        if (isset($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] === true) {

            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }

            // user logout ok
            redirect('login');
        } else {

            redirect('login');
        }
    }
}
