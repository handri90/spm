<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tracking_spm extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("trx_spm_model");
    }

    public function index()
    {
        $data['header_main'] = $this->load->view("template_home/header");
        $data['content_main'] = $this->load->view("tracking_spm");
        $data['footer_main'] = $this->load->view("template_home/footer");
    }

    public function tracking_spm_check()
    {
        $status = true;
        $no_tracking = $this->input->get("no_tracking", true);

        $data = $this->trx_spm_model->query("
        SELECT spm.`no_spm`,nama_program,nama_kegiatan,nama_sub_kegiatan,spm.`nilai`,spm.`uraian`,
        DATE_FORMAT(spm.`tanggal_spm`,'%d/%m/%Y') AS tanggal_spm,
        DATE_FORMAT(a.`tanggal_jam_penyerahan_spm_masuk`,'%d/%m/%Y') AS tanggal_spm_register_masuk,
        DATE_FORMAT(a.`tanggal_jam_penyerahan_spm_masuk`,'%H:%i:%s') AS jam_spm_register_masuk,
        a.`no_register` AS no_register_masuk,
        a.`catatan`,
        DATE_FORMAT(a.`tanggal_jam_perbendaharaan_spm_masuk`,'%d/%m/%Y') AS tanggal_spm_perbendaharaan,
        DATE_FORMAT(a.`tanggal_jam_perbendaharaan_spm_masuk`,'%H:%i:%s') AS jam_spm_perbendaharaan,
        a.user_perbendaharaan_spm_masuk,
        DATE_FORMAT(b.`tanggal_jam_perbendaharaan_penyelesaian_sp2d`,'%d/%m/%Y') AS tanggal_spm_penyelesaian,
        DATE_FORMAT(b.`tanggal_jam_perbendaharaan_penyelesaian_sp2d`,'%H:%i:%s') AS jam_spm_penyelesaian,
        b.no_sp2d_penyelesaian AS no_sp2d_penyelesaian,
        c.status_spm,nama_sumber_pendanaan,
        DATE_FORMAT(c.tanggal_ditolak,'%Y-%m-%d') AS tanggal_ditolak_custom,
        DATE_FORMAT(c.tanggal_ditolak,'%H:%i:%s') AS jam_ditolak,
        c.jenis_pengembalian,
        DATE_FORMAT(c.tanggal_perbaikan_diterima,'%Y-%m-%d') AS tanggal_perbaikan_diterima,
        DATE_FORMAT(c.tanggal_perbaikan_diterima,'%H:%i:%s') AS jam_perbaikan_diterima
        FROM spm
        JOIN master_satker  ON id_master_satker=spm.master_satker_id
        LEFT JOIN trx_spm a ON id_spm=a.spm_id AND a.user_penyerahan_spm_masuk IS NOT NULL AND a.tanggal_jam_penyerahan_spm_masuk IS NOT NULL AND a.deleted_at IS NULL
        LEFT JOIN trx_spm b ON id_spm=b.spm_id AND b.status_spm = '4' AND b.deleted_at IS NULL
        LEFT JOIN (
            SELECT *
            FROM trx_spm
            WHERE id_trx_spm IN (
            SELECT MAX(id_trx_spm)
            FROM trx_spm
            WHERE trx_spm.deleted_at IS NULL
            GROUP BY spm_id
            ) AND trx_spm.deleted_at IS NULL
        ) AS c ON c.spm_id=id_spm
        INNER JOIN master_sumber_pendanaan ON master_sumber_pendanaan_id=id_master_sumber_pendanaan
        LEFT JOIN referensi_spm ON id_referensi_spm=referensi_spm_id
        LEFT JOIN master_program ON id_master_program=referensi_spm.master_program_id
        LEFT JOIN master_kegiatan ON id_master_kegiatan=referensi_spm.master_kegiatan_id
        LEFT JOIN master_sub_kegiatan ON id_master_sub_kegiatan=referensi_spm.master_sub_kegiatan_id
        WHERE no_spm = '{$no_tracking}' AND spm.deleted_at IS NULL
        ")->row();

        $data->tanggal_ditolak_custom = ($data->tanggal_ditolak_custom ? longdate_indo($data->tanggal_ditolak_custom) : "");
        $data->tanggal_perbaikan_diterima = ($data->tanggal_perbaikan_diterima ? longdate_indo($data->tanggal_perbaikan_diterima) : "");

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
