<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <?php echo form_open(base_url() . "tanda_terima_sp2d/cetak_tanda_terima", array("target" => "_blank", "method" => "get")); ?>
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">SKPD : </label>
                        <div class="col-lg-6">
                            <select class="form-control select-search" name="skpd">
                                <option value="">-- ALL --</option>
                                <?php
                                foreach ($list_skpd as $key => $value) {
                                ?>
                                    <option value="<?php echo encrypt_data($value->id_master_satker); ?>"><?php echo $value->nama; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Tanggal: </label>
                        <div class="col-lg-6">
                            <button type="button" class="btn btn-light daterange-predefined">
                                <i class="icon-calendar22 mr-2"></i>
                                <span></span>
                            </button>

                            <input type="hidden" name="start_date" />
                            <input type="hidden" name="end_date" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Tanggal Cetak: </label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control daterange-single" name="tanggal_cetak" readonly placeholder="Tanggal">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Penandatangan (KBUD) : </label>
                        <div class="col-lg-6">
                            <select class="form-control select-search" name="kbud_user" required>
                                <option value="">-- PILIH --</option>
                                <?php
                                foreach ($list_kbud as $key => $value) {
                                ?>
                                    <option value="<?php echo encrypt_data($value->id_user); ?>"><?php echo $value->nama_lengkap; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary">Cetak <i class="icon-printer ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            dateLimit: {
                days: 60
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));

    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });
</script>