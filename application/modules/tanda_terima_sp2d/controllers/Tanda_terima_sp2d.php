<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tanda_terima_sp2d extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("master_satker/master_satker_model", "master_satker_model");
        $this->load->model("user_model");
        $this->load->model("trx_spm_model");
    }

    public function index()
    {
        $data['list_skpd'] = $this->master_satker_model->get(
            array(
                "order_by" => array(
                    "nama" => "ASC"
                )
            )
        );

        $data['list_kbud'] = $this->user_model->get(
            array(
                "order_by" => array(
                    "nama_lengkap" => "ASC"
                ),
                "where" => array(
                    "level_user_id" => "5"
                )
            )
        );

        $data['breadcrumb'] = [['link' => false, 'content' => 'Laporan', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function cetak_tanda_terima()
    {
        $skpd = decrypt_data($this->iget("skpd"));
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $tanggal_cetak = $this->iget("tanggal_cetak");
        $kbud_user = decrypt_data($this->iget("kbud_user"));

        $user_kbud = $this->user_model->get_by($kbud_user);

        if ($skpd) {
            $data_skpd = $this->master_satker_model->get_by($skpd);
        }

        $data = array();
        $data['detail']->nama = ($skpd) ? $data_skpd->nama : "SEMUA DINAS";
        $data['detail']->tanggal = date_indo($start_date) . " s.d " . date_indo($end_date);
        $data['detail']->tanggal_cetak = date_indo(date("Y-m-d", strtotime($tanggal_cetak)));
        $data['detail']->nama_kbud = $user_kbud->nama_lengkap;
        $data['detail']->nip = $user_kbud->nip;

        $wh = "";
        if ($skpd) {
            $wh = "`master_satker_id` = '{$skpd}' AND `status_spm` = '4' AND";
        } else {
            $wh = "`status_spm` = '4' AND";
        }

        $data_trx_spm = $this->trx_spm_model->query("
            SELECT `id_trx_spm`, DATE_FORMAT(trx_spm.tanggal_spm_trx, '%Y-%m-%d') AS tanggal_spm_trx, trx_spm.`no_register`, `no_spm`, `nama`, `nilai`, `uraian`, `no_sp2d_penyelesaian`,a.no_register AS nomor_berkas,DATE_FORMAT(trx_spm.tanggal_spm_trx, '%d-%m-%Y') AS tanggal_spm_trx_range,DATE_FORMAT(a.tanggal_spm_trx,'%d-%m-%Y') as tanggal_berkas,nama_sumber_pendanaan
            FROM `trx_spm` 
            JOIN `spm` ON `spm_id`=`id_spm` AND `spm`.`deleted_at` IS NULL 
            JOIN `master_satker` ON `id_master_satker`=`master_satker_id`
            JOIN `master_sumber_pendanaan` ON `id_master_sumber_pendanaan`=`master_sumber_pendanaan_id` 
            LEFT JOIN (
                SELECT no_register,spm_id,tanggal_spm_trx
                FROM trx_spm
                WHERE status_spm = '2' AND trx_spm.deleted_at IS NULL
            ) AS a ON a.spm_id=id_spm
            WHERE {$wh} trx_spm.tanggal_spm_trx BETWEEN '{$start_date}' AND '{$end_date}' AND `trx_spm`.`deleted_at` IS NULL 
            ORDER BY `trx_spm`.`tanggal_spm_trx` DESC
            ")->result();

        $data['content'] = $data_trx_spm;

        $mpdf = new \Mpdf\Mpdf(['format' => 'Legal', 'orientation' => 'P', 'tempDir' => '/tmp']);
        $data = $this->load->view("tanda_terima_sp2d", $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }
}
