<style>
	.user-image-custom {
		margin-bottom: 10px;
	}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
			<fieldset class="mb-3">
				<legend class="text-uppercase font-size-sm font-weight-bold">User</legend>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Nama Lengkap <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_lengkap : ""; ?>" name="nama_lengkap" required placeholder="Nama Lengkap">
						<input type="hidden" name="id_user" value="<?php echo !empty($content) ? encrypt_data($content->id_user) : ""; ?>" />
						<input type="hidden" name="id_level_user" value="<?php echo !empty($content) ? $content->level_user_id : ""; ?>" />
						<input type="hidden" name="id_master_satker" value="<?php echo !empty($content) ? $content->master_satker_id : ""; ?>" />
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">NIP</label>
					<div class="col-lg-10">
						<input type="number" class="form-control" value="<?php echo !empty($content) ? $content->nip : ""; ?>" name="nip" placeholder="NIP">
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Username <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->username : ""; ?>" name="username" required placeholder="Username">
					</div>
				</div>


				<div class="form-group row">
					<label class="col-form-label col-lg-2">Password <?php echo !empty($content) ? '' : '<span class="text-danger">*</span>'; ?></label>
					<div class="col-lg-10">
						<input type="password" name="password" id="password" <?php echo !empty($content) ? '' : 'required'; ?> placeholder="Password" class="form-control">
						<?php echo empty($content) ? '' : '<span class="form-text text-muted">Jika ingin merubah password, silahkan diisi</span>'; ?>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Level <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<select class="form-control select-search" name="level_user" required onchange="cek_level()">
							<option value="">-- Pilih Level User --</option>
							<?php
							foreach ($level_user as $key => $row) {
								$selected = "";
								if (!empty($content)) {
									if ($row->id_level_user == $content->level_user_id) {
										$selected = 'selected="selected"';
									}
								}
							?>
								<option <?php echo $selected; ?> value="<?php echo $row->id_level_user; ?>"><?php echo $row->nama_level_user; ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>

				<div class="form-group row is-show-skpd">
					<label class="col-form-label col-lg-2">SKPD <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<select class="form-control select-search" name="skpd">
							<option value="">-- PILIH --</option>
						</select>
					</div>
				</div>
			</fieldset>

			<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>

<script>
	$("form").submit(function() {
		var swalInit = swal.mixin({
			buttonsStyling: false,
			confirmButtonClass: 'btn btn-primary',
			cancelButtonClass: 'btn btn-light'
		});

		let id_user = $("input[name='id_user']").val();
		let username = $("input[name='username']").val();
		let status = true;

		$.ajax({
			url: base_url + 'user/request/cek_username',
			async: false,
			data: {
				username: username,
				id_user: id_user
			},
			type: 'GET',
			beforeSend: function() {
				HoldOn.open(optionsHoldOn);
			},
			success: function(response) {
				// alert(response);
				status = response;
			},
			complete: function(response) {
				HoldOn.close();
			}
		});

		if (!status) {
			swalInit(
				'Gagal',
				'Username sudah digunakan',
				'error'
			);
			return false;
		}
	});

	$(".is-show-skpd").hide();
	$("select[name='skpd']").attr("required", false);

	function cek_level() {
		let level = $("select[name='level_user']").val();
		$(".is-show-skpd").hide();
		$("select[name='skpd']").attr("required", false);
		if (level == "7") {
			$(".is-show-skpd").show();
			$("select[name='skpd']").attr("required", true);
			list_skpd();
		}
	}

	function list_skpd() {
		let id_master_satker = $("input[name='id_master_satker']").val();

		let html = "<option value=''>-- PILIH --</option>";
		$.ajax({
			url: base_url + 'spm_masuk/request/get_data_skpd',
			type: 'GET',
			success: function(response) {
				$.each(response, function(index, value) {
					let selected = "";
					if (id_master_satker) {
						if (value.id_master_satker == id_master_satker) {
							selected = "selected";
						}
					}
					html += "<option data-id='" + value.id_master_satker + "' " + selected + " value='" + value.id_encrypt + "'>" + value.nama + "</option>";
				});

				$("select[name='skpd']").html(html);
			}
		});
	}

	$(function() {
		let level = $("select[name='level_user']").val();
		if (level) {
			cek_level();
		}
	})
</script>