<?php

class Tahun_anggaran_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "tahun_anggaran";
        $this->primary_id = "id_tahun_anggaran";
    }
}
