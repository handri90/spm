<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model("referensi_spm_model");
    }

    public function get_data_spm()
    {
        $skpd = decrypt_data($this->iget("skpd"));

        $wh = array();
        if ($skpd) {
            $wh = array(
                "master_satker_id" => $skpd
            );
        }

        $data_spm = $this->referensi_spm_model->get(
            array(
                "fields" => "referensi_spm.*,nama,uraian_sub_rincian_objek,nama_program,nama_kegiatan,nama_sub_kegiatan",
                "where" => $wh,
                "join" => array(
                    "master_satker" => "id_master_satker=master_satker_id",
                    "master_sub_kegiatan" => "referensi_spm.master_sub_kegiatan_id=id_master_sub_kegiatan",
                    "master_kegiatan" => "referensi_spm.master_kegiatan_id=id_master_kegiatan",
                    "master_program" => "referensi_spm.master_program_id=id_master_program",
                ),
                "left_join" => array(
                    "master_kode_sub_rincian_objek" => "master_kode_sub_rincian_objek_id=id_master_kode_sub_rincian_objek",
                )
            )
        );

        $templist = array();
        foreach ($data_spm as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_referensi_spm);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
