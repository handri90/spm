<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Import_spm extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('referensi_spm_model');
        $this->load->model('master_kode_akun_model');
        $this->load->model('master_kode_kelompok_model');
        $this->load->model('master_kode_jenis_model');
        $this->load->model('master_kode_objek_model');
        $this->load->model('master_kode_rincian_objek_model');
        $this->load->model('master_kode_sub_rincian_objek_model');
        $this->load->model('master_urusan_model');
        $this->load->model('master_bidang_urusan_model');
        $this->load->model('master_program_model');
        $this->load->model('master_kegiatan_model');
        $this->load->model('master_sub_kegiatan_model');
        $this->load->model('master_satker/master_satker_model', 'master_satker_model');
        $this->load->model('master_sumber_pendanaan/master_sumber_pendanaan_model', 'master_sumber_pendanaan_model');
    }

    public function index()
    {
        $data['list_skpd'] = $this->master_satker_model->get(
            array(
                "order_by" => array(
                    "nama" => "ASC"
                )
            )
        );
        $data['breadcrumb'] = [['link' => false, 'content' => 'Import SPM', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function upload_spm()
    {
        if (empty($_FILES)) {
            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'import_spm', 'content' => 'Import SPM', 'is_active' => false], ['link' => false, 'content' => 'Upload SPM', 'is_active' => true]];
            $this->execute('form_import_spm', $data);
        } else {
            $input_name = 'file_excel';
            $upload_file = $this->upload_file($input_name, $this->config->item('path_excell'), "", "excell");

            if (!isset($upload_file['error'])) {
                require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');

                $excelreader = new PHPExcel_Reader_Excel2007();

                // $objPHPExcel = PHPExcel_IOFactory::load('./' . $this->config->item('path_excell') . '/72c8c90ff42968ec2e0e6e5614dd22ad.xlsx');
                // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                // $objWriter->save(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell') . '/72c8c90ff42968ec2e0e6e5614dd22ad.xlsx'));

                // chmod(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell') . '/' . $upload_file['data']['file_name']), 0777);

                $loadexcel = $excelreader->load(str_replace('.xls', '.xlsx', './' . $this->config->item('path_excell') . '/72c8c90ff42968ec2e0e6e5614dd22ad.xlsx'));

                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

                $numrow = 1;
                $numrow_skpd_false = 0;
                $numrow_neraca_false = 0;
                $numrow_id_false = 0;
                $duplicate_id_false = 0;
                $temp_arr_index_id = array();
                $data_save = array();
                $data_update = array();
                foreach ($sheet as $row) {
                    //cek master satker

                    if ($numrow > 1) {
                        if (trim($row['A']) != "" && trim($row['A']) != "0") {
                            $master_satker = $this->master_satker_model->get(
                                array(
                                    "where" => array(
                                        "kode_skpd_terbaru" => trim($row['B'])
                                    )
                                ),
                                "row"
                            );
    
                            // cek kode_urusan
                            $cek_urusan = $this->master_urusan_model->get(
                                array(
                                    "where" => array(
                                        "kode_urusan" => trim($row['E'])
                                    )
                                ),
                                "row"
                            );
    
                            if (!$cek_urusan) {
                                $data_urusan = array(
                                    "kode_urusan" => trim($row['E']),
                                    "nama_urusan" => trim($row['F'])
                                );
    
                                $id_master_urusan = $this->master_urusan_model->save($data_urusan);
                            } else {
                                $id_master_urusan = $cek_urusan->id_master_urusan;
                            }
    
                            //cek kode_bidang_urusan
                            $cek_bidang_urusan = $this->master_bidang_urusan_model->get(
                                array(
                                    "where" => array(
                                        "master_urusan_id" => $id_master_urusan,
                                        "kode_bidang_urusan" => substr(trim($row['H']),2)
                                    )
                                ),
                                "row"
                            );
    
                            if (!$cek_bidang_urusan) {
                                $data_bidang_urusan = array(
                                    "master_urusan_id" => $id_master_urusan,
                                    "kode_bidang_urusan" => substr(trim($row['H']),2),
                                    "nama_bidang_urusan" => trim($row['I'])
                                );
    
                                $id_master_bidang_urusan = $this->master_bidang_urusan_model->save($data_bidang_urusan);
                            } else {
                                $id_master_bidang_urusan = $cek_bidang_urusan->id_master_bidang_urusan;
                            }
    
                            //cek kode_program
                            $cek_program = $this->master_program_model->get(
                                array(
                                    "join" => array(
                                        "master_bidang_urusan" => "id_master_bidang_urusan=master_bidang_urusan_id",
                                        "master_urusan" => "id_master_urusan=master_urusan_id"
                                    ),
                                    "where" => array(
                                        "master_urusan_id" => $id_master_urusan,
                                        "master_bidang_urusan_id" => $id_master_bidang_urusan,
                                        "kode_program" => substr(trim($row['K']),5)
                                    )
                                ),
                                "row"
                            );
    
                            if (!$cek_program) {
                                $data_program = array(
                                    "master_bidang_urusan_id" => $id_master_bidang_urusan,
                                    "kode_program" => substr(trim($row['K']),5),
                                    "nama_program" => trim($row['L'])
                                );
    
                                $id_master_program = $this->master_program_model->save($data_program);
                            } else {
                                $id_master_program = $cek_program->id_master_program;
                            }
    
                            //cek kode_program
                            $cek_kegiatan = $this->master_kegiatan_model->get(
                                array(
                                    "join" => array(
                                        "master_program" => "id_master_program=master_program_id",
                                        "master_bidang_urusan" => "id_master_bidang_urusan=master_bidang_urusan_id",
                                        "master_urusan" => "id_master_urusan=master_urusan_id"
                                    ),
                                    "where" => array(
                                        "master_urusan_id" => $id_master_urusan,
                                        "master_bidang_urusan_id" => $id_master_bidang_urusan,
                                        "master_program_id" => $id_master_program,
                                        "kode_kegiatan" => substr(trim($row['N']),8)
                                    )
                                ),
                                "row"
                            );
    
                            if (!$cek_kegiatan) {
                                $data_kegiatan = array(
                                    "master_program_id" => $id_master_program,
                                    "kode_kegiatan" => substr(trim($row['N']),8),
                                    "nama_kegiatan" => trim($row['O'])
                                );
    
                                $id_master_kegiatan = $this->master_kegiatan_model->save($data_kegiatan);
                            } else {
                                $id_master_kegiatan = $cek_kegiatan->id_master_kegiatan;
                            }
    
                            //cek kode_program
                            $cek_sub_kegiatan = $this->master_sub_kegiatan_model->get(
                                array(
                                    "join" => array(
                                        "master_kegiatan" => "id_master_kegiatan=master_kegiatan_id",
                                        "master_program" => "id_master_program=master_program_id",
                                        "master_bidang_urusan" => "id_master_bidang_urusan=master_bidang_urusan_id",
                                        "master_urusan" => "id_master_urusan=master_urusan_id"
                                    ),
                                    "where" => array(
                                        "master_urusan_id" => $id_master_urusan,
                                        "master_bidang_urusan_id" => $id_master_bidang_urusan,
                                        "master_program_id" => $id_master_program,
                                        "master_kegiatan_id" => $id_master_kegiatan,
                                        "kode_sub_kegiatan" => substr(trim($row['Q']),13)
                                    )
                                ),
                                "row"
                            );
    
                            if (!$cek_sub_kegiatan) {
                                $data_sub_kegiatan = array(
                                    "master_kegiatan_id" => $id_master_kegiatan,
                                    "kode_sub_kegiatan" => substr(trim($row['Q']),13),
                                    "nama_sub_kegiatan" => trim($row['R'])
                                );
    
                                $id_master_sub_kegiatan = $this->master_sub_kegiatan_model->save($data_sub_kegiatan);
                            } else {
                                $id_master_sub_kegiatan = $cek_sub_kegiatan->id_master_sub_kegiatan;
                            }
    
                            if (trim($row['S'])) {
                                $expl_kode_rekening = explode('.', trim($row['S']));
    
                                $data_neraca = $this->master_kode_akun_model->get(
                                    array(
                                        "join" => array(
                                            "master_kode_kelompok" => "id_master_kode_akun=master_kode_akun_id",
                                            "master_kode_jenis" => "id_master_kode_kelompok=master_kode_kelompok_id",
                                            "master_kode_objek" => "id_master_kode_jenis=master_kode_jenis_id",
                                            "master_kode_rincian_objek" => "id_master_kode_objek=master_kode_objek_id",
                                            "master_kode_sub_rincian_objek" => "id_master_kode_rincian_objek=master_kode_rincian_objek_id"
                                        ),
                                        "where" => array(
                                            "kode_akun" => $expl_kode_rekening[0],
                                            "kode_kelompok" => $expl_kode_rekening[1],
                                            "kode_jenis" => $expl_kode_rekening[2],
                                            "kode_objek" => $expl_kode_rekening[3],
                                            "kode_rincian_objek" => $expl_kode_rekening[4],
                                            "kode_sub_rincian_objek" => $expl_kode_rekening[5],
                                        )
                                    ),
                                    "row"
                                );
                            }
    
                            $cek_data_referensi = $this->referensi_spm_model->get(
                                array(
                                    "where" => array(
                                        "master_satker_id" => $master_satker->id_master_satker,
                                        "master_urusan_id" => $id_master_urusan,
                                        "master_bidang_urusan_id" => $id_master_bidang_urusan,
                                        "master_program_id" => $id_master_program,
                                        "master_kegiatan_id" => $id_master_kegiatan,
                                        "master_sub_kegiatan_id" => $id_master_sub_kegiatan,
                                        "master_kode_sub_rincian_objek_id" => (trim($row['S']) ? $data_neraca->id_master_kode_sub_rincian_objek : NULL),
                                    )
                                ),
                                "row"
                            );
    
                            if (!$cek_data_referensi) {
                                array_push($data_save, array(
                                        "id_excel" => NULL,
                                        "master_satker_id" => $master_satker->id_master_satker,
                                        "nilai_anggaran" =>  str_replace(",","",trim($row['U'])),
                                    "master_urusan_id" => $id_master_urusan,
                                    "master_bidang_urusan_id" => $id_master_bidang_urusan,
                                    "master_program_id" => $id_master_program,
                                    "master_kegiatan_id" => $id_master_kegiatan,
                                    "master_sub_kegiatan_id" => $id_master_sub_kegiatan,
                                    "master_kode_sub_rincian_objek_id" => (trim($row['S']) ? $data_neraca->id_master_kode_sub_rincian_objek : NULL),
                                    "created_at" => $this->datetime(),
                                ));
                            }else{
    
                                $data = array(
                                    "master_satker_id" => $master_satker->id_master_satker,
                                    "nilai_anggaran" => str_replace(",","",trim($row['U'])),
                                    "master_urusan_id" => $id_master_urusan,
                                    "master_bidang_urusan_id" => $id_master_bidang_urusan,
                                    "master_program_id" => $id_master_program,
                                    "master_kegiatan_id" => $id_master_kegiatan,
                                    "master_sub_kegiatan_id" => $id_master_sub_kegiatan,
                                    "master_kode_sub_rincian_objek_id" => (trim($row['S']) ? $data_neraca->id_master_kode_sub_rincian_objek : NULL),
                                    "updated_at" => $this->datetime(),
                                );
                    
                                $this->referensi_spm_model->edit($cek_data_referensi->id_referensi_spm, $data);
                            }
                        }
                    }

                    $numrow++; // Tambah 1 setiap kali looping
                }

                if ($numrow_skpd_false == 0 && $numrow_neraca_false == 0 && $numrow_id_false == 0 && $duplicate_id_false == 0) {
                    $this->referensi_spm_model->save_batch($data_save);
                    $this->session->set_flashdata('message', 'Data Berhasil ditambahkan');
                    redirect('import_spm');
                } else if ($duplicate_id_false != 0) {
                    $this->session->set_flashdata('message', 'Gagal upload file : Tidak diperbolehkan nomor yang sama. Kesalahan pada kolom A baris ' . $duplicate_id_false);
                    redirect('import_spm/upload_spm');
                } else if ($numrow_id_false != 0) {
                    $this->session->set_flashdata('message', 'Gagal upload file : Kesalahan pada kolom A baris ' . $numrow_id_false);
                    redirect('import_spm/upload_spm');
                } else if ($numrow_skpd_false != 0) {
                    $this->session->set_flashdata('message', 'Gagal upload file : Kesalahan pada kolom B baris ' . $numrow_skpd_false);
                    redirect('import_spm/upload_spm');
                } else if ($numrow_neraca_false != 0) {
                    $this->session->set_flashdata('message', 'Gagal upload file : Kesalahan pada data nomenklatur rekening baris ' . $numrow_neraca_false);
                    redirect('import_spm/upload_spm');
                }
            } else {
                $this->session->set_flashdata('message', 'Gagal upload file');
                redirect('import_spm');
            }
        }
    }

    // public function upload_neraca()
    // {
    //     require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');

    //     $excelreader = new PHPExcel_Reader_Excel2007();

    //     $loadexcel = $excelreader->load("./assets/master_neraca.xlsx");

    //     $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

    //     $numrow = 1;
    //     $numrow_skpd_false = 0;
    //     $data = array();
    //     $id_akun = NULL;
    //     $id_kelompok = NULL;
    //     $id_jenis = NULL;
    //     $id_objek = NULL;
    //     $id_rincian = NULL;
    //     $id_sub_rincian_objek = NULL;
    //     foreach ($sheet as $key => $row) {
    //         $kode_rekening = trim($row['A']);

    //         $expl_kode_rekening = explode(".", $kode_rekening);

    //         if (count($expl_kode_rekening) == '1') {
    //             $id_akun = $this->master_kode_akun_model->save(array("kode_akun" => $expl_kode_rekening[0], "uraian_akun" => trim($row['B'])));
    //         } else if (count($expl_kode_rekening) == '2') {
    //             $id_kelompok = $this->master_kode_kelompok_model->save(array("kode_kelompok" => $expl_kode_rekening[1], "master_kode_akun_id" => $id_akun, "uraian_kelompok" => trim($row['B'])));
    //         } else if (count($expl_kode_rekening) == '3') {
    //             $id_jenis = $this->master_kode_jenis_model->save(array("kode_jenis" => $expl_kode_rekening[2], "master_kode_kelompok_id" => $id_kelompok, "uraian_jenis" => trim($row['B'])));
    //         } else if (count($expl_kode_rekening) == '4') {
    //             $id_objek = $this->master_kode_objek_model->save(array("kode_objek" => $expl_kode_rekening[3], "master_kode_jenis_id" => $id_jenis, "uraian_objek" => trim($row['B'])));
    //         } else if (count($expl_kode_rekening) == '5') {
    //             $id_rincian_objek = $this->master_kode_rincian_objek_model->save(array("kode_rincian_objek" => $expl_kode_rekening[4], "master_kode_objek_id" => $id_objek, "uraian_rincian_objek" => trim($row['B'])));
    //         } else if (count($expl_kode_rekening) == '6') {
    //             $id_sub_rincian_objek = $this->master_kode_sub_rincian_objek_model->save(array("kode_sub_rincian_objek" => $expl_kode_rekening[5], "master_kode_rincian_objek_id" => $id_rincian_objek, "uraian_sub_rincian_objek" => trim($row['B'])));
    //         }
    //     }
    // }

    // public function upload_sumber_pendanaan()
    // {
    //     require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');

    //     $excelreader = new PHPExcel_Reader_Excel2007();

    //     $loadexcel = $excelreader->load("./assets/sumber_pendanaan.xlsx");

    //     $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

    //     foreach ($sheet as $key => $row) {
    //         $this->master_sumber_pendanaan_model->save(array("nama_sumber_pendanaan" => $row['A']));
    //     }
    // }
}
