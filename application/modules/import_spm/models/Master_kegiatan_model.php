<?php

class Master_kegiatan_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_kegiatan";
        $this->primary_id = "id_master_kegiatan";
    }
}
