<?php

class Master_kode_kelompok_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_kode_kelompok";
        $this->primary_id = "id_master_kode_kelompok";
    }
}
