<?php

class Master_kode_akun_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_kode_akun";
        $this->primary_id = "id_master_kode_akun";
    }
}
