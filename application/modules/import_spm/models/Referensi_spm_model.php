<?php

class Referensi_spm_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "referensi_spm";
        $this->primary_id = "id_referensi_spm";
    }
}
