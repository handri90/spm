<?php

class Master_kode_jenis_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_kode_jenis";
        $this->primary_id = "id_master_kode_jenis";
    }
}
