<?php

class Master_kode_rincian_objek_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_kode_rincian_objek";
        $this->primary_id = "id_master_kode_rincian_objek";
    }
}
