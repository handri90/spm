<?php

class Master_sub_kegiatan_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_sub_kegiatan";
        $this->primary_id = "id_master_sub_kegiatan";
    }
}
