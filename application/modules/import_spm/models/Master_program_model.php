<?php

class Master_program_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_program";
        $this->primary_id = "id_master_program";
    }
}
