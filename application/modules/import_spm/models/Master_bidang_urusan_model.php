<?php

class Master_bidang_urusan_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_bidang_urusan";
        $this->primary_id = "id_master_bidang_urusan";
    }
}
