<style>
	.user-image-custom {
		margin-bottom: 10px;
	}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open_multipart(); ?>
			<fieldset class="mb-3">
				<legend class="text-uppercase font-size-sm font-weight-bold">Upload SPM</legend>
				<div class="form-group row">
					<label class="col-lg-2 col-form-label">File <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="file" class="file-input" name="file_excel" required data-show-upload="false">
					</div>
				</div>
			</fieldset>

			<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>