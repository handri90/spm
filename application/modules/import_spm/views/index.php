<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>SKPD : </label>
                        <select class="form-control select-search" name="skpd" onChange="get_data_spm()">
                            <option value="">-- SEMUA --</option>
                            <?php
                            foreach ($list_skpd as $key => $row) {
                            ?>
                                <option value="<?php echo encrypt_data($row->id_master_satker); ?>"><?php echo $row->nama; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'import_spm/upload_spm'; ?>" class="btn btn-info">Upload File</a>
            </div>
        </div>
        <table id="datatableSPM" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>SKPD</th>
                    <th>Kegiatan</th>
                    <th>Sub Kegiatan</th>
                    <th>Sub Rincian Obyek</th>
                    <th>Nilai</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    const IDR = value => currency(
        value, {
            symbol: "Rp. ",
            precision: 0,
            separator: "."
        });

    let datatableSPM = $("#datatableSPM").DataTable({
        "columns": [
            null,
            null,
            null,
            null,
            {
                "width": "140"
            }
        ]
    });

    get_data_spm();

    function get_data_spm() {
        let skpd = $("select[name='skpd']").val();

        datatableSPM.clear().draw();

        if (skpd) {
            $.ajax({
                url: base_url + 'import_spm/request/get_data_spm',
                data: {
                    skpd: skpd
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        datatableSPM.row.add([
                            value.nama,
                            value.nama_kegiatan,
                            value.nama_sub_kegiatan,
                            value.uraian_sub_rincian_objek,
                            IDR(value.nilai_anggaran).format(true)
                        ]).draw(false);
                    });
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }

    }
</script>