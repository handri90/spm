<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['breadcrumb'] = [];
        $this->execute('index', $data);
    }
}
