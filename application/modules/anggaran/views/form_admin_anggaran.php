<style>
    .user-image-custom {
        margin-bottom: 10px;
    }

    .input-group {
        flex-wrap: initial;
    }
</style>
<div class="content">
    <?php echo form_open(); ?>
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Tambah SPM</legend>
                <input type="hidden" name="id_skpd_edit" value="<?php echo isset($content)?encrypt_data($content->master_satker_id):''; ?>" />
                <input type="hidden" name="id_anggaran_edit" value="<?php echo isset($content)?encrypt_data($content->id_anggaran):''; ?>" />
                <input type="hidden" name="id_sub_kegiatan_edit" value="<?php echo isset($content)?encrypt_data($content->master_sub_kegiatan_id):''; ?>" />
                <input type="hidden" name="id_sub_kegiatan_edit_no_enc" value="<?php echo isset($content)?$content->master_sub_kegiatan_id:''; ?>" />
                <input type="hidden" name="id_skpd_after" value="<?php echo !empty($master_satker_after)?encrypt_data($master_satker_after):''; ?>" />
                <input type="hidden" name="id_sub_kegiatan_after" value="<?php echo !empty($sub_kegiatan_after)?encrypt_data($sub_kegiatan_after):''; ?>" />
                <input type="hidden" name="id_sub_kegiatan_after_no_enc" value="<?php echo !empty($sub_kegiatan_after)?$sub_kegiatan_after:''; ?>" />
                <div class="form-group row">
                    <label class="col-form-label col-lg-2">SKPD <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select <?php echo !empty($content)?"disabled":"" ?> class="form-control select-search" name="skpd" required onchange="get_sub_kegiatan()">
                            <option value="">-- PILIH SKPD --</option>
                            <?php
                            foreach($skpd as $value){
                                $selected = "";
                                if (!empty($content)) {
                                    if ($value->id_master_satker == $content->master_satker_id) {
                                        $selected = 'selected="selected"';
                                    }
                                }else if(!empty($master_satker_after)){
                                    if ($value->id_master_satker == $master_satker_after) {
                                        $selected = 'selected="selected"';
                                    }
                                }
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo encrypt_data($value->id_master_satker); ?>"><?php echo $value->nama; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Sub Kegiatan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select <?php echo !empty($content)?"disabled":"" ?> class="form-control select-search" name="sub_kegiatan" required onchange="cek_data()">
                            <option value="">-- Pilih Sub Kegiatan --</option>
                        </select>
                    </div>
                </div>
            </fieldset>

            <!-- <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
            </div> -->
        </div>
    </div>
    <span class="loading-page">
    <span class="card-edit"></span>
    <span class='copy-model-akun'></span>
    </span>
    <div class="card">
        <div class="card-body">
            <div class="float-right">
                <a type="button" class="btn btn-warning" href="#tambahAkun" onclick="add_field_akun()">Tambah Akun</a>
                <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
            </div>
        </div>
    </div>
    <!-- /form inputs -->
    <?php echo form_close(); ?>

    <span class="master-akun d-none">
        <span>
            <div class="card akun-clone">
                <div class="card-body">
                    <fieldset class="mb-3">
                        <legend class="text-uppercase font-size-sm font-weight-bold">Tambah Akun & Sumber Pendanaan
                            <span class="close-model-akun"></span> 
                                <select class="form-control select-search" name="sub_rincian_objek[]" data-index="0" indexsubrincian="0" onchange="tampil_nilai_anggaran(this)" required>
                                    <option value="">-- Pilih Sub Rincian Objek --</option>
                                </select>
                                <input type='hidden' name='index_send[]' value='0' />
                                <div class="nilai-anggaran-0"></div>
                                <input type="hidden" name="nilai_anggaran_0[]" />
                        </legend>

                        <div class="row m-2">
                            <a type="button" class="btn btn-success" href="#tambahSumberDana" data-index="0" onclick="add_field(this)">Tambah Sumber Pendanaan</a>
                        </div>

                        <span class="clone-model-0">
                            <hr />
                            <span class="close-sumber-pendanaan"></span>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Sumber Pendanaan <span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <select class="form-control select-search" name="sumber_pendanaan_0[][]" required>
                                        <option value="">-- Pilih Sumber Pendanaan --</option>
                                        <?php
                                        foreach ($sumber_dana_anggaran as $key => $row) {
                                        ?>
                                            <option value="<?php echo encrypt_data($row->id_master_sumber_pendanaan_anggaran); ?>" idsumber="<?php echo $row->id_master_sumber_pendanaan_anggaran; ?>"><?php echo $row->nama_sumber_pendanaan_anggaran; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-2">Nilai Rincian <span class="text-danger">*</span></label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control input-currency-0" name="nilai_0[][]" placeholder="Nilai Rincian" required data-index="0" onkeyup="calc_nilai(this)">
                                </div>
                            </div>
                        </span>
                        <span class="copy-model-0"></span>
                    </fieldset>
                </div>
            </div>
        </span>
        <span class="copy-model-akun"></span>
    </span>

    <span class="d-none clone-model-akun">
        <div class="card akun-clone">
            <div class="card-body">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Tambah Akun & Sumber Pendanaan
                        <span class="close-model-akun"></span> 
                            <select class="form-control select-search" name="sub_rincian_objek_clone[]" data-index="0" indexsubrincian="0" onchange="tampil_nilai_anggaran(this)" required>
                                <option value="">-- Pilih Sub Rincian Objek --</option>
                                <?php
                                foreach ($sub_rincian_objek as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value->id_master_kode_sub_rincian_objek; ?>" nilaianggaran="<?php echo $value->nilai_anggaran; ?>"><?php echo $value->uraian_sub_rincian_objek; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <input type='hidden' name='index_send_clone[]' value='0' />
                            <div class="nilai-anggaran-0-clone"></div>
                            <input type="hidden" name="nilai_anggaran_clone[]" />
                    </legend>

                    <div class="row m-2">
                        <a type="button" class="btn btn-success" href="#tambahSumberDana" data-index="0" onclick="add_field(this)">Tambah Sumber Pendanaan</a>
                    </div>

                    <span class="clone-model-0-clone">
                        <hr />
                        <span class="close-sumber-pendanaan"></span>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Sumber Pendanaan <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <select class="form-control select-search" name="sumber_pendanaan_0_clone[][]" required>
                                    <option value="">-- Pilih Sumber Pendanaan --</option>
                                    <?php
                                    foreach ($sumber_dana_anggaran as $key => $row) {
                                    ?>
                                        <option value="<?php echo encrypt_data($row->id_master_sumber_pendanaan_anggaran); ?>" idsumber="<?php echo $row->id_master_sumber_pendanaan_anggaran; ?>"><?php echo $row->nama_sumber_pendanaan_anggaran; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Nilai Rincian <span class="text-danger">*</span></label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control input-currency-0" name="nilai_0_clone[][]" placeholder="Nilai Rincian" required data-index="0" onkeyup="calc_nilai(this)">
                            </div>
                        </div>
                    </span>
                    <span class="copy-model-0-clone"></span>
                </fieldset>
            </div>
        </div>
    </span>
</div>

<script>

    const IDR = value => currency(
    value, {
        symbol: "Rp. ",
        precision: 0,
        separator: "."
    });

    $(function(){
        let id_skpd_edit = $("input[name='id_skpd_edit']").val();
        let id_anggaran_edit = $("input[name='id_anggaran_edit']").val();
        if(id_anggaran_edit != ""){
            get_sub_kegiatan(id_skpd_edit);
            $.ajax({
                url: base_url + 'anggaran/request/get_data_trx_anggaran_edit',
                data: {
                    anggaran_id: id_anggaran_edit
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    let index = 0;
                    response.forEach(element => {
                        generate_sub_rincian_objek(element.id_encrypt,element.master_kode_sub_rincian_objek_id,index,id_skpd_edit);
                        index++;
                    });
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }

        let id_skpd_after = $("input[name='id_skpd_after']").val();
        if(id_skpd_after != ""){
            get_sub_kegiatan(id_skpd_after);
        }
    });

    function cek_data(){
        let skpd = $("select[name='skpd']").val();
        if(skpd == undefined){
            skpd = "";
        }
        let id = $("select[name='sub_kegiatan']").val();
        $.ajax({
            url: base_url + 'anggaran/request/cek_data_anggaran',
            data: {
                sub_kegiatan_id: id,
                skpd_id: skpd,
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                if(response.trx_anggaran.length > 0){
                    $(".loading-page").html("");
                    let html = "<span class='card-edit'></span>"+
                    "<span class='copy-model-akun'></span>";
                    $(".loading-page").html(html);
                    // $(".akun-clone").addClass("d-none");
                    $("input[name='id_anggaran_edit']").val(response.anggaran.id_encrypt);
                    $("input[name='id_skpd_edit']").val(skpd);
                    $("input[name='id_sub_kegiatan_edit']").val(id);
                    let index = 0;
                    get_sub_rincian_objek_clone();
                    response.trx_anggaran.forEach(element => {
                        generate_sub_rincian_objek(element.id_encrypt,element.master_kode_sub_rincian_objek_id,index,skpd);
                        index++;
                    });
                }else{
                    $("input[name='id_anggaran_edit']").val("");
                    $("input[name='id_skpd_edit']").val("");
                    $("input[name='id_sub_kegiatan_edit']").val("");
                    $(".loading-page").html("");
                    $("select[name='sub_rincian_objek[]']").select2("destroy").end();
                    $("select[name='sumber_pendanaan_0[][]']").select2("destroy").end();
                    var html_clone = $(".master-akun").clone();
                    html_clone.removeClass("d-none");
                    $(html_clone).appendTo(".loading-page");
                    $("select[name='sub_rincian_objek[]']").select2();
                    $("select[name='sumber_pendanaan_0[][]']").select2();
                    new Cleave('.input-currency-0', {
                        numeral: true,
                        numeralThousandsGroupStyle: 'thousand',
                        numeralDecimalMark: ',',
                        delimiter: '.',
                    });
                    get_sub_rincian_objek();
                }
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function generate_sub_rincian_objek(id_encrypt,master_kode_sub_rincian_objek_id,index,skpd){
        let id_sub_kegiatan_edit = $("input[name='id_sub_kegiatan_edit']").val();
        $.ajax({
            url: base_url + 'anggaran/request/get_data_sub_rincian_objek_edit',
            async:false,
            data: {
                id: id_sub_kegiatan_edit,
                "master_kode_sub_rincian_objek_id":master_kode_sub_rincian_objek_id,
                "id_trx_anggaran":id_encrypt,
                "index":index,
                "skpd":skpd,
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let htmls = "";
                let htmloption = "";
                let nilai_anggaran = 0;
                htmls += "<span>"+
                            "<div class='card akun-clone'>"+
                                "<div class='card-body'>"+
                                    "<fieldset class='mb-3'>"+
                                        "<legend class='text-uppercase font-size-sm font-weight-bold'>Tambah Akun & Sumber Pendanaan"+
                                            "<span class='close-model-akun'><a href='#deleteModelAkun' class='delete-model-akun float-right' style='color:red;' onclick=\"delete_akun_edit('"+id_encrypt+"',this)\"><i class='icon-close2' style='font-size:20pt;color:red;'></i>Hapus Akun</a></span> "+
                                            "<select class='form-control' name='sub_rincian_objek[]' data-index='"+index+"' indexsubrincian='"+index+"' onchange='tampil_nilai_anggaran(this)' required>"+
                                                "<option value=''>-- Pilih Sub Rincian Objek --</option>";

                                                $.each(response.sub_rincian_belanja, function(index, value) {
                                                    let selected = "";
                                                    if(master_kode_sub_rincian_objek_id == value.id_master_kode_sub_rincian_objek){
                                                        nilai_anggaran = value.nilai_anggaran;
                                                        selected = "selected";
                                                    }
                                                    htmls += "<option "+selected+" value='" + value.id_master_kode_sub_rincian_objek + "' nilaianggaran='"+value.nilai_anggaran+"'>" + value.uraian_sub_rincian_objek + "</option>";
                                                });
                                htmls += "</select>"+
                                "<input type='hidden' name='index_send[]' value='"+index+"' /><div class='nilai-anggaran-"+index+"'><span class='badge badge-danger mt-2'><h6>PAGU : "+IDR(nilai_anggaran).format(true)+"</h6></span></div>"+
                            "<input type='hidden' name='nilai_anggaran_"+index+"[]' value='"+nilai_anggaran+"' />"+
                                        "</legend>"+
                                        "<div class='row m-2'>"+
                                        "<a type='button' class='btn btn-success' href='#tambahSumberDana' data-index='"+index+"' onclick='add_field(this)'>Tambah Sumber Pendanaan</a>"+
                                        "</div>";



                                htmls += response.sumber_dana+
                                    "</fieldset>"+
                                "</div>"+
                            "</div>"+
                        "</span>";
                        $(".card-edit").append(htmls);
                $("select[name='sub_rincian_objek[]']",".card-edit").select2();
                $("select[name='sumber_pendanaan_"+index+"[][]']",".card-edit").select2();
                if(response.angka_random_currency.length > 0){
                    for (let j = 0; j < response.angka_random_currency.length; j++) {
                        new Cleave('.input-currency-'+index.toString()+response.angka_random_currency[j].toString(), {
                            numeral: true,
                            numeralThousandsGroupStyle: 'thousand',
                            numeralDecimalMark: ',',
                            delimiter: '.',
                        });
                        
                    }
                }
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function get_sub_rincian_objek_clone() {
        let id = $("select[name='sub_kegiatan']").val();
        let skpd = $("select[name='skpd']").val();
        if(skpd == undefined){
            skpd = "";
        }
        let html = "<option value=''>-- Pilih Sub Rincian Objek --</option>";
        if (id) {
            $.ajax({
                url: base_url + 'anggaran/request/get_data_sub_rincian_objek',
                data: {
                    id: id,
                    skpd: skpd,
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.id_master_kode_sub_rincian_objek + "' nilaianggaran='"+value.nilai_anggaran+"'>" + value.uraian_sub_rincian_objek + "</option>";
                    });
                    $("select[name='sub_rincian_objek_clone[]']").html(html);
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='sub_rincian_objek_clone[]']").html(html);
        }
    }

    function get_sub_rincian_objek(sub_kegiatan, sub_rincian) {
        let skpd = $("select[name='skpd']").val();
        if(skpd == undefined){
            skpd = "";
        }
        let id = $("select[name='sub_kegiatan']").val();
        if (sub_kegiatan) {
            id = sub_kegiatan;
        }
        let html = "<option value=''>-- Pilih Sub Rincian Objek --</option>";
        if (id) {
            $.ajax({
                url: base_url + 'anggaran/request/get_data_sub_rincian_objek',
                data: {
                    id: id,
                    skpd: skpd,
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        html += "<option value='" + value.id_master_kode_sub_rincian_objek + "' nilaianggaran='"+value.nilai_anggaran+"'>" + value.uraian_sub_rincian_objek + "</option>";
                    });
                    $("select[name='sub_rincian_objek[]']").html(html);
                    $("select[name='sub_rincian_objek_clone[]']").html(html);
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='sub_rincian_objek[]']").html(html);
            $("select[name='sub_rincian_objek_clone[]']").html(html);
        }
    }
    
    function get_sub_kegiatan(skpd_edit) {
        let skpd = $("select[name='skpd']").val();
        if(skpd == undefined){
            skpd = "";
        }
        let id_sub_kegiatan_edit = $("input[name='id_sub_kegiatan_edit_no_enc']").val();
        let id_sub_kegiatan_after = $("input[name='id_sub_kegiatan_after_no_enc']").val();
        if(skpd_edit != undefined){
            skpd = skpd_edit;
        }
        let html = "<option value=''>-- PILIH SUB KEGIATAN --</option>";
        if (skpd) {
            $.ajax({
                url: base_url + 'anggaran/request/get_data_sub_kegiatan',
                data: {
                    skpd: skpd
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        let selected = "";
                        if(id_sub_kegiatan_edit){
                            if(value.id_master_sub_kegiatan === id_sub_kegiatan_edit){
                                selected = "selected";
                            }
                        }else if(id_sub_kegiatan_after){
                            if(value.id_master_sub_kegiatan === id_sub_kegiatan_after){
                                selected = "selected";
                            }
                        }
                        html += "<option "+selected+" value='" + value.id_encrypt + "'>" + value.nama_program + " - " + value.nama_kegiatan + " - " + value.nama_sub_kegiatan + "</option>";
                    });
                    $("select[name='sub_kegiatan']").html(html);
                    if(id_sub_kegiatan_after){
                        cek_data();
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='sub_kegiatan']").html(html);
        }
    }

    function tampil_nilai_anggaran(e){
        $(".nilai-anggaran-"+$(e).data("index")).html("");
        $("input[name='nilai_anggaran_"+$(e).data("index")+"[]']").val("");
        if($("option:selected",e).attr("nilaianggaran") != undefined){
            $("input[name='nilai_anggaran_"+$(e).data("index")+"[]']").val($("option:selected",e).attr("nilaianggaran"));
            $(".nilai-anggaran-"+$(e).data("index")).html("<span class='badge badge-danger mt-2'><h6>Pagu : "+IDR($("option:selected",e).attr("nilaianggaran")).format(true)+"</h6></span>");
        }
    }

    var cleave = new Cleave('.input-currency-0', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand',
        numeralDecimalMark: ',',
        delimiter: '.',
    });

    $(function() {
        let sub_rincian_objek = $("input[name='sub_rincian_objek']").val();
        let sub_kegiatan = $("input[name='sub_kegiatan']").val();
        if (sub_rincian_objek) {
            get_sub_rincian_objek(sub_kegiatan, sub_rincian_objek);
        }
    })

    function add_field(e){
        let random = Math.floor(Math.random() * 100);
        let data_index = $(e).data("index");
        $("select[name='sumber_pendanaan_"+data_index+"[][]']").select2("destroy");
        var html_clone = $(".clone-model-"+data_index).clone().first();
        $(html_clone).find("input:text").val("");
        $(html_clone).find("input:text").removeAttr("value");
        $(html_clone).find("input:text").removeAttr("class");
        $(html_clone).find("input:text").addClass("input-currency-"+data_index+random).addClass("form-control");
        $(html_clone).removeClass("clone-model-"+data_index);
        $(html_clone).find(".close-sumber-pendanaan").html("");
        $(html_clone).find(".close-sumber-pendanaan").append("<div class='text-right mb-2'><a href='#hapusSumberPendanaan' class='delete-model-sumber-pendanaan'>Hapus Sumber Pendanaan</a></div>");
        
        $(html_clone).appendTo(".copy-model-"+data_index);
        new Cleave('.input-currency-'+data_index+random, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand',
            numeralDecimalMark: ',',
            delimiter: '.',
        });
        $("select[name='sumber_pendanaan_"+data_index+"[][]']").select2();


        $(".delete-model-sumber-pendanaan").on("click",function(e){
            if($("form select[name='sumber_pendanaan_"+data_index+"[][]']").length == 1){
                //ditolak
                Swal.fire(
                    'Gagal',
                    'Tidak bisa dihapus',
                    'error'
                )
            }else{
                e.preventDefault();
                $(this).parent().parent().parent().remove();
            }
        });
    }

    function add_field_akun(){
        let id_sub_kegiatan = $("select[name='sub_kegiatan']").val();
        let id_anggaran_edit = $("input[name='id_anggaran_edit']").val();
        if(id_sub_kegiatan){
            let banyak_akun = 0;
            if(id_anggaran_edit != ""){
                let input_akun = $(".loading-page .akun-clone");
                banyak_akun = input_akun.length + 1;
            }else{
                let input_akun = $(".loading-page .master-akun .akun-clone");
                banyak_akun = input_akun.length;
            }
            $("select[name='sub_rincian_objek_clone[]']").select2("destroy").end();
            $("select[name='sumber_pendanaan_0_clone[][]']").select2("destroy").end();
            let html_clone = $(".clone-model-akun").clone();
            $(html_clone).find("select[name='sub_rincian_objek_clone[]']").attr("name","sub_rincian_objek[]");
            $(html_clone).find("input").val("");
            $(html_clone).removeClass("d-none");
            $(html_clone).find("select[name='sumber_pendanaan_0_clone[][]']").attr("name","sumber_pendanaan_0[][]");
            $(html_clone).find("select[name='sumber_pendanaan_0[][]']").attr("name","sumber_pendanaan_"+banyak_akun+"[][]");
            $(html_clone).find("input[name='index_send_clone[]']").attr("name","index_send[]");
            $(html_clone).find("input[name='index_send[]']").val(banyak_akun);
            $(html_clone).find("input[name='nilai_0_clone[][]']").attr("name","nilai_0[][]");
            $(html_clone).find("input[name='nilai_0[][]']").attr("name","nilai_"+banyak_akun+"[][]");
            $(html_clone).find("input[name='nilai_anggaran_clone[]']").attr("name","nilai_anggaran_0[]");
            $(html_clone).find("input[name='nilai_anggaran_0[]']").attr("name","nilai_anggaran_"+banyak_akun+"[]");
            $(html_clone).find(".nilai-anggaran-0-clone").removeClass("nilai-anggaran-0-clone").addClass("nilai-anggaran-0");
            // $(html_clone).find(".nilai-anggaran-0").html("");
            $(html_clone).find(".input-currency-0").removeClass("input-currency-0").addClass("input-currency-"+banyak_akun);
            $(html_clone).find(".nilai-anggaran-0").removeClass("nilai-anggaran-0").addClass("nilai-anggaran-"+banyak_akun);
            $(html_clone).find(".clone-model-0-clone").removeClass("clone-model-0-clone").addClass("clone-model-0");
            $(html_clone).find(".clone-model-0").removeClass("clone-model-0").addClass("clone-model-"+banyak_akun);
            $(html_clone).find(".copy-model-0-clone").removeClass("copy-model-0-clone").addClass("copy-model-0");
            $(html_clone).find(".copy-model-0").removeClass("copy-model-0").addClass("copy-model-"+banyak_akun);
            $(html_clone).removeClass("clone-model-akun").addClass("clone-model-akun-"+banyak_akun);
            $(html_clone).find("[data-index='0']").attr("indexsubrincian",banyak_akun);
            $(html_clone).find("[data-index='0']").attr("data-index",banyak_akun);
            html_clone.find(".close-model-akun").append("<a href='#deleteModelAkun' class='delete-model-akun float-right' style='color:red;'><i class='icon-close2' style='font-size:20pt;color:red;'></i>Hapus Akun</a>");
            if(id_anggaran_edit != ""){
                $(html_clone).appendTo(".loading-page .copy-model-akun");
            }else{
                $(html_clone).appendTo(".loading-page .master-akun .copy-model-akun");
            }
            $("select[name='sub_rincian_objek[]']").select2();
            $("select[name='sub_rincian_objek_clone[]']").select2();
            $("select[name='sumber_pendanaan_"+banyak_akun+"[][]']").select2();
            $("select[name='sumber_pendanaan_0_clone[][]']").select2();
            new Cleave('.input-currency-'+banyak_akun, {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralDecimalMark: ',',
                delimiter: '.',
            });
    
            $(".delete-model-akun").on("click",function(e){
                e.preventDefault();
                $(this).parent().parent().parent().parent().parent().parent().remove();
            });
        }
    }

    $("form").submit(function(){
        let bool_form= true;
        let bool_form_2= true;
        let bool_form_3= true;
        let bool_form_4= true;
        let return_form = true;
        let arr_sub_rincian_objek = [];
        let arr_sumber_pendanaan = [];
        let nilai_pagu = 0;
        let total_rincian = 0;
        for(let i=0;i<$("form select[name^='sub_rincian_objek']").length;i++){
            arr_sumber_pendanaan = [];
            if(arr_sub_rincian_objek.hasOwnProperty($("form select[name^='sub_rincian_objek']")[i].value)){
                bool_form = false;
                break;
            }

            let index_sub_rincian_objek = $("form select[name^='sub_rincian_objek']")[i].attributes.indexsubrincian.value;

            arr_sub_rincian_objek[$("form select[name^='sub_rincian_objek']")[i].value] = $("form select[name^='sub_rincian_objek']")[i].value;
            
            for(let j = 0;j<$("form select[name='sumber_pendanaan_"+index_sub_rincian_objek+"[][]']").length;j++){
                if(arr_sumber_pendanaan.hasOwnProperty($("form select[name='sumber_pendanaan_"+index_sub_rincian_objek+"[][]'] option:selected")[j].attributes.idsumber.value)){
                    bool_form_2 = false;
                    break;
                }else{
                    arr_sumber_pendanaan[$("form select[name='sumber_pendanaan_"+index_sub_rincian_objek+"[][]'] option:selected")[j].attributes.idsumber.value] = $("form select[name='sumber_pendanaan_"+index_sub_rincian_objek+"[][]'] option:selected")[j].attributes.idsumber.value;
                }
            }

            let nilai_anggaran = $("input[name='nilai_anggaran_"+index_sub_rincian_objek+"[]']").val();
            let jumlah = 0;
            if(nilai_anggaran){
                for(let i=0;i<$("input[name='nilai_"+index_sub_rincian_objek+"[][]']").length;i++){
                    if($("input[name='nilai_"+index_sub_rincian_objek+"[][]']")[i].value != ""){
                        jumlah += parseInt($("input[name='nilai_"+index_sub_rincian_objek+"[][]']")[i].value.replaceAll(".",""));
                    }
                }

                if(jumlah > nilai_anggaran){
                    bool_form_3 = false;
                    nilai_pagu = nilai_anggaran;
                    total_rincian = jumlah;
                }
            }else{
                bool_form_4 = false;
            }
        }

        if(!bool_form){
            Swal.fire(
                'Gagal',
                'Tidak diperbolehkan pilihan sub rincian objek yang sama',
                'error'
            );
            return_form = false;
        }else if(!bool_form_2){
            Swal.fire(
                'Gagal',
                'Tidak diperbolehkan pilihan sumber pendanaan yang sama dalam 1 nomor rekening',
                'error'
            )
            return_form = false;
        }else if(!bool_form_3){
            Swal.fire(
                'Gagal',
                'Jumlah nilai rincian melebihi Pagu<br />Nilai Pagu = '+IDR(nilai_pagu).format(true)+'<br />Total Nilai Rincian = '+IDR(total_rincian).format(true),
                'error'
            )
            return_form = false;
        }else if(!bool_form_4){
            Swal.fire(
                'Gagal',
                'Sub Rincian Objek belum dipilih',
                'error'
            )
            return_form = false;
        }
        return return_form;
    })

    function calc_nilai(e){
        let nilai_anggaran = $("input[name='nilai_anggaran_"+$(e).data("index")+"[]']").val();
        let jumlah = 0;
        if(nilai_anggaran){
            for(let i=0;i<$("input[name='nilai_"+$(e).data("index")+"[][]']").length;i++){
                if($("input[name='nilai_"+$(e).data("index")+"[][]']")[i].value != ""){
                    jumlah += parseInt($("input[name='nilai_"+$(e).data("index")+"[][]']")[i].value.replaceAll(".",""));
                }
            }

            if(jumlah > nilai_anggaran){
                Swal.fire(
                    'Peringatan',
                    'Jumlah nilai rincian melebihi Pagu<br />Nilai Pagu = '+IDR(nilai_anggaran).format(true)+'<br />Total Nilai Rincian = '+IDR(jumlah).format(true),
                    'warning'
                )
            }
        }else{
            Swal.fire(
                'Gagal',
                'Sub Rincian Objek belum dipilih',
                'error'
            )
        }
    }

    function delete_akun_edit(id_trx_anggaran,e){
        //cek akun
        if($("form select[name^='sub_rincian_objek']").length == 1){
            //ditolak
            Swal.fire(
                'Gagal',
                'Tidak bisa dihapus',
                'error'
            )
        }else{
            var swalInit = swal.mixin({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });

            swalInit({
                title: 'Apakah anda yakin menghapus data ini?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya!',
                cancelButtonText: 'Batal!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'anggaran/delete_akun',
                        data: {
                            id_trx_anggaran: id_trx_anggaran
                        },
                        type: 'GET',
                        beforeSend: function() {
                            HoldOn.open(optionsHoldOn);
                        },
                        success: function(response) {
                            if (response) {
                                $(e).parent().parent().parent().parent().parent().parent().remove();
                                swalInit(
                                    'Berhasil',
                                    'Data sudah dihapus',
                                    'success'
                                );
                            } else {
                                $(e).parent().parent().parent().parent().parent().parent().remove();
                                swalInit(
                                    'Gagal',
                                    'Data tidak bisa dihapus',
                                    'error'
                                );
                            }
                        },
                        complete: function(response) {
                            HoldOn.close();
                        }
                    });
                } else if (result.dismiss === swal.DismissReason.cancel) {
                    swalInit(
                        'Batal',
                        'Data masih tersimpan!',
                        'error'
                    ).then(function(results) {
                        HoldOn.close();
                    });
                }
            });
        }
    }

    function delete_sumber_dana(id_trx_sumber,index,e){
        if($("form select[name='sumber_pendanaan_"+index+"[][]']").length == 1){
            //ditolak
            Swal.fire(
                'Gagal',
                'Tidak bisa dihapus',
                'error'
            )
        }else{
            var swalInit = swal.mixin({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });

            swalInit({
                title: 'Apakah anda yakin menghapus data ini?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya!',
                cancelButtonText: 'Batal!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: base_url + 'anggaran/delete_trx_sumber_dana',
                        data: {
                            id_trx_sumber: id_trx_sumber
                        },
                        type: 'GET',
                        beforeSend: function() {
                            HoldOn.open(optionsHoldOn);
                        },
                        success: function(response) {
                            if (response) {
                                $(e).parent().parent().parent().remove();
                                swalInit(
                                    'Berhasil',
                                    'Data sudah dihapus',
                                    'success'
                                );
                            } else {
                                $(e).parent().parent().parent().remove();
                                swalInit(
                                    'Gagal',
                                    'Data tidak bisa dihapus',
                                    'error'
                                );
                            }
                        },
                        complete: function(response) {
                            HoldOn.close();
                        }
                    });
                } else if (result.dismiss === swal.DismissReason.cancel) {
                    swalInit(
                        'Batal',
                        'Data masih tersimpan!',
                        'error'
                    ).then(function(results) {
                        HoldOn.close();
                    });
                }
            });
        }
    }
</script>