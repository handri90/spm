<style>
    .rght-border {
        border-right: 1px solid #ddd;
        padding-right: 20px;
    }

    #AnyTime--anytime-both {
        left: 330px !important;
        top: 50px !important;
    }

    .badge {
        display: block !important;
        width: max-content;
    }

    .info-penyerahan {
        text-align: left;
        line-height: 2;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <?php echo form_open("/anggaran/cetak"); ?>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">SKPD</label>
                <div class="col-lg-6">
                    <select class="form-control select-search" name="skpd" onChange="reload_datatable()">
                        <option value="">-- SEMUA SKPD --</option>
                        <?php
                        foreach($skpd as $value){
                            ?>
                            <option value="<?php echo encrypt_data($value->id_master_satker); ?>"><?php echo $value->nama; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Sumber Pendanaan</label>
                <div class="col-lg-6">
                    <select class="form-control select-search" name="sumber_pendanaan" onChange="reload_datatable()">
                        <option value="">-- SEMUA SUMBER PENDANAAN --</option>
                        <?php
                        foreach($sumber_dana_anggaran as $value){
                            ?>
                            <option value="<?php echo encrypt_data($value->id_master_sumber_pendanaan_anggaran); ?>"><?php echo $value->nama_sumber_pendanaan_anggaran; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="text-right">
                <button type="submit" href="<?php echo base_url() . 'anggaran/cetak'; ?>" class="btn btn-warning">Cetak</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'anggaran/tambah_anggaran'; ?>" class="btn btn-info">Tambah Sumber Pendanaan</a>
            </div>
        </div>
        <table id="datatableAnggaran" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Kode</th>
                    <th>Uraian</th>
                    <th>Sumber Pendanaan</th>
                    <th>Nilai Rincian</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    const IDR = value => currency(
        value, {
            symbol: "Rp. ",
            precision: 0,
            separator: "."
        });

    // let datatableAnggaran = $("#datatableAnggaran").DataTable();
    function reload_datatable() {
        $('#datatableAnggaran').DataTable().ajax.reload();
    }

    get_data_anggaran();
    function get_data_anggaran() {
        $("#datatableAnggaran").DataTable({
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "iDisplayLength":-1,
            ajax: {
                "url": base_url + 'anggaran/request/get_data_anggaran',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "dataSrc": '',
                "data": function(d) {
                    return $.extend({}, d, {
                        "master_satker_id": $("select[name='skpd']").val(),
                        "sumber_pendanaan_id": $("select[name='sumber_pendanaan']").val(),
                    });
                },
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columnDefs": [
                { "width": "20%", "targets": 0 },
                { "width": "10%", className:"text-right", "targets": 3 },
                { "width": "10%", "targets": 4 },
            ],
            "columns": [
                {
                    name:"pertama",
                },
                {
                    name:"kedua",
                },
                {
                    name:"ketiga",
                },
                {
                    name:"keempat",
                },
                {
                    name:"kelima"   
                },
            ],
            rowsGroup: [4]
        });
        // datatableAnggaran.clear().draw();
        // $.ajax({
		// 	url: base_url + 'anggaran/request/get_data_anggaran',
		// 	type: 'GET',
		// 	beforeSend: function() {
		// 		// loading_start();
		// 	},
		// 	success: function(response) {
		// 		let no = 1;
        //         let before_id_program = 0;
        //         let before_id_kegiatan = 0;
        //         let before_id_sub_kegiatan = 0;
		// 		$.each(response, function(index, value) {
        //             if(before_id_program == 0 || before_id_program != value.id_master_program){
        //                 datatableAnggaran.row.add([
        //                     "",
        //                     value.nama_program,
        //                     "",
        //                     "",
        //                     "<a href='" + base_url + "anggaran/edit_anggaran/" + value.id_encrypt + "' class='btn btn-primary btn-icon btn-sm'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon btn-sm' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>",
        //                 ]).draw(false);
        //             }
        //             before_id_program = value.id_master_program;
                    
        //             if(before_id_kegiatan == 0 || before_id_kegiatan != value.id_master_kegiatan){
        //                 datatableAnggaran.row.add([
        //                     "",
        //                     value.nama_kegiatan,
        //                     "",
        //                     "",
        //                     "",
        //                 ]).draw(false);
        //             }
        //             before_id_kegiatan = value.id_master_kegiatan;
                    
        //             if(before_id_sub_kegiatan == 0 || before_id_sub_kegiatan != value.id_master_sub_kegiatan){
        //                 datatableAnggaran.row.add([
        //                     "",
        //                     value.nama_sub_kegiatan,
        //                     "",
        //                     "",
        //                     "",
        //                 ]).draw(false);
        //             }
        //             before_id_sub_kegiatan = value.id_master_sub_kegiatan;

		// 			datatableAnggaran.row.add([
		// 				"",
		// 				value.uraian_sub_rincian_objek,
		// 				value.nama_sumber_pendanaan_anggaran,
		// 				value.nilai_rincian,
		// 				"",
		// 			]).draw(false);
		// 			no++;
		// 		});
		// 	},
		// 	complete: function() {
		// 		// loading_stop();
		// 	}
		// });
    }

    // // setInterval(get_data_spm, 5000);

    function confirm_delete(id_anggaran) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'anggaran/delete_anggaran',
                    data: {
                        id_anggaran: id_anggaran
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $('#datatableAnggaran').DataTable().ajax.reload();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            $('#datatableAnggaran').DataTable().ajax.reload();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        $('#datatableAnggaran').DataTable().ajax.reload();
                    }
                });
            }
        });
    }
</script>