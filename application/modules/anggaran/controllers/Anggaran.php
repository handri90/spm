<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anggaran extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('master_satker/master_satker_model','master_satker_model');
        $this->load->model('anggaran_model');
        $this->load->model('import_spm/master_sub_kegiatan_model', 'master_sub_kegiatan_model');
        $this->load->model('master_sumber_pendanaan_anggaran/master_sumber_pendanaan_anggaran_model', 'master_sumber_pendanaan_anggaran_model');
        $this->load->model('import_spm/referensi_spm_model', 'referensi_spm_model');
        // $this->load->model('master_rekanan/master_rekanan_model', 'master_rekanan_model');
        $this->load->model('trx_anggaran_model');
        $this->load->model('trx_sumber_pendanaan_anggaran_model');
        $this->load->model('import_spm/master_kode_sub_rincian_objek_model', 'master_kode_sub_rincian_objek_model');
    }

    public function index()
    {

        $data['breadcrumb'] = [['link' => false, 'content' => 'Anggaran', 'is_active' => true]];
        if($this->session->userdata("level_user_id") == "7"){
            $this->execute('index', $data);
        }else{
            $data['skpd'] = $this->master_satker_model->get(
                array(
                    "order_by" => array(
                        "nama" => "ASC"
                    )
                )
            );

            $data['sumber_dana_anggaran'] = $this->master_sumber_pendanaan_anggaran_model->get(
                array(
                    "order_by"=>array(
                        "kode_sumber_pendanaan_anggaran"=>"ASC"
                    )
                )
            );

            $this->execute('index_anggaran', $data);
        }
    }

    public function cetak(){
        $where_arr_skpd_anggaran = [];
        if($this->ipost("skpd") != ""){
            $where_arr_skpd_anggaran["anggaran.master_satker_id"] = decrypt_data($this->ipost("skpd"));
        }else if($this->session->userdata("master_satker_id") != ""){
            $where_arr_skpd_anggaran["anggaran.master_satker_id"] = $this->session->userdata("master_satker_id");
        }
        
        if($this->ipost("sumber_pendanaan") != ""){
            $where_arr_skpd_anggaran["master_sumber_pendanaan_anggaran_id"] = decrypt_data($this->ipost("sumber_pendanaan"));
        }

        // if(!array_key_exists("sumber_pendanaan",$where_arr_skpd_anggaran)){
        //     $data_skpd = $this->master_satker_model->get_by($this->session->userdata("master_satker_id"));
        // }

        require(APPPATH . 'third_party/PHPExcel-1.8/Classes/PHPExcel.php');

        $filename = "Laporan.xlsx";

        $objPHPExcel = new PHPExcel();

        $style_header = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'font' => array(
                'size' => 11,
            )
        );

        $style_program = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
                'font' => array(
                'size' => 11,
                'bold' => true
            )
        );
        
        $style_jumlah = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
                'font' => array(
                'size' => 11,
                'bold' => true
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
        );
        
        $style_data = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
                'font' => array(
                'size' => 11,
            )
        );
        
        $style_header_table = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            ),
                'font' => array(
                'size' => 11,
            )
        );

        $objPHPExcel->getActiveSheet()->SetCellValue("A1", "PEMERINTAH KABUPATEN KOTAWARINGIN BARAT");
        $objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValue("A2", "LAPORAN PENGGUNAAN SUMBER PENDANAAN APBD BERDASARKAN SKPD");
        $objPHPExcel->getActiveSheet()->getStyle("A2")->applyFromArray($style_header);
        $objPHPExcel->getActiveSheet()->SetCellValue("A3", "TAHUN ANGGARAN ".$this->session->userdata("tahun"));
        $objPHPExcel->getActiveSheet()->getStyle("A3")->applyFromArray($style_header);
        
        $objPHPExcel->getActiveSheet()->mergeCells("A1:E1");
        $objPHPExcel->getActiveSheet()->mergeCells("A2:E2");
        $objPHPExcel->getActiveSheet()->mergeCells("A3:E3");
        
        if(!array_key_exists("master_sumber_pendanaan_anggaran_id",$where_arr_skpd_anggaran)){
           //laporan anggaran per skpd
           $skpd = $this->master_satker_model->get(
                array(
                    "fields"=>"master_satker.*",
                    "join"=>array(
                        "anggaran"=>"id_master_satker=master_satker_id"
                    ),
                    "where"=>$where_arr_skpd_anggaran,
                    "group_by"=>"id_master_satker"
                )
           );

           if($skpd){
                $row_skpd = 5;
                foreach ($skpd as $key_skpd => $value_skpd) {

                    $data = $this->anggaran_model->get(
                        array(
                            "fields"=>"id_master_program,
                            kode_program,
                            nama_program,
                            id_master_kegiatan,
                            kode_kegiatan,
                            nama_kegiatan,
                            id_master_sub_kegiatan,
                            kode_sub_kegiatan,
                            nama_sub_kegiatan,
                            kode_bidang_urusan,
                            kode_urusan,
                            kode_sub_rincian_objek,
                            uraian_sub_rincian_objek,
                            nama_sumber_pendanaan_anggaran,
                            nilai_rincian,
                            CONCAT(kode_akun,'.',kode_kelompok,'.',kode_jenis,'.',kode_objek,'.',kode_rincian_objek,'.',master_kode_rincian_objek_id) AS nomor_rekening",
                            "join"=>array(
                                "trx_anggaran"=>"id_anggaran = anggaran_id",
                                "trx_sumber_pendanaan_anggaran"=>"id_trx_anggaran = trx_anggaran_id",
                                "master_sumber_pendanaan_anggaran"=>"id_master_sumber_pendanaan_anggaran = master_sumber_pendanaan_anggaran_id",
                                "referensi_spm"=>"id_referensi_spm = referensi_spm_id",
                                "master_sub_kegiatan"=>"id_master_sub_kegiatan = anggaran.master_sub_kegiatan_id",
                                "master_kegiatan"=>"id_master_kegiatan = referensi_spm.master_kegiatan_id",
                                "master_program"=>"id_master_program = referensi_spm.master_program_id",
                                "master_bidang_urusan"=>"id_master_bidang_urusan = referensi_spm.master_bidang_urusan_id",
                                "master_urusan"=>"id_master_urusan = referensi_spm.master_urusan_id",
                                "master_kode_sub_rincian_objek"=>"id_master_kode_sub_rincian_objek = trx_anggaran.master_kode_sub_rincian_objek_id",
                                "master_kode_rincian_objek"=>"id_master_kode_rincian_objek = master_kode_rincian_objek_id",
                                "master_kode_objek"=>"id_master_kode_objek = master_kode_objek_id",
                                "master_kode_jenis"=>"id_master_kode_jenis = master_kode_jenis_id",
                                "master_kode_kelompok"=>"id_master_kode_kelompok = master_kode_kelompok_id",
                                "master_kode_akun"=>"id_master_kode_akun = master_kode_akun_id",
                            ),
                            "where"=>array(
                                "anggaran.master_satker_id"=>$value_skpd->id_master_satker
                            ),
                            "order_by"=>array(
                                "id_master_program,id_master_kegiatan,id_master_sub_kegiatan,id_master_kode_sub_rincian_objek,id_master_sumber_pendanaan_anggaran"=>"ASC"
                            )
                        )
                    );
                    
                    $objPHPExcel->getActiveSheet()->SetCellValue("A".$row_skpd, "Kode SKPD");
                    $objPHPExcel->getActiveSheet()->SetCellValue("B".$row_skpd, ": ".$value_skpd->kode_skpd_terbaru);
                    $row_skpd++;
                    $objPHPExcel->getActiveSheet()->SetCellValue("A".$row_skpd, "Nama SKPD");
                    $objPHPExcel->getActiveSheet()->SetCellValue("B".$row_skpd, ": ".$value_skpd->nama);
                    $row_skpd++;
                    $row_skpd++;
                    $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row_skpd, "Kode");
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $row_skpd)->applyFromArray($style_header_table);
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row_skpd, "Uraian");
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row_skpd)->applyFromArray($style_header_table);
                    $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row_skpd, "Sumber Pendanaan");
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row_skpd)->applyFromArray($style_header_table);
                    $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row_skpd, "Nilai Rincian");
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row_skpd)->applyFromArray($style_header_table);
                    $row_skpd++;

                    $row = $row_skpd;
                    $before_id_program = 0;
                    $before_id_kegiatan = 0;
                    $before_id_sub_kegiatan = 0;
                    $jumlah = 0;
                    foreach ($data as $key => $value) {
                        if($before_id_program == 0 || $before_id_program != $value->id_master_program){
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $value->kode_urusan.".".$value->kode_bidang_urusan.".".$value->kode_program);
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_program);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_program);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_program);
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                            $row++;
                        }
                        $before_id_program = $value->id_master_program;
                        
                        
                        if($before_id_kegiatan == 0 || $before_id_kegiatan != $value->id_master_kegiatan){
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $value->kode_urusan.".".$value->kode_bidang_urusan.".".$value->kode_program.".".$value->kode_kegiatan);
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_kegiatan);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                            $row++;
                        }
                        $before_id_kegiatan = $value->id_master_kegiatan;

                        if($before_id_sub_kegiatan == 0 || $before_id_sub_kegiatan != $value->id_master_sub_kegiatan){
                            $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $value->kode_urusan.".".$value->kode_bidang_urusan.".".$value->kode_program.".".$value->kode_kegiatan.".".$value->kode_sub_kegiatan);
                            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                            $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_sub_kegiatan);
                            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                            $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                            $row++;
                        }
                        $before_id_sub_kegiatan = $value->id_master_sub_kegiatan;
                        
                        $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $value->kode_urusan.".".$value->kode_bidang_urusan.".".$value->kode_program.".".$value->kode_kegiatan.".".$value->kode_sub_kegiatan." ".$value->nomor_rekening);
                        $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                        $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->uraian_sub_rincian_objek);
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                        $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, $value->nama_sumber_pendanaan_anggaran);
                        $objPHPExcel->getActiveSheet()->SetCellValue("D" . $row, nominal($value->nilai_rincian));
                        $objPHPExcel->getActiveSheet()
                        ->getStyle("D" . $row)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                            "D".$row,
                            nominal($value->nilai_rincian),
                            PHPExcel_Cell_DataType::TYPE_STRING
                        );
                        $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);  
                        $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_data);
                        $jumlah = $jumlah + $value->nilai_rincian;
                        $row++;
                    }
                    
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->SetCellValue("C".$row, "Jumlah");
                    $objPHPExcel->getActiveSheet()->SetCellValue("D".$row, nominal($jumlah));
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_jumlah);  
                    $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->applyFromArray($style_program);
                    $objPHPExcel->getActiveSheet()
                        ->getStyle("D" . $row)
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                        "D".$row,
                        nominal($jumlah),
                        PHPExcel_Cell_DataType::TYPE_STRING
                    );
                    $row++;
                    $row++;
                    $row_skpd = $row;
                }
           }else{
                $objPHPExcel->getActiveSheet()->SetCellValue("A4", "Belum Ada Data");
                $objPHPExcel->getActiveSheet()->mergeCells("A4:E4");
                $objPHPExcel->getActiveSheet()->getStyle("A4")->applyFromArray($style_header);
           }
        }else{
            //laporan sumber dana
            $data = $this->anggaran_model->get(
                array(
                    "fields"=>"id_master_program,
                    kode_program,
                    nama_program,
                    id_master_kegiatan,
                    kode_kegiatan,
                    nama_kegiatan,
                    id_master_sub_kegiatan,
                    kode_sub_kegiatan,
                    nama_sub_kegiatan,
                    kode_bidang_urusan,
                    kode_urusan,
                    kode_sub_rincian_objek,
                    uraian_sub_rincian_objek,
                    nama_sumber_pendanaan_anggaran,
                    nilai_rincian,
                    CONCAT(kode_akun,'.',kode_kelompok,'.',kode_jenis,'.',kode_objek,'.',kode_rincian_objek,'.',master_kode_rincian_objek_id) AS nomor_rekening,
                    id_master_satker,
                    kode_skpd_terbaru,
                    master_satker.nama AS nama_skpd",
                    "join"=>array(
                        "trx_anggaran"=>"id_anggaran = anggaran_id",
                        "trx_sumber_pendanaan_anggaran"=>"id_trx_anggaran = trx_anggaran_id",
                        "master_sumber_pendanaan_anggaran"=>"id_master_sumber_pendanaan_anggaran = master_sumber_pendanaan_anggaran_id",
                        "referensi_spm"=>"id_referensi_spm = referensi_spm_id",
                        "master_sub_kegiatan"=>"id_master_sub_kegiatan = anggaran.master_sub_kegiatan_id",
                        "master_kegiatan"=>"id_master_kegiatan = referensi_spm.master_kegiatan_id",
                        "master_program"=>"id_master_program = referensi_spm.master_program_id",
                        "master_bidang_urusan"=>"id_master_bidang_urusan = referensi_spm.master_bidang_urusan_id",
                        "master_urusan"=>"id_master_urusan = referensi_spm.master_urusan_id",
                        "master_kode_sub_rincian_objek"=>"id_master_kode_sub_rincian_objek = trx_anggaran.master_kode_sub_rincian_objek_id",
                        "master_kode_rincian_objek"=>"id_master_kode_rincian_objek = master_kode_rincian_objek_id",
                        "master_kode_objek"=>"id_master_kode_objek = master_kode_objek_id",
                        "master_kode_jenis"=>"id_master_kode_jenis = master_kode_jenis_id",
                        "master_kode_kelompok"=>"id_master_kode_kelompok = master_kode_kelompok_id",
                        "master_kode_akun"=>"id_master_kode_akun = master_kode_akun_id",
                        "master_satker"=>"id_master_satker = anggaran.master_satker_id",
                    ),
                    "where"=>$where_arr_skpd_anggaran,
                    "order_by"=>array(
                        "id_master_program,id_master_kegiatan,id_master_sub_kegiatan,id_master_kode_sub_rincian_objek,id_master_sumber_pendanaan_anggaran"=>"ASC"
                    )
                )
            );

            $sumber_dana = $this->master_sumber_pendanaan_anggaran_model->get_by(decrypt_data($this->ipost("sumber_pendanaan")));
            
            $objPHPExcel->getActiveSheet()->SetCellValue("A5", "Kode");
            $objPHPExcel->getActiveSheet()->SetCellValue("B5", ": ".$sumber_dana->kode_sumber_pendanaan_anggaran);
            $objPHPExcel->getActiveSheet()->SetCellValue("A6", "Sumber Pendanaan");
            $objPHPExcel->getActiveSheet()->SetCellValue("B6", ": ".$sumber_dana->nama_sumber_pendanaan_anggaran);

            $objPHPExcel->getActiveSheet()->SetCellValue("A8", "Kode");
            $objPHPExcel->getActiveSheet()->getStyle("A8")->applyFromArray($style_header_table);
            $objPHPExcel->getActiveSheet()->SetCellValue("B8", "Uraian");
            $objPHPExcel->getActiveSheet()->getStyle("B8")->applyFromArray($style_header_table);
            $objPHPExcel->getActiveSheet()->SetCellValue("C8", "Nilai Rincian");
            $objPHPExcel->getActiveSheet()->getStyle("C8")->applyFromArray($style_header_table);

            $row = 9;
            $id_master_satker_before = 0;
            $before_id_program = 0;
            $before_id_kegiatan = 0;
            $before_id_sub_kegiatan = 0;
            $jumlah = 0;
            foreach ($data as $key => $value) {
                if($id_master_satker_before == 0 || $id_master_satker_before != $value->id_master_satker){
                    $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $value->kode_skpd_terbaru);
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_program);
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_skpd);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_program);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $row++;
                }
                $id_master_satker_before = $value->id_master_satker;

                if($before_id_program == 0 || $before_id_program != $value->id_master_program){
                    $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $value->kode_urusan.".".$value->kode_bidang_urusan.".".$value->kode_program);
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_program);
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_program);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_program);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $row++;
                }
                $before_id_program = $value->id_master_program;
                
                
                if($before_id_kegiatan == 0 || $before_id_kegiatan != $value->id_master_kegiatan){
                    $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $value->kode_urusan.".".$value->kode_bidang_urusan.".".$value->kode_program.".".$value->kode_kegiatan);
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_kegiatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $row++;
                }
                $before_id_kegiatan = $value->id_master_kegiatan;

                if($before_id_sub_kegiatan == 0 || $before_id_sub_kegiatan != $value->id_master_sub_kegiatan){
                    $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $value->kode_urusan.".".$value->kode_bidang_urusan.".".$value->kode_program.".".$value->kode_kegiatan.".".$value->kode_sub_kegiatan);
                    $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->nama_sub_kegiatan);
                    $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                    $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);
                    $row++;
                }
                $before_id_sub_kegiatan = $value->id_master_sub_kegiatan;
                
                $objPHPExcel->getActiveSheet()->SetCellValue("A" . $row, $value->kode_urusan.".".$value->kode_bidang_urusan.".".$value->kode_program.".".$value->kode_kegiatan.".".$value->kode_sub_kegiatan." ".$value->nomor_rekening);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->SetCellValue("B" . $row, $value->uraian_sub_rincian_objek);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_data);
                $objPHPExcel->getActiveSheet()->SetCellValue("C" . $row, nominal($value->nilai_rincian));
                $objPHPExcel->getActiveSheet()
                ->getStyle("C" . $row)
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                    "C".$row,
                    nominal($value->nilai_rincian),
                    PHPExcel_Cell_DataType::TYPE_STRING
                );
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_data);  
                $jumlah = $jumlah + $value->nilai_rincian;
                $row++;
            }
            
            $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->applyFromArray($style_data);
            $objPHPExcel->getActiveSheet()->SetCellValue("B".$row, "Jumlah");
            $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->applyFromArray($style_jumlah);
            $objPHPExcel->getActiveSheet()->SetCellValue("C".$row, nominal($jumlah));
            $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->applyFromArray($style_jumlah);
            $objPHPExcel->getActiveSheet()
                ->getStyle("C" . $row)
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit(
                "C".$row,
                nominal($jumlah),
                PHPExcel_Cell_DataType::TYPE_STRING
            );
            $row_skpd = $row;
        }

        $jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
        $jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

        $col = 'A';
        while (true) {
            $tempCol = $col++;
            $objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
            if ($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()) {
                break;
            }
        }

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $writer->save('php://output');
        exit;
    }

    public function tambah_anggaran($master_satker = "",$sub_kegiatan = "")
    {
        if (empty($_POST)) {
            $data['sumber_dana_anggaran'] = $this->master_sumber_pendanaan_anggaran_model->get(
                array(
                    "order_by"=>array(
                        "kode_sumber_pendanaan_anggaran"=>"ASC"
                    )
                )
            );

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'Anggaran', 'content' => 'Anggaran', 'is_active' => false], ['link' => false, 'content' => 'Tambah SPM', 'is_active' => true]];

            if($this->session->userdata("level_user_id") == "7"){
                $data['list_sub_kegiatan'] = $this->master_sub_kegiatan_model->get(
                    array(
                        "fields" => "master_sub_kegiatan.*,nama_program,nama_kegiatan",
                        "join"   => array(
                            "master_kegiatan" => "id_master_kegiatan=master_kegiatan_id",
                            "master_program"  => "id_master_program=master_program_id",
                            "referensi_spm"  => "id_master_sub_kegiatan=master_sub_kegiatan_id",
                        ),
                        'order_by' => array(
                            'nama_program'      => 'ASC',
                            'nama_kegiatan'     => 'ASC',
                            'nama_sub_kegiatan' => 'ASC'
                        ),
                        "where"=>array(
                            "master_satker_id"=>$this->session->userdata('master_satker_id')
                        ),
                        "group_by"=>"referensi_spm.master_sub_kegiatan_id,referensi_spm.master_kegiatan_id,referensi_spm.master_program_id"
                    )
                );
                $data["sub_kegiatan_after"] = $sub_kegiatan;
                $this->execute('form_anggaran', $data);
                
            }else{
                $data['skpd'] = $this->master_satker_model->get(
                    array(
                        "order_by" => array(
                            "nama" => "ASC"
                            )
                        )
                    );
                $data["master_satker_after"] = $master_satker;
                $data["sub_kegiatan_after"] = $sub_kegiatan;
                $this->execute('form_admin_anggaran', $data);
            }
        } else {
            $master_satker_id = $this->session->userdata('master_satker_id');
            if($this->ipost("skpd") != ""){
                $master_satker_id = decrypt_data($this->ipost("skpd"));
            }

            if($this->ipost("id_anggaran_edit") != ""){
                $data = $this->anggaran_model->get(
                    array(
                        "where"=>array(
                            "id_anggaran"=>decrypt_data($this->ipost("id_anggaran_edit"))
                        ),
                        "join"=>array(
                            "trx_anggaran"=>"anggaran_id=id_anggaran",
                            "trx_sumber_pendanaan_anggaran"=>"trx_anggaran_id=id_trx_anggaran",
                        )
                    ),"row"
                );
    
                $this->trx_sumber_pendanaan_anggaran_model->query("DELETE FROM trx_sumber_pendanaan_anggaran WHERE trx_anggaran_id=".$data->trx_anggaran_id);
                $this->trx_anggaran_model->query("DELETE FROM trx_anggaran WHERE anggaran_id=".decrypt_data($this->ipost("id_anggaran_edit")));

                $anggaran = decrypt_data($this->ipost("id_anggaran_edit"));
            }else{
                
                $data_anggaran = [
                    "master_sub_kegiatan_id"=>decrypt_data($this->ipost('sub_kegiatan')),
                    "master_satker_id"=>$master_satker_id,
                    "created_at"=>$this->datetime(),
                    "id_user_created"=>$this->session->userdata('id_user'),
                ];
    
                $anggaran = $this->anggaran_model->save($data_anggaran);
            }

            foreach($this->ipost("sub_rincian_objek") as $key=>$value){
                $data_referensi_spm = $this->referensi_spm_model->get(
                    array(
                        "where" => array(
                            "master_sub_kegiatan_id"           => decrypt_data($this->ipost('sub_kegiatan')),
                            "master_kode_sub_rincian_objek_id" => $value,
                        )
                    ),
                    "row"
                );

                $data_trx_anggaran = [
                    "anggaran_id"=>$anggaran,
                    "referensi_spm_id"=>$data_referensi_spm->id_referensi_spm,
                    "master_kode_sub_rincian_objek_id"=>$value,
                    "created_at"=>$this->datetime(),
                    "id_user_created"=>$this->session->userdata('id_user'),
                ];

                $trx_anggaran = $this->trx_anggaran_model->save($data_trx_anggaran);
                foreach ($this->ipost("sumber_pendanaan_".$this->ipost("index_send")[$key]) as $key_sumber => $value_sumber) {
                    $data_trx_sumber_pendanaan_anggaran = [
                        "trx_anggaran_id"=>$trx_anggaran,
                        "master_sumber_pendanaan_anggaran_id"=>decrypt_data($value_sumber[0]),
                        "nilai_rincian"=>str_replace(".","",$this->ipost("nilai_".$this->ipost("index_send")[$key])[$key_sumber][0]),
                        "created_at"=>$this->datetime(),
                        "id_user_created"=>$this->session->userdata('id_user'),
                    ];
    
                    $trx_sumber_pendanaan_anggaran = $this->trx_sumber_pendanaan_anggaran_model->save($data_trx_sumber_pendanaan_anggaran);
                }
            }

            redirect('anggaran/tambah_anggaran/'.$master_satker_id.'/'.decrypt_data($this->ipost('sub_kegiatan')));
        }
    }

    public function edit_anggaran($id_anggaran)
    {
        $data_master = $this->anggaran_model->get_by(decrypt_data($id_anggaran));

        if (!$data_master) {
            $this->page_error();
        }

        // //cek trx
        if (empty($_POST)) {
            $data['content']           = $data_master;

            $data['sub_rincian_objek'] = $this->master_kode_sub_rincian_objek_model->get(
                array(
                    "fields"=>"master_kode_sub_rincian_objek.*,nilai_anggaran",
                    "order_by" => array(
                        "uraian_sub_rincian_objek" => "ASC"
                    ),
                    "join"=>array(
                        "referensi_spm"=>"id_master_kode_sub_rincian_objek=master_kode_sub_rincian_objek_id"
                    ),
                    "where"=>array(
                        "master_satker_id"=>$data_master->master_satker_id,
                        "master_sub_kegiatan_id"=>$data_master->master_sub_kegiatan_id,
                    ),
                    "where_false" => "referensi_spm.deleted_at is null"
                )
            );

            $data['list_sub_kegiatan'] = $this->master_sub_kegiatan_model->get(
                array(
                    "fields" => "master_sub_kegiatan.*,nama_program,nama_kegiatan",
                    "join"   => array(
                        "master_kegiatan" => "id_master_kegiatan=master_kegiatan_id",
                        "master_program"  => "id_master_program=master_program_id",
                        "referensi_spm"  => "id_master_sub_kegiatan=master_sub_kegiatan_id",
                    ),
                    'order_by' => array(
                        'nama_program'      => 'ASC',
                        'nama_kegiatan'     => 'ASC',
                        'nama_sub_kegiatan' => 'ASC'
                    ),
                    "where"=>array(
                        "master_satker_id"=>$data_master->master_satker_id
                    ),
                    "group_by"=>"referensi_spm.master_sub_kegiatan_id,referensi_spm.master_kegiatan_id,referensi_spm.master_program_id"
                )
            );

            $data['sumber_dana_anggaran'] = $this->master_sumber_pendanaan_anggaran_model->get(
                array(
                    "order_by"=>array(
                        "kode_sumber_pendanaan_anggaran"=>"ASC"
                    )
                )
            );

            $data['skpd'] = $this->master_satker_model->get(
            array(
                "order_by" => array(
                    "nama" => "ASC"
                    )
                )
            );

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'Anggaran', 'content' => 'Anggaran', 'is_active' => false], ['link' => false, 'content' => 'Ubah SPM', 'is_active' => true]];

            if($this->session->userdata("level_user_id") == "7"){
                $this->execute('form_anggaran', $data);
            }else{
                $this->execute('form_admin_anggaran', $data);
            }
        } else {
            $data = $this->anggaran_model->get(
                array(
                    "where"=>array(
                        "id_anggaran"=>decrypt_data($id_anggaran)
                    ),
                    "join"=>array(
                        "trx_anggaran"=>"anggaran_id=id_anggaran",
                        "trx_sumber_pendanaan_anggaran"=>"trx_anggaran_id=id_trx_anggaran",
                    )
                ),"row"
            );

            $this->trx_sumber_pendanaan_anggaran_model->query("DELETE FROM trx_sumber_pendanaan_anggaran WHERE trx_anggaran_id=".$data->trx_anggaran_id);
            $this->trx_anggaran_model->query("DELETE FROM trx_anggaran WHERE anggaran_id=".decrypt_data($id_anggaran));
            
            foreach($this->ipost("sub_rincian_objek") as $key=>$value){
                $data_referensi_spm = $this->referensi_spm_model->get(
                    array(
                        "where" => array(
                            "master_sub_kegiatan_id"           => $data_master->master_sub_kegiatan_id,
                            "master_kode_sub_rincian_objek_id" => $value,
                        )
                    ),
                    "row"
                );

                $data_trx_anggaran = [
                    "anggaran_id"=>decrypt_data($id_anggaran),
                    "referensi_spm_id"=>$data_referensi_spm->id_referensi_spm,
                    "master_kode_sub_rincian_objek_id"=>$value,
                    "created_at"=>$this->datetime(),
                ];

                $trx_anggaran = $this->trx_anggaran_model->save($data_trx_anggaran);
                foreach ($this->ipost("sumber_pendanaan_".$this->ipost("index_send")[$key]) as $key_sumber => $value_sumber) {
                    $data_trx_sumber_pendanaan_anggaran = [
                        "trx_anggaran_id"=>$trx_anggaran,
                        "master_sumber_pendanaan_anggaran_id"=>decrypt_data($value_sumber[0]),
                        "nilai_rincian"=>str_replace(".","",$this->ipost("nilai_".$this->ipost("index_send")[$key])[$key_sumber][0]),
                        "created_at"=>$this->datetime(),
                    ];
    
                    $trx_sumber_pendanaan_anggaran = $this->trx_sumber_pendanaan_anggaran_model->save($data_trx_sumber_pendanaan_anggaran);
                }
            }

            if($this->session->userdata("level_user_id") == "7"){
                redirect('anggaran/edit_anggaran/'.$id_anggaran);
            }else{
                redirect('anggaran/edit_anggaran/'.$id_anggaran);
            }
        }
    }

    public function delete_akun()
    {
        $id_trx_anggaran      = $this->iget('id_trx_anggaran');
        $data_master = $this->trx_anggaran_model->get_by(decrypt_data($id_trx_anggaran));

        if (!$data_master) {
            $this->page_error();
        }

        $this->trx_sumber_pendanaan_anggaran_model->query("DELETE FROM trx_sumber_pendanaan_anggaran WHERE trx_anggaran_id=".decrypt_data($id_trx_anggaran));
        $status = $this->trx_anggaran_model->query("DELETE FROM trx_anggaran WHERE id_trx_anggaran=".decrypt_data($id_trx_anggaran));

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
    
    public function delete_trx_sumber_dana()
    {
        $id_trx_sumber      = decrypt_data($this->iget('id_trx_sumber'));
        $data_master = $this->trx_sumber_pendanaan_anggaran_model->get_by($id_trx_sumber);

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->trx_sumber_pendanaan_anggaran_model->query("DELETE FROM trx_sumber_pendanaan_anggaran WHERE id_trx_sumber_pendanaan_anggaran=".$id_trx_sumber);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
    
    public function delete_anggaran()
    {
        $id_anggaran      = decrypt_data($this->iget('id_anggaran'));
        $data_master = $this->anggaran_model->get_by($id_anggaran);
        $data_trx = $this->trx_anggaran_model->get(
            array(
                "where"=>array(
                    "anggaran_id"=>$id_anggaran
                )
            )
        );
        
        if (!$data_master) {
            $this->page_error();
        }
        
        foreach ($data_trx as $key => $value) {
            $this->trx_sumber_pendanaan_anggaran_model->query("DELETE FROM trx_sumber_pendanaan_anggaran WHERE trx_anggaran_id=".$value->id_trx_anggaran);
        }
        
        $this->trx_anggaran_model->query("DELETE FROM trx_anggaran WHERE anggaran_id=".$id_anggaran);
        $status = $this->trx_sumber_pendanaan_anggaran_model->query("DELETE FROM anggaran WHERE id_anggaran=".$id_anggaran);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
