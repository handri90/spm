<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('anggaran_model');
        $this->load->model('trx_anggaran_model');
        $this->load->model('trx_sumber_pendanaan_anggaran_model');
        $this->load->model('import_spm/master_kode_sub_rincian_objek_model', 'master_kode_sub_rincian_objek_model');
        $this->load->model('master_sumber_pendanaan_anggaran/master_sumber_pendanaan_anggaran_model', 'master_sumber_pendanaan_anggaran_model');
        $this->load->model('import_spm/master_sub_kegiatan_model', 'master_sub_kegiatan_model');
        // $this->load->model('master_rekanan/master_rekanan_model', 'master_rekanan_model');
    }

    public function get_data_anggaran()
    {
        $where_arr_skpd_anggaran = [];
        if($this->iget("master_satker_id") != ""){
            $where_arr_skpd_anggaran["anggaran.master_satker_id"] = decrypt_data($this->iget("master_satker_id"));
        }else if($this->session->userdata("master_satker_id") != ""){
            $where_arr_skpd_anggaran["anggaran.master_satker_id"] = $this->session->userdata("master_satker_id");
        }
        
        if($this->iget("sumber_pendanaan_id") != ""){
            $where_arr_skpd_anggaran["master_sumber_pendanaan_anggaran_id"] = decrypt_data($this->iget("sumber_pendanaan_id"));
        }

        $data_anggaran = $this->anggaran_model->get(
            array(
                "fields"=>"
                id_anggaran,id_master_program,
                kode_program,
                nama_program,
                id_master_kegiatan,
                kode_kegiatan,
                nama_kegiatan,
                id_master_sub_kegiatan,
                kode_sub_kegiatan,
                nama_sub_kegiatan,
                kode_bidang_urusan,
                kode_urusan,
                kode_sub_rincian_objek,
                uraian_sub_rincian_objek,
                nama_sumber_pendanaan_anggaran,
                nilai_rincian,
                CONCAT(kode_akun,'.',kode_kelompok,'.',kode_jenis,'.',kode_objek,'.',kode_rincian_objek,'.',master_kode_rincian_objek_id) AS nomor_rekening,
                id_master_satker,
                kode_skpd_terbaru,
                master_satker.nama AS nama_skpd",
                "join"=>array(
                    "trx_anggaran"=>"id_anggaran = anggaran_id",
                    "trx_sumber_pendanaan_anggaran"=>"id_trx_anggaran = trx_anggaran_id",
                    "master_sumber_pendanaan_anggaran"=>"id_master_sumber_pendanaan_anggaran = master_sumber_pendanaan_anggaran_id",
                    "referensi_spm"=>"id_referensi_spm = referensi_spm_id",
                    "master_sub_kegiatan"=>"id_master_sub_kegiatan = anggaran.master_sub_kegiatan_id",
                    "master_kegiatan"=>"id_master_kegiatan = referensi_spm.master_kegiatan_id",
                    "master_program"=>"id_master_program = referensi_spm.master_program_id",
                    "master_bidang_urusan"=>"id_master_bidang_urusan = referensi_spm.master_bidang_urusan_id",
                    "master_urusan"=>"id_master_urusan = referensi_spm.master_urusan_id",
                    "master_kode_sub_rincian_objek"=>"id_master_kode_sub_rincian_objek = trx_anggaran.master_kode_sub_rincian_objek_id",
                    "master_kode_rincian_objek"=>"id_master_kode_rincian_objek = master_kode_rincian_objek_id",
                    "master_kode_objek"=>"id_master_kode_objek = master_kode_objek_id",
                    "master_kode_jenis"=>"id_master_kode_jenis = master_kode_jenis_id",
                    "master_kode_kelompok"=>"id_master_kode_kelompok = master_kode_kelompok_id",
                    "master_kode_akun"=>"id_master_kode_akun = master_kode_akun_id",
                    "master_satker"=>"id_master_satker = anggaran.master_satker_id",
                ),
                "where"=>$where_arr_skpd_anggaran,
                "order_by"=>array(
                    "id_master_program,id_master_kegiatan,id_master_sub_kegiatan,id_master_kode_sub_rincian_objek,id_master_sumber_pendanaan_anggaran"=>"ASC"
                )
            )
        );

        $templist = array();
        $html = [];
        $html_link = "";
        $id_anggaran_before = 0;
        $id_master_satker_before = 0;
        $id_program_before = 0;
        $id_kegiatan_before = 0;
        $id_sub_kegiatan_before = 0;
        $id_encrypt = "";
        foreach ($data_anggaran as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }

            if($id_anggaran_before == 0 || $id_anggaran_before != $row->id_anggaran){
                $id_encrypt = encrypt_data($row->id_anggaran);
                $html_link = "<a href='".base_url()."anggaran/edit_anggaran/".$id_encrypt."' class='btn btn-primary btn-icon btn-sm'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon btn-sm' onClick=\"confirm_delete('".$id_encrypt."')\" href='#'><i class='icon-trash'></i></a>";
            }
            $id_anggaran_before = $row->id_anggaran;

            if(array_key_exists("master_sumber_pendanaan_anggaran_id",$where_arr_skpd_anggaran)){
                if($id_master_satker_before == 0 || $id_master_satker_before != $row->id_master_satker){
                    $id_encrypt = encrypt_data($row->id_anggaran);
                    array_push($html,["<span class='font-weight-bold'>".$row->kode_skpd_terbaru."</span>","<span class='font-weight-bold'>".$row->nama_skpd."</span>","","",$html_link]);
                }
                $id_master_satker_before = $row->id_master_satker;
            }

            if($id_program_before == 0 || $id_program_before != $row->id_master_program){
                $kode = "";
                $nama_program = "";
                if(!array_key_exists("master_sumber_pendanaan_anggaran_id",$where_arr_skpd_anggaran)){
                    $id_encrypt = encrypt_data($row->id_anggaran);
                    $kode = "<span class='font-weight-bold'>".$row->kode_urusan.".".$row->kode_bidang_urusan.".".$row->kode_program."</span>";
                    $nama_program = "<span class='font-weight-bold'>".$row->nama_program."</span>";
                }else{
                    $kode = $row->kode_urusan.".".$row->kode_bidang_urusan.".".$row->kode_program;
                    $nama_program = $row->nama_program;
                }
                array_push($html,[$kode,$nama_program,"","",$html_link]);
            }
            $id_program_before = $row->id_master_program;
            
            if($id_kegiatan_before == 0 || $id_kegiatan_before != $row->id_master_kegiatan){
                array_push($html,[$row->kode_urusan.".".$row->kode_bidang_urusan.".".$row->kode_program.".".$row->kode_kegiatan,$row->nama_kegiatan,"","",$html_link]);
            }
            $id_kegiatan_before = $row->id_master_kegiatan;
            
            if($id_sub_kegiatan_before == 0 || $id_sub_kegiatan_before != $row->id_master_sub_kegiatan){
                array_push($html,[$row->kode_urusan.".".$row->kode_bidang_urusan.".".$row->kode_program.".".$row->kode_kegiatan.".".$row->kode_sub_kegiatan,$row->nama_sub_kegiatan,"","",$html_link]);
            }
            $id_sub_kegiatan_before = $row->id_master_sub_kegiatan;

            array_push($html,[$row->kode_urusan.".".$row->kode_bidang_urusan.".".$row->kode_program.".".$row->kode_kegiatan.".".$row->kode_sub_kegiatan." ".$row->nomor_rekening,$row->uraian_sub_rincian_objek,$row->nama_sumber_pendanaan_anggaran,nominal($row->nilai_rincian),$html_link]);

            $templist[$key]['id_encrypt'] = encrypt_data($row->id_anggaran);
        }
        
        $data = $html;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_sub_rincian_objek()
    {
        $id = decrypt_data($this->iget("id"));
        $master_satker_id = $this->session->userdata('master_satker_id');
        if($this->iget("skpd") != ""){
            $master_satker_id = decrypt_data($this->iget("skpd"));
        }

        $data_rincian = $this->master_kode_sub_rincian_objek_model->get(
            array(
                "fields"=>"master_kode_sub_rincian_objek.*,nilai_anggaran",
                "order_by" => array(
                    "uraian_sub_rincian_objek" => "ASC"
                ),
                "join"=>array(
                    "referensi_spm"=>"id_master_kode_sub_rincian_objek=master_kode_sub_rincian_objek_id"
                ),
                "where"=>array(
                    "master_satker_id"=>$master_satker_id,
                    "master_sub_kegiatan_id"=>$id,
                ),
                "where_false" => "referensi_spm.deleted_at is null"
            )
        );

        $templist = array();
        foreach ($data_rincian as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_kode_sub_rincian_objek);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
    
    public function get_data_sub_rincian_objek_edit()
    {
        $id = decrypt_data($this->iget("id"));
        $master_kode_sub_rincian_objek_id = $this->iget("master_kode_sub_rincian_objek_id");
        $id_trx_anggaran = decrypt_data($this->iget("id_trx_anggaran"));
        $index = $this->iget("index");
        $arr_angk_random_input_currency = [];
        
        $master_satker_id = $this->session->userdata('master_satker_id');
        if($this->iget("skpd") != ""){
            $master_satker_id = decrypt_data($this->iget("skpd"));
        }

        $data_rincian = $this->master_kode_sub_rincian_objek_model->get(
            array(
                "fields"=>"master_kode_sub_rincian_objek.*,nilai_anggaran",
                "order_by" => array(
                    "master_kode_sub_rincian_objek_id" => "ASC"
                ),
                "join"=>array(
                    "referensi_spm"=>"id_master_kode_sub_rincian_objek=master_kode_sub_rincian_objek_id"
                ),
                "where"=>array(
                    "master_satker_id"=>$master_satker_id,
                    "master_sub_kegiatan_id"=>$id,
                ),
                "where_false" => "referensi_spm.deleted_at is null",
            )
        );

        $data_trx_anggaran = $this->trx_anggaran_model->get(
            array(
                "fields"=>"trx_sumber_pendanaan_anggaran.*",
                "join"=>array(
                    "trx_sumber_pendanaan_anggaran"=>"trx_anggaran_id=id_trx_anggaran"
                ),
                "where"=>array(
                    "id_trx_anggaran"=>$id_trx_anggaran,
                    "master_kode_sub_rincian_objek_id"=>$master_kode_sub_rincian_objek_id,
                ),
                "order_by"=>array(
                    "master_sumber_pendanaan_anggaran_id"=>"ASC"
                )
            )
        );

        $templist_sumber_dana = "";
        $copy_model = "";
        foreach ($data_trx_anggaran as $key => $row) {
            $angka_rand = rand(0,1000);
            array_push($arr_angk_random_input_currency,$angka_rand);
            $data_sumber_dana_anggaran = $this->master_sumber_pendanaan_anggaran_model->get(
                array(
                    "order_by"=>array(
                        "kode_sumber_pendanaan_anggaran"=>"ASC"
                    )
                )
            );


            if($key == 0){
                $templist_sumber_dana .= "<span class='clone-model-".$index."'>".
                "<hr />".
                "<span class='close-sumber-pendanaan'><div class='text-right mb-2'><a href='#hapusSumberPendanaan' onclick=\"delete_sumber_dana('".encrypt_data($row->id_trx_sumber_pendanaan_anggaran)."',".$index.",this)\">Hapus Sumber Pendanaan</a></div></span>".
                "<div class='form-group row'>".
                "<label class='col-form-label col-lg-2'>Sumber Pendanaan <span class='text-danger'>*</span></label>".
                "<div class='col-lg-10'>".
                "<select class='form-control select-search' name='sumber_pendanaan_".$index."[][]' required>".
                "<option value=''>-- Pilih Sumber Pendanaan --</option>";
                foreach ($data_sumber_dana_anggaran as $key_sumber_dana => $value_sumber_dana) {
                    $selected = "";
                    if($value_sumber_dana->id_master_sumber_pendanaan_anggaran == $row->master_sumber_pendanaan_anggaran_id){
                        $selected = "selected";
                    }
                    $templist_sumber_dana .= "<option ".$selected." value='".encrypt_data($value_sumber_dana->id_master_sumber_pendanaan_anggaran)."' idsumber='".$value_sumber_dana->id_master_sumber_pendanaan_anggaran."'>".$value_sumber_dana->nama_sumber_pendanaan_anggaran."</option>";
                }

                $templist_sumber_dana .= "</select>".
                "</div>".
                "</div>".
                "<div class='form-group row'>".
                "<label class='col-form-label col-lg-2'>Nilai Rincian <span class='text-danger'>*</span></label>".
                "<div class='col-lg-10'>".
                "<input type='text' class='form-control input-currency-".$index."".$angka_rand."' name='nilai_".$index."[][]' value='".$row->nilai_rincian."' placeholder='Nilai Rincian' required data-index='".$index."' onkeyup='calc_nilai(this)'>".
                "</div>".
                "</div>".
                "</span>";
            }else{
                $copy_model .= "<span class=''>".
                "<hr />".
                "<span class='close-sumber-pendanaan'><div class='text-right mb-2'><a href='#hapusSumberPendanaan' onclick=\"delete_sumber_dana('".encrypt_data($row->id_trx_sumber_pendanaan_anggaran)."',".$index.",this)\">Hapus Sumber Pendanaan</a></div></span>".
                "<div class='form-group row'>".
                "<label class='col-form-label col-lg-2'>Sumber Pendanaan <span class='text-danger'>*</span></label>".
                "<div class='col-lg-10'>".
                "<select class='form-control select-search' name='sumber_pendanaan_".$index."[][]' required>".
                "<option value=''>-- Pilih Sumber Pendanaan --</option>";
                foreach ($data_sumber_dana_anggaran as $key_sumber_dana => $value_sumber_dana) {
                    $selected = "";
                    if($value_sumber_dana->id_master_sumber_pendanaan_anggaran == $row->master_sumber_pendanaan_anggaran_id){
                        $selected = "selected";
                    }
                    $copy_model .= "<option ".$selected." value='".encrypt_data($value_sumber_dana->id_master_sumber_pendanaan_anggaran)."' idsumber='".$value_sumber_dana->id_master_sumber_pendanaan_anggaran."'>".$value_sumber_dana->nama_sumber_pendanaan_anggaran."</option>";
                }

                $copy_model .= "</select>".
                "</div>".
                "</div>".
                "<div class='form-group row'>".
                "<label class='col-form-label col-lg-2'>Nilai Rincian <span class='text-danger'>*</span></label>".
                "<div class='col-lg-10'>".
                "<input type='text' class='form-control input-currency-".$index."".$angka_rand."' name='nilai_".$index."[][]' value='".$row->nilai_rincian."' placeholder='Nilai Rincian' required data-index='".$index."' onkeyup='calc_nilai(this)'>".
                "</div>".
                "</div>".
                "</span>";
            }
        }
        $templist_sumber_dana .= "<span class='copy-model-".$index."'>".$copy_model."</span>";


        $templist = array();
        foreach ($data_rincian as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_kode_sub_rincian_objek);
        }

        $data = ["sub_rincian_belanja"=>$templist,"sumber_dana"=>$templist_sumber_dana,"angka_random_currency"=>$arr_angk_random_input_currency];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
    
    public function get_data_trx_anggaran_edit()
    {
        $anggaran_id = decrypt_data($this->iget("anggaran_id"));

        $data_trx_anggaran = $this->trx_anggaran_model->get(
            array(
                "fields"=>"trx_anggaran.*",
                "where"=>array(
                    "anggaran_id"=>$anggaran_id,
                ),
                "join"=>array(
                    "referensi_spm"=>"referensi_spm_id=id_referensi_spm"
                ),
                "order_by"=>array(
                    "master_program_id"=>"ASC",
                    "master_kegiatan_id"=>"ASC",
                    "master_sub_kegiatan_id"=>"ASC",
                    "trx_anggaran.master_kode_sub_rincian_objek_id"=>"ASC",
                )
            )
        );

        $templist = array();
        foreach ($data_trx_anggaran as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_anggaran);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
    
    public function cek_data_anggaran()
    {
        $master_satker_id = $this->session->userdata('master_satker_id');
        if($this->iget("skpd_id") != ""){
            $master_satker_id = decrypt_data($this->iget("skpd_id"));
        }

        $sub_kegiatan_id = decrypt_data($this->iget("sub_kegiatan_id"));
        $data_anggaran = $this->anggaran_model->get(
            array(
                "where"=>array(
                    "master_satker_id"=>$master_satker_id,
                    "master_sub_kegiatan_id"=>$sub_kegiatan_id,
                )
            ),"row"
        );

        if($data_anggaran){
            $data_anggaran->id_encrypt = encrypt_data($data_anggaran->id_anggaran);
    
            $data_trx_anggaran = $this->trx_anggaran_model->get(
                array(
                    "where"=>array(
                        "anggaran_id"=>$data_anggaran->id_anggaran,
                    ),
                )
            );
    
            $templist = array();
            foreach ($data_trx_anggaran as $key => $row) {
                foreach ($row as $keys => $rows) {
                    $templist[$key][$keys] = $rows;
                }
                $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_anggaran);
            }
        }else{
            $templist = [];
            $data_anggaran = [];
        }

        $data = ["trx_anggaran"=>$templist,"anggaran"=>$data_anggaran];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    // public function get_data_rekanan()
    // {
    //     $data_rekanan = $this->master_rekanan_model->get(
    //         array(
    //             "where" => array(
    //                 "master_satker_id" => $this->session->userdata("master_satker_id")
    //             ),
    //             "order_by" => array(
    //                 "nama_rekanan" => "ASC"
    //             )
    //         )
    //     );

    //     $templist = array();
    //     foreach ($data_rekanan as $key => $row) {
    //         foreach ($row as $keys => $rows) {
    //             $templist[$key][$keys] = $rows;
    //         }
    //         $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_rekanan);
    //     }

    //     $data = $templist;
    //     $this->output
    //         ->set_content_type('application/json')
    //         ->set_output(json_encode($data));
    // }

    public function get_data_sub_kegiatan(){
        $skpd = decrypt_data($this->iget("skpd"));

        $data_list_sub_kegiatan = $this->master_sub_kegiatan_model->get(
            array(
                "fields" => "master_sub_kegiatan.*,nama_program,nama_kegiatan",
                "join"   => array(
                    "master_kegiatan" => "id_master_kegiatan=master_kegiatan_id",
                    "master_program"  => "id_master_program=master_program_id",
                    "referensi_spm"  => "id_master_sub_kegiatan=master_sub_kegiatan_id",
                ),
                'order_by' => array(
                    'nama_program'      => 'ASC',
                    'nama_kegiatan'     => 'ASC',
                    'nama_sub_kegiatan' => 'ASC'
                ),
                "where"=>array(
                    "master_satker_id"=>$skpd
                ),
                "group_by"=>"referensi_spm.master_sub_kegiatan_id,referensi_spm.master_kegiatan_id,referensi_spm.master_program_id"
            )
        );

        $templist = array();
        foreach ($data_list_sub_kegiatan as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_sub_kegiatan);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
