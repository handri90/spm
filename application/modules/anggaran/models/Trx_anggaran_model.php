<?php

class Trx_anggaran_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "trx_anggaran";
        $this->primary_id = "id_trx_anggaran";
    }
}
