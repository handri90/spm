<?php

class Anggaran_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "anggaran";
        $this->primary_id = "id_anggaran";
    }
}
