<?php

class Trx_sumber_pendanaan_anggaran_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "trx_sumber_pendanaan_anggaran";
        $this->primary_id = "id_trx_sumber_pendanaan_anggaran";
    }
}
