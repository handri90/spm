<style>
	.user-image-custom {
		margin-bottom: 10px;
	}
</style>
<div class="content">
	<!-- Form inputs -->
	<div class="card">
		<div class="card-body">
			<?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
			<fieldset class="mb-3">
				<legend class="text-uppercase font-size-sm font-weight-bold">User</legend>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Nama Lengkap <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->nama_lengkap : ""; ?>" name="nama_lengkap" required placeholder="Nama Lengkap">
						<inptu type="hidden" name="id_user" value="<?php echo !empty($content) ? $content->id_user : ""; ?>" />
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Username <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" class="form-control" value="<?php echo !empty($content) ? $content->username : ""; ?>" name="username" required placeholder="Username">
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Password <?php echo !empty($content) ? '' : '<span class="text-danger">*</span>'; ?></label>
					<div class="col-lg-10">
						<input type="password" name="password" id="password" <?php echo !empty($content) ? '' : 'required'; ?> placeholder="Password" class="form-control">
						<?php echo empty($content) ? '' : '<span class="form-text text-muted">Jika ingin merubah password, silahkan diisi</span>'; ?>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-form-label col-lg-2">Level <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<select class="form-control select-search" name="level_user" required>
							<option value="">-- Pilih Level User --</option>
							<?php
							foreach ($level_user as $key => $row) {
								$selected = "";
								if (!empty($content)) {
									if ($row->id_level_user == $content->level_user_id) {
										$selected = 'selected="selected"';
									}
								}
							?>
								<option <?php echo $selected; ?> value="<?php echo $row->id_level_user; ?>"><?php echo $row->nama_level_user; ?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>
			</fieldset>

			<div class="text-right">
				<button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
	<!-- /form inputs -->

</div>

<script>
	$("form").submit(function() {
		var swalInit = swal.mixin({
			buttonsStyling: false,
			confirmButtonClass: 'btn btn-primary',
			cancelButtonClass: 'btn btn-light'
		});

		let id_user = $("input[name=id_user]").val();
		let username = $("input[name=username]").val();
		let status = true;

		$.ajax({
			url: base_url + 'user/request/cek_username',
			async: false,
			data: {
				username: username,
				id_user: id_user
			},
			type: 'GET',
			beforeSend: function() {
				loading_start();
			},
			success: function(response) {
				// alert(response);
				status = response;
			},
			complete: function(response) {
				loading_stop();
			}
		});

		if (!status) {
			swalInit(
				'Gagal',
				'Username sudah digunakan',
				'error'
			);
			return false;
		}
	})
</script>