<div class="content">
    <div class="card card-table">
        <div class="card-body">
            <div class="card card-table table-responsive shadow-0 mb-0">
                <?php echo form_open(); ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Nama SKPD</th>
                            <th>Pemroses SP2D</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($list_master_satker) {
                            $no = 1;
                            foreach ($list_master_satker as $key => $row) {
                        ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $row->kode; ?></td>
                                    <td><?php echo $row->nama; ?></td>
                                    <td>
                                        <select class="form-control select-search" name="petugas_pemroses[<?php echo $row->id_master_satker; ?>]">
                                            <option value="">-- Pilih Petugas--</option>
                                            <?php
                                            if ($list_user) {
                                                foreach ($list_user as $key_user => $row_user) {
                                                    $selected = "";
                                                    if ($row->id_user_pemroses == $row_user->id_user) {
                                                        $selected = 'selected="selected"';
                                                    }
                                            ?>
                                                    <option <?php echo $selected; ?> value="<?php echo encrypt_data($row_user->id_user); ?>"><?php echo $row_user->nama_lengkap; ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <div class="card-body">
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>