<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Petugas_pemroses extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('user_model');
        $this->load->model('master_satker/master_satker_model', 'master_satker_model');
    }

    public function index()
    {
        $data['list_master_satker'] = $this->master_satker_model->get();
        $data['list_user'] = $this->user_model->get(
            array(
                "where" => array(
                    "level_user_id" => "4"
                )
            )
        );

        $data['breadcrumb'] = [['link' => false, 'content' => 'Petugas Pemroses', 'is_active' => true]];
        if (empty($_POST)) {
            $this->execute('index', $data);
        } else {
            $status = false;
            foreach ($data['list_master_satker'] as $key => $row) {
                $petugas_pemroses = decrypt_data($_POST['petugas_pemroses'][$row->id_master_satker]);

                $data = array(
                    "id_user_pemroses" => ($petugas_pemroses ? $petugas_pemroses : NULL),
                    'updated_at' => $this->datetime()
                );

                $status = $this->master_satker_model->edit($row->id_master_satker, $data);
            }

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect("petugas_pemroses");
        }
    }
}
