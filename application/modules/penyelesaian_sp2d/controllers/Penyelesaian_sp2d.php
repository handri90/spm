<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penyelesaian_sp2d extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('trx_spm_model');
        $this->load->model('track_position/trx_spm_track_position_model', 'trx_spm_track_position_model');
    }

    public function index()
    {

        $data['breadcrumb'] = [['link' => false, 'content' => 'Penyelesaian SP2D', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function act_penyelesaian_sp2d()
    {
        $tanggal_register = explode("-", date("Y-m-d"));
        $tanggal_jam_perbend = explode(" ", $this->ipost("tanggal_jam_perbend"));
        $tanggal_perbend = explode("-", $tanggal_jam_perbend[0]);

        if ($this->ipost("id_trx_spm")) {
            $data = array(
                "no_sp2d_penyelesaian" => $this->ipost("nomor_registrasi_pertama"),
                "user_perbendaharaan_penyelesaian_sp2d" => $this->ipost("nama_bidang_perbend"),
                "tanggal_jam_perbendaharaan_penyelesaian_sp2d" => $tanggal_perbend[2] . "-" . $tanggal_perbend[1] . "-" . $tanggal_perbend[0] . " " . $tanggal_jam_perbend[1],
                "updated_at" => $this->datetime(),
                "id_user_updated" => $this->session->userdata("id_user")
            );

            $status = $this->trx_spm_model->edit(decrypt_data($this->ipost("id_trx_spm")), $data);
        } else {

            $data = array(
                "spm_id" => decrypt_data($this->ipost("id_spm")),
                "tanggal_spm_trx" => date("Y-m-d"),
                "no_sp2d_penyelesaian" => $this->ipost("nomor_registrasi_pertama"),
                "status_spm" => "4",
                "user_perbendaharaan_penyelesaian_sp2d" => $this->ipost("nama_bidang_perbend"),
                "tanggal_jam_perbendaharaan_penyelesaian_sp2d" => $tanggal_perbend[2] . "-" . $tanggal_perbend[1] . "-" . $tanggal_perbend[0] . " " . $tanggal_jam_perbend[1],
                "created_at" => $this->datetime(),
                "id_user_created" => $this->session->userdata("id_user")
            );

            $status = $this->trx_spm_model->save($data);

            $data_track_position = array(
                "trx_spm_id" => $status,
                "tanggal_jam_position" => $tanggal_perbend[2] . "-" . $tanggal_perbend[1] . "-" . $tanggal_perbend[0] . " " . $tanggal_jam_perbend[1],
                "position_spm" => "3",
                "created_at" => $this->datetime(),
                "id_user_created" => $this->session->userdata("id_user")
            );

            $this->trx_spm_track_position_model->save($data_track_position);
        }


        $param = false;
        if ($status) {
            $param = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($param));
    }

    public function delete_penyelesaian_sp2d()
    {
        $id_trx_spm = $this->iget('id_trx_spm');
        $data_master = $this->trx_spm_model->get_by(decrypt_data($id_trx_spm));
        $data_track_position = $this->trx_spm_track_position_model->get(
            array(
                "where" => array(
                    "trx_spm_id" => decrypt_data($id_trx_spm)
                )
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->trx_spm_model->remove(decrypt_data($id_trx_spm));
        $this->trx_spm_track_position_model->remove($data_track_position->id_trx_spm_track_position);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function cetak_penyelesaian_sp2d($id_trx_spm)
    {

        $data['content'] = $this->trx_spm_model->query("
                SELECT `id_trx_spm`, DATE_FORMAT(trx_spm.tanggal_spm_trx, '%d-%m-%Y') AS tanggal_spm_trx, trx_spm.`no_register`, `no_spm`, `nama`, `nilai`, `uraian`, `no_sp2d_penyelesaian`,a.no_register AS nomor_berkas,DATE_FORMAT(trx_spm.tanggal_spm_trx, '%d-%m-%Y') AS tanggal_spm_trx_range,DATE_FORMAT(a.tanggal_spm_trx,'%Y-%m-%d') as tanggal_berkas,nama_sumber_pendanaan,nama_sub_kegiatan,a.user_penyerahan_spm_masuk,DATE_FORMAT(a.tanggal_jam_penyerahan_spm_masuk, '%d-%m-%Y %H:%i:%s') AS tanggal_jam_penyerahan_spm_masuk,user_perbendaharaan_penyelesaian_sp2d,DATE_FORMAT(trx_spm.tanggal_jam_perbendaharaan_penyelesaian_sp2d, '%d-%m-%Y %H:%i:%s') AS tanggal_jam_perbendaharaan_penyelesaian_sp2d,DATE_FORMAT(spm.tanggal_spm, '%d-%m-%Y') AS tanggal_spm,a.user_perbendaharaan_spm_masuk,nama_lengkap,DATE_FORMAT(a.tanggal_jam_perbendaharaan_spm_masuk, '%d-%m-%Y %H:%i:%s') AS tanggal_jam_perbendaharaan_spm_masuk
                FROM `trx_spm` 
                JOIN `spm` ON `spm_id`=`id_spm` AND `spm`.`deleted_at` IS NULL 
                JOIN `master_satker` ON `id_master_satker`=`master_satker_id`
                JOIN `master_sumber_pendanaan` ON `id_master_sumber_pendanaan`=`master_sumber_pendanaan_id`
                LEFT JOIN `referensi_spm` ON id_referensi_spm=referensi_spm_id
                LEFT JOIN master_sub_kegiatan ON id_master_sub_kegiatan=referensi_spm.master_sub_kegiatan_id 
                LEFT JOIN (
                    SELECT no_register,spm_id,tanggal_spm_trx,user_penyerahan_spm_masuk,tanggal_jam_penyerahan_spm_masuk,tanggal_jam_perbendaharaan_penyelesaian_sp2d,user_perbendaharaan_spm_masuk,nama_lengkap,tanggal_jam_perbendaharaan_spm_masuk
                    FROM trx_spm
                    JOIN user on id_user_created=id_user
                    WHERE status_spm IN ('1','2') AND trx_spm.deleted_at IS NULL
                ) AS a ON a.spm_id=id_spm
                WHERE id_trx_spm = '" . decrypt_data($id_trx_spm) . "' AND `trx_spm`.`deleted_at` IS NULL
                ")->row();

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'tempDir' => '/tmp']);
        $data = $this->load->view('cetak_penyelesaian_sp2d', $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }
}
