<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('monitoring_spm/spm_model', 'spm_model');
        $this->load->model('trx_spm_model');
        $this->load->model('user_model');
        $this->load->model('master_satker/master_satker_model', 'master_satker_model');
    }

    public function get_data_penyelesaian_sp2d()
    {
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $skpd = decrypt_data($this->iget("skpd"));
        $wh = "";
        if ($skpd) {
            $wh = "AND master_satker_id = " . $skpd;
        }

        $wh = " AND YEAR(tanggal_spm) = '" . $this->session->userdata('tahun') . "'";

        if ($this->session->userdata("level_user_id") == "4") {
            $data_trx_spm = $this->trx_spm_model->get(
                array(
                    "fields" => "id_trx_spm,DATE_FORMAT(tanggal_jam_perbendaharaan_penyelesaian_sp2d,'%Y-%m-%d') as tanggal_jam_perbendaharaan_penyelesaian_sp2d,no_register,no_spm,nama,nilai,uraian,no_sp2d_penyelesaian",
                    "join" => array(
                        "spm" => "spm_id=id_spm AND spm.deleted_at IS NULL",
                        "master_satker" => "id_master_satker=master_satker_id"
                    ),
                    "where" => array(
                        "status_spm" => "4",
                        "id_user_pemroses" => $this->session->userdata("id_user")
                    ),
                    "where_false" => "DATE_FORMAT(trx_spm.created_at,'%Y-%m-%d') BETWEEN '${start_date}' AND '${end_date}' " . $wh,
                    "order_by" => array(
                        "trx_spm.created_at" => "DESC"
                    )
                )
            );
        } else {
            $data_trx_spm = $this->trx_spm_model->get(
                array(
                    "fields" => "id_trx_spm,DATE_FORMAT(tanggal_jam_perbendaharaan_penyelesaian_sp2d,'%Y-%m-%d') as tanggal_jam_perbendaharaan_penyelesaian_sp2d,no_register,no_spm,nama,nilai,uraian,no_sp2d_penyelesaian",
                    "join" => array(
                        "spm" => "spm_id=id_spm AND spm.deleted_at IS NULL",
                        "master_satker" => "id_master_satker=master_satker_id"
                    ),
                    "where" => array(
                        "status_spm" => "4"
                    ),
                    "where_false" => "DATE_FORMAT(trx_spm.created_at,'%Y-%m-%d') BETWEEN '${start_date}' AND '${end_date}' " . $wh,
                    "order_by" => array(
                        "trx_spm.created_at" => "DESC"
                    )
                )
            );
        }

        $templist = array();
        foreach ($data_trx_spm as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_spm);
            $templist[$key]['tanggal_jam_perbendaharaan_penyelesaian_sp2d'] = date_indo($row->tanggal_jam_perbendaharaan_penyelesaian_sp2d);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_spm()
    {
        $id_spm = decrypt_data($this->iget("id_spm"));

        $data = $this->spm_model->get(
            array(
                "fields" => "spm.*,nama,trx_spm.no_register,DATE_FORMAT(tanggal_spm,'%d-%m-%Y') as tanggal_spm,DATE_FORMAT(tanggal_spm_trx,'%d-%m-%Y') as tanggal_spm_trx,catatan,DATE_FORMAT(tanggal_jam_penyerahan_spm_masuk,'%d-%m-%Y %H:%i:%s') AS tanggal_jam_penyerahan_spm_masuk,nama_program,nama_kegiatan,nama_sub_kegiatan,nama_sumber_pendanaan",
                "join" => array(
                    "master_satker" => "id_master_satker=spm.master_satker_id",
                    "trx_spm" => "id_spm=spm_id AND trx_spm.deleted_at IS NULL",
                    "master_sumber_pendanaan" => "master_sumber_pendanaan_id=id_master_sumber_pendanaan",
                ),
                "left_join" => array(
                    "referensi_spm" => "id_referensi_spm=referensi_spm_id",
                    "master_program" => "id_master_program=referensi_spm.master_program_id",
                    "master_kegiatan" => "id_master_kegiatan=referensi_spm.master_kegiatan_id",
                    "master_sub_kegiatan" => "id_master_sub_kegiatan=referensi_spm.master_sub_kegiatan_id",
                ),
                "where" => array(
                    "id_spm" => $id_spm,
                    "status_spm" => "2"
                )
            ),
            "row"
        );

        $data_trx_spm = $this->trx_spm_model->get(
            array(
                "fields" => "no_register",
                "where" => array(
                    "status_spm" => "4"
                ),
                "order_by" => array(
                    "trx_spm.created_at" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        if ($data_trx_spm) {
            $expl = explode("/", $data_trx_spm->no_register);
            $data->jumlah_trx_spm = (int) $expl[0] + 1;
        } else {
            $data->jumlah_trx_spm = 1;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_kbu()
    {
        $data_kbu = $this->user_model->get(
            array(
                "where" => array(
                    "level_user_id" => "5"
                )
            )
        );

        $templist = array();
        foreach ($data_kbu as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_user);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_spm()
    {
        $skpd = decrypt_data($this->iget("skpd"));

        if ($this->session->userdata("level_user_id") == "4") {
            $data_spm = $this->spm_model->get(
                array(
                    "fields" => "spm.*,nama,nama_program,nama_kegiatan,nama_sub_kegiatan",
                    "join" => array(
                        "trx_spm" => "id_spm=spm_id AND trx_spm.deleted_at IS NULL AND `status_spm` = '2' ",
                        "master_satker" => "id_master_satker=spm.master_satker_id",
                    ),
                    "left_join" => array(
                        "referensi_spm" => "id_referensi_spm=referensi_spm_id",
                        "master_program" => "id_master_program=referensi_spm.master_program_id",
                        "master_kegiatan" => "id_master_kegiatan=referensi_spm.master_kegiatan_id",
                        "master_sub_kegiatan" => "id_master_sub_kegiatan=referensi_spm.master_sub_kegiatan_id",
                    ),
                    "where" => array(
                        "spm.master_satker_id" => $skpd,
                        "id_user_pemroses" => $this->session->userdata("id_user"),
                        "YEAR(tanggal_spm)" => $this->session->userdata('tahun')
                    ),
                    "where_false" => "id_spm NOT IN (SELECT spm_id FROM trx_spm where trx_spm.deleted_at IS NULL AND (status_spm = '4' OR (status_spm = '3' AND jenis_pengembalian NOT IN ('3'))))"
                )
            );
        } else {
            $data_spm = $this->spm_model->get(
                array(
                    "fields" => "spm.*,nama,nama_program,nama_kegiatan,nama_sub_kegiatan",
                    "join" => array(
                        "trx_spm" => "id_spm=spm_id AND trx_spm.deleted_at IS NULL AND `status_spm` = '2' ",
                        "master_satker" => "id_master_satker=spm.master_satker_id",
                    ),
                    "left_join" => array(
                        "referensi_spm" => "id_referensi_spm=referensi_spm_id",
                        "master_program" => "id_master_program=referensi_spm.master_program_id",
                        "master_kegiatan" => "id_master_kegiatan=referensi_spm.master_kegiatan_id",
                        "master_sub_kegiatan" => "id_master_sub_kegiatan=referensi_spm.master_sub_kegiatan_id",
                    ),
                    "where" => array(
                        "spm.master_satker_id" => $skpd,
                        "YEAR(tanggal_spm)" => $this->session->userdata('tahun')
                    ),
                    "where_false" => "id_spm NOT IN (SELECT spm_id FROM trx_spm where trx_spm.deleted_at IS NULL AND (status_spm = '4' OR (status_spm = '3' AND jenis_pengembalian NOT IN ('3'))))"
                )
            );
        }

        $templist = array();
        foreach ($data_spm as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_spm);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_skpd()
    {
        if ($this->session->userdata("level_user_id") == "4") {
            $data_skpd = $this->master_satker_model->get(
                array(
                    "where" => array(
                        "id_user_pemroses" => $this->session->userdata("id_user")
                    ),
                    "order_by" => array(
                        "nama" => "ASC"
                    )
                )
            );
        } else {
            $data_skpd = $this->master_satker_model->get(
                array(
                    "order_by" => array(
                        "nama" => "ASC"
                    )
                )
            );
        }

        $templist = array();
        foreach ($data_skpd as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_satker);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_spm_for_edit()
    {
        $id_trx_spm = decrypt_data($this->iget("id_trx_spm"));

        $data = $this->trx_spm_model->get(
            array(
                "fields" => "trx_spm.*,DATE_FORMAT(tanggal_spm_trx,'%d-%m-%Y') as tanggal_spm_trx,DATE_FORMAT(tanggal_jam_perbendaharaan_penyelesaian_sp2d,'%d-%m-%Y %H:%i:%s') as tanggal_jam_perbendaharaan_penyelesaian_sp2d",
                "where" => array(
                    "id_trx_spm" => $id_trx_spm
                )
            ),
            "row"
        );

        $data_spm_masuk = $this->spm_model->get(
            array(
                "fields" => "spm.*,nama,trx_spm.no_register,DATE_FORMAT(tanggal_spm,'%d-%m-%Y') as tanggal_spm,DATE_FORMAT(tanggal_spm_trx,'%d-%m-%Y') as tanggal_spm_trx,catatan,DATE_FORMAT(tanggal_jam_penyerahan_spm_masuk,'%d-%m-%Y') AS tanggal_jam_penyerahan_spm_masuk,nama_sumber_pendanaan",
                "join" => array(
                    "master_satker" => "id_master_satker=master_satker_id",
                    "trx_spm" => "id_spm=spm_id",
                    "master_sumber_pendanaan" => "master_sumber_pendanaan_id=id_master_sumber_pendanaan",
                ),
                "where" => array(
                    "id_spm" => $data->spm_id,
                    "status_spm" => "2"
                )
            ),
            "row"
        );

        $data->nomor_terima_spm = $data_spm_masuk->no_register;
        $data->tanggal_terima_spm = $data_spm_masuk->tanggal_spm_trx;
        $data->nilai_terima_spm = $data_spm_masuk->nilai;
        $data->catatan_terima_spm = $data_spm_masuk->catatan;
        $data->nomor_data_spm = $data_spm_masuk->no_spm;
        $data->tanggal_data_spm = $data_spm_masuk->tanggal_spm;
        $data->skpd_data_spm = $data_spm_masuk->nama;
        $data->nilai_data_spm = $data_spm_masuk->nilai;
        $data->sumber_dana_data_spm = $data_spm_masuk->nama_sumber_pendanaan;
        $data->uraian_data_spm = $data_spm_masuk->uraian;

        $data->id_encrypt = encrypt_data($data->id_trx_spm);
        $data->id_spm_encrypt = encrypt_data($data->spm_id);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
