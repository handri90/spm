<style>
    .rght-border {
        border-right: 1px solid #ddd;
        padding-right: 20px;
    }

    #AnyTime--anytime-both {
        left: 330px !important;
        top: 50px !important;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light daterange-predefined">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date" />
                    <input type="hidden" name="end_date" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">SKPD</label>
                <div class="col-lg-4">
                    <select class="form-control select-search" name="skpd" onChange="reload_datatable()">
                        <option value="">-- PILIH --</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="#formulirPenyelesaianSP2D" id="formulirPenyelesaianSP2D" class="btn btn-info">Formulir Penyelesaian SP2D</a>
            </div>
        </div>
        <table id="datatablePenyelesaianSP2D" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Tanggal Penyelesaian SP2D</th>
                    <th>Nomor</th>
                    <th>Nomor SPM</th>
                    <th>SKPD</th>
                    <th>Nilai</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalRegisterSPM" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> List SPM</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="card border-top-success">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>SKPD : </label>
                                    <select class="form-control select-search" name="skpd_register_spm" onChange="get_data_spm()">
                                        <option value="">-- PILIH --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-table">
                    <table id="datatableRegisterSPM" class="table datatable-save-state table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>NO SPM</th>
                                <th>SKPD</th>
                                <th>Uraian</th>
                                <th>Program</th>
                                <th>Kegiatan</th>
                                <th>Sub Kegiatan</th>
                                <th>Nilai</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalFormulirSPM" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Formulir Penyelesaian SP2D</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="row">

                    <div class="col-md-6 rght-border">
                        <div class="card-body">

                            <table class="table table-dark bg-slate-600">
                                <tr>
                                    <td colspan="3">Data SPM</td>
                                </tr>
                                <tr>
                                    <td>Nomor</td>
                                    <td>:</td>
                                    <td><span class="nomor_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>Tanggal</td>
                                    <td>:</td>
                                    <td><span class="tanggal_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>SKPD</td>
                                    <td>:</td>
                                    <td><span class="nama_skpd"></span></td>
                                </tr>
                                <tr>
                                    <td>Nilai SPM</td>
                                    <td>:</td>
                                    <td><span class="nilai_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>Sumber Dana</td>
                                    <td>:</td>
                                    <td><span class="sumber_dana"></span></td>
                                </tr>
                                <tr>
                                    <td>Uraian</td>
                                    <td>:</td>
                                    <td><span class="uraian"></span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="card-body">

                            <table class="table table-dark bg-slate-600">
                                <tr>
                                    <td colspan="3">Register Masuk</td>
                                </tr>
                                <tr>
                                    <td>Nomor</td>
                                    <td>:</td>
                                    <td><span class="terima_spm_nomor_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>Tanggal</td>
                                    <td>:</td>
                                    <td><span class="terima_spm_tanggal_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>Nilai SPM</td>
                                    <td>:</td>
                                    <td><span class="terima_spm_nilai_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>Catatan</td>
                                    <td>:</td>
                                    <td><span class="terima_spm_catatan"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3">Nomor SP2D</label>
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-8">
                                        <input type="hidden" class="form-control" name="id_spm">
                                        <input type="hidden" class="form-control" name="id_trx_spm">
                                        <input type="text" class="form-control" name="nomor_registrasi_pertama" placeholder="XXXXX">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">Bidang Perbendaharaan
                            </legend>

                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Nama : <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" name="nama_bidang_perbend" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-3">Tanggal/Jam :</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="anytime-both" name="tanggal_jam_perbend" readonly placeholder="Tanggal/Jam">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_spm()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const IDR = value => currency(
        value, {
            symbol: "Rp. ",
            precision: 0,
            separator: "."
        });

    function reload_datatable() {
        $('#datatablePenyelesaianSP2D').DataTable().ajax.reload();
    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            $('#datatablePenyelesaianSP2D').DataTable().ajax.reload();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_data_penyelesaian_sp2d();

    function get_data_penyelesaian_sp2d() {

        $("#datatablePenyelesaianSP2D").DataTable({
            ajax: {
                "url": base_url + 'penyelesaian_sp2d/request/get_data_penyelesaian_sp2d',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "start_date": $("input[name='start_date']").val(),
                        "end_date": $("input[name='end_date']").val(),
                        "skpd": $("select[name=skpd]").val(),
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "tanggal_jam_perbendaharaan_penyelesaian_sp2d"
            }, {
                data: "no_sp2d_penyelesaian"
            }, {
                data: "no_spm"
            }, {
                data: "nama"
            }, {
                "render": function(data, type, full, meta) {
                    let str_nilai = "";
                    let spl_nilai = full.nilai.split(",");
                    str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

                    return str_nilai;
                }
            }, {
                "width": "15%",
                "render": function(data, type, full, meta) {
                    return "<a href='#' onClick=\"edit_penyelesaian_sp2d('" + full.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + full.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a> <a target='_blank' class='btn btn-success btn-icon' href='" + base_url + "penyelesaian_sp2d/cetak_penyelesaian_sp2d/" + full.id_encrypt + "'><i class='icon-printer'></i></a>";
                }
            }]
        });
    }

    function confirm_delete(id_trx_spm) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'penyelesaian_sp2d/delete_penyelesaian_sp2d',
                    data: {
                        id_trx_spm: id_trx_spm
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $('#datatablePenyelesaianSP2D').DataTable().ajax.reload();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            $('#datatablePenyelesaianSP2D').DataTable().ajax.reload();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        $('#datatablePenyelesaianSP2D').DataTable().ajax.reload();
                    }
                });
            }
        });
    }

    $(function() {
        $("#formulirPenyelesaianSP2D").on("click", function() {
            $("#modalRegisterSPM").modal("show");
            get_data_spm();
        });

        $('.modal').on("hidden.bs.modal", function(e) { //fire on closing modal box
            if ($('.modal:visible').length) { // check whether parent modal is opend after child modal close
                $('body').addClass('modal-open'); // if open mean length is 1 then add a bootstrap css class to body of the page
            }
        });

        $('.daterange-single').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'), 10)
        });

        $('#anytime-both').AnyTime_picker({
            format: '%d-%m-%Y %H:%i:%s',
        });
    });

    function get_data_spm() {
        let datatableRegisterSPM = $("#datatableRegisterSPM").DataTable();

        let skpd = $("select[name='skpd_register_spm']").val();

        if (!skpd) {
            skpd = $("select[name='skpd']").val();
            list_skpd_register_spm($("select[name='skpd']").find("option:selected").attr("data-id"));
        } else {
            list_skpd($("select[name='skpd_register_spm']").find("option:selected").attr("data-id"));
        }

        datatableRegisterSPM.clear().draw();

        if (skpd) {
            $.ajax({
                url: base_url + 'penyelesaian_sp2d/request/get_data_spm',
                data: {
                    skpd: skpd
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        let str_nilai = "";
                        let spl_nilai = value.nilai.split(",");
                        str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

                        datatableRegisterSPM.row.add([
                            value.no_spm,
                            value.nama,
                            value.uraian,
                            value.nama_program,
                            value.nama_kegiatan,
                            value.nama_sub_kegiatan,
                            str_nilai,
                            "<a href='#' onClick=\"show_formulir_spm('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-touch'></i></a>"
                        ]).draw(false);
                    });
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function show_formulir_spm(id_spm) {
        $("#modalRegisterSPM").modal("toggle");
        $.ajax({
            url: base_url + 'penyelesaian_sp2d/request/get_detail_spm',
            data: {
                id_spm: id_spm
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let str_nilai = "";
                let spl_nilai = response.nilai.split(",");
                str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

                $("#modalFormulirSPM").modal("show");
                $(".alert_form").html("");
                $(".terima_spm_tanggal_spm").html(response.tanggal_jam_penyerahan_spm_masuk);
                $(".terima_spm_nomor_spm").html(response.no_register);
                $(".terima_spm_nilai_spm").html(str_nilai);
                $(".terima_spm_catatan").html(response.catatan);
                $(".tanggal_spm").html(response.tanggal_spm);
                $(".nomor_spm").html(response.no_spm);
                $(".nama_skpd").html(response.nama);
                $(".nilai_spm").html(str_nilai);
                $(".sumber_dana").html(response.nama_sumber_pendanaan);
                $(".uraian").html(response.uraian);
                $("input[name='id_spm']").val(id_spm);
                $("input[name='id_trx_spm']").val("");
                $("input[name='nomor_registrasi_pertama']").val("");
                // $("input[name='nomor_registrasi_kedua']").val("SP2D");
                // $("input[name='nomor_registrasi_ketiga']").val(moment().format('YYYY'));
                $("input[name='nama_bidang_perbend']").val("");
                $("input[name='tanggal_jam_perbend']").val(moment().format('DD-MM-YYYY HH:mm:ss'));
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function action_form_spm() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let nomor_registrasi_pertama = $("input[name='nomor_registrasi_pertama']").val();
        // let nomor_registrasi_kedua = $("input[name='nomor_registrasi_kedua']").val();
        // let nomor_registrasi_ketiga = $("input[name='nomor_registrasi_ketiga']").val();
        let nama_bidang_perbend = $("input[name='nama_bidang_perbend']").val();
        let tanggal_jam_perbend = $("input[name='tanggal_jam_perbend']").val();
        let id_spm = $("input[name='id_spm']").val();
        let id_trx_spm = $("input[name='id_trx_spm']").val();

        if (!nomor_registrasi_pertama) {
            $(".alert_form").html("<div class='alert alert-danger'>Nomor SP2D tidak boleh kosong.</div>");
        } else if (!nama_bidang_perbend) {
            $(".alert_form").html("<div class='alert alert-danger'>Bidang Perbendaharaan tidak boleh kosong.</div>");
        } else if (!tanggal_jam_perbend) {
            $(".alert_form").html("<div class='alert alert-danger'>Tanggal/Jam tidak boleh kosong.</div>");
        } else {
            $.ajax({
                url: base_url + 'penyelesaian_sp2d/act_penyelesaian_sp2d',
                data: {
                    nomor_registrasi_pertama: nomor_registrasi_pertama,
                    nama_bidang_perbend: nama_bidang_perbend,
                    tanggal_jam_perbend: tanggal_jam_perbend,
                    id_spm: id_spm,
                    id_trx_spm: id_trx_spm
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $(".alert_form").html("");
                    $(".terima_spm_tanggal_spm").html("");
                    $(".terima_spm_nomor_spm").html("");
                    $(".terima_spm_nilai_spm").html("");
                    $(".terima_spm_catatan").html("");
                    $(".tanggal_spm").html("");
                    $(".nomor_spm").html("");
                    $(".nama_skpd").html("");
                    $(".nilai_spm").html("");
                    $(".sumber_dana").html("");
                    $(".uraian").html("");
                    $("input[name='id_spm']").val("");
                    $("input[name='id_trx_spm']").val("");
                    $("input[name='nomor_registrasi_pertama']").val("");
                    // $("input[name='nomor_registrasi_kedua']").val("");
                    // $("input[name='nomor_registrasi_ketiga']").val("");
                    $("input[name='nomor_registrasi_keempat']").val("");
                    $("input[name='nomor_registrasi_kelima']").val(moment().format('YYYY'));
                    $("input[name='nama_bidang_perbend']").val("");
                    $("input[name='tanggal_jam_perbend']").val("");
                    $("#modalFormulirSPM").modal("toggle");
                    if (response) {
                        $('#datatablePenyelesaianSP2D').DataTable().ajax.reload();
                        if (id_trx_spm) {
                            swalInit(
                                'Berhasil',
                                'Data berhasil diubah',
                                'success'
                            );
                        } else {
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        }
                    } else {
                        $('#datatablePenyelesaianSP2D').DataTable().ajax.reload();
                        if (id_trx_spm) {
                            swalInit(
                                'Gagal',
                                'Data gagal diubah',
                                'error'
                            );
                        } else {
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    list_skpd();

    function list_skpd(param_selected = "") {
        let html = "<option value=''>-- SEMUA --</option>";
        $.ajax({
            url: base_url + 'penyelesaian_sp2d/request/get_data_skpd',
            type: 'GET',
            success: function(response) {
                $.each(response, function(index, value) {
                    let selected = "";
                    if (value.id_master_satker == param_selected) {
                        selected = "selected";
                    }
                    html += "<option data-id='" + value.id_master_satker + "' " + selected + " value='" + value.id_encrypt + "'>" + value.nama + "</option>";
                });

                $("select[name='skpd']").html(html);
                if (param_selected) {
                    $('#datatablePenyelesaianSP2D').DataTable().ajax.reload();
                }
            }
        });
    }

    function list_skpd_register_spm(param_selected = "") {
        let html = "<option value=''>-- PILIH --</option>";
        $.ajax({
            url: base_url + 'penyelesaian_sp2d/request/get_data_skpd',
            type: 'GET',
            success: function(response) {
                $.each(response, function(index, value) {
                    let selected = "";
                    if (value.id_master_satker == param_selected) {
                        selected = "selected";
                    }
                    html += "<option data-id='" + value.id_master_satker + "' " + selected + " value='" + value.id_encrypt + "'>" + value.nama + "</option>";
                });

                $("select[name='skpd_register_spm']").html(html);
            }
        });
    }

    function edit_penyelesaian_sp2d(id_trx_spm) {
        $.ajax({
            url: base_url + 'penyelesaian_sp2d/request/get_detail_spm_for_edit',
            data: {
                id_trx_spm: id_trx_spm
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let str_nilai_terima = "";
                let spl_nilai_terima = response.nilai_terima_spm.split(",");
                str_nilai_terima = IDR(spl_nilai_terima[0]).format(true) + (spl_nilai_terima[1] ? "," + spl_nilai_terima[1] : "");

                let str_nilai = "";
                let spl_nilai = response.nilai_data_spm.split(",");
                str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

                $("#modalFormulirSPM").modal("show");
                $(".alert_form").html("");
                $(".terima_spm_tanggal_spm").html(response.tanggal_terima_spm);
                $(".terima_spm_nomor_spm").html(response.nomor_terima_spm);
                $(".terima_spm_nilai_spm").html(str_nilai_terima);
                $(".terima_spm_catatan").html(response.catatan_terima_spm);
                $(".tanggal_spm").html(response.tanggal_data_spm);
                $(".nomor_spm").html(response.nomor_data_spm);
                $(".nama_skpd").html(response.skpd_data_spm);
                $(".nilai_spm").html(str_nilai);
                $(".sumber_dana").html(response.sumber_dana_data_spm);
                $(".uraian").html(response.uraian_data_spm);
                $("input[name='id_spm']").val(response.id_spm_encrypt);
                $("input[name='id_trx_spm']").val(response.id_encrypt);
                $("input[name='nomor_registrasi_pertama']").val(response.no_sp2d_penyelesaian);
                // $("input[name='nomor_registrasi_kedua']").val(spl_no_register[1]);
                // $("input[name='nomor_registrasi_ketiga']").val(spl_no_register[2]);
                $("input[name='nama_bidang_perbend']").val(response.user_perbendaharaan_penyelesaian_sp2d);
                $("input[name='tanggal_jam_perbend']").val(response.tanggal_jam_perbendaharaan_penyelesaian_sp2d);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
</script>