<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('import_spm/spm_model', 'spm_model');
        $this->load->model('monitoring_spm/spm_model', 'spm_model');
        $this->load->model('trx_spm_model');
        $this->load->model('user_model');
        $this->load->model('master_satker/master_satker_model', 'master_satker_model');
    }

    public function get_data_skpd()
    {
        $data_skpd = $this->master_satker_model->get(
            array(
                "order_by" => array(
                    "nama" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_skpd as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_satker);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_spm_masuk()
    {
        $skpd = decrypt_data($this->iget("skpd"));

        $data_trx_spm = $this->trx_spm_model->query("
        SELECT `id_trx_spm`, DATE_FORMAT(tanggal_spm_trx, '%Y-%m-%d') AS tanggal_spm_trx, `no_register`, `no_spm`, `nama`, `nilai`, `uraian` , position_spm
        FROM `trx_spm` 
        JOIN `spm` ON `spm_id`=`id_spm` AND `spm`.`deleted_at` IS NULL 
        JOIN `master_satker` ON `id_master_satker`=`master_satker_id`
        LEFT JOIN (
            SELECT *
            FROM trx_spm_track_position
            WHERE id_trx_spm_track_position IN (
                SELECT MAX(id_trx_spm_track_position)
                FROM trx_spm_track_position
                WHERE trx_spm_track_position.deleted_at IS NULL
                GROUP BY trx_spm_id
            ) AND trx_spm_track_position.deleted_at IS NULL
        ) AS a ON a.trx_spm_id=id_trx_spm
        WHERE `master_satker_id` = '" . $skpd . "' AND `status_spm` = '1' AND `trx_spm`.`deleted_at` IS NULL 
        ORDER BY `trx_spm`.`created_at` DESC
        ")->result();

        $templist = array();
        foreach ($data_trx_spm as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_spm);
            $templist[$key]['tanggal_spm_trx'] = date_indo($row->tanggal_spm_trx);
            $templist[$key]['position_spm'] = ($row->position_spm == "1" ? "Loket Pelayanan" : ($row->position_spm == "2" ? "Bidang Belanja" : ($row->position_spm == "3" ? "Selesai" : "")));
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_spm()
    {
        $id_trx_spm = decrypt_data($this->iget("id_trx_spm"));

        $data = $this->trx_spm_model->query("
        SELECT `trx_spm`.*, `no_spm`, `nama`, DATE_FORMAT(tanggal_spm_trx, '%d-%m-%Y') AS tanggal_spm_trx, `id_spm`,a.position_spm,DATE_FORMAT(tanggal_jam_position,'%d-%m-%Y %H:%i:%s') AS tanggal_jam_position
        FROM `trx_spm` 
        JOIN `spm` ON `id_spm`=`spm_id` AND `spm`.`deleted_at` IS NULL 
        JOIN `master_satker` ON `id_master_satker`=`master_satker_id` 
        LEFT JOIN (
            SELECT *
            FROM trx_spm_track_position
            WHERE id_trx_spm_track_position IN (
                SELECT MAX(id_trx_spm_track_position)
                FROM trx_spm_track_position
                WHERE trx_spm_track_position.deleted_at IS NULL
                GROUP BY trx_spm_id
            ) AND trx_spm_track_position.deleted_at IS NULL
        ) AS a ON a.trx_spm_id=id_trx_spm 
        WHERE `id_trx_spm` = '" . $id_trx_spm . "' AND `trx_spm`.`deleted_at` IS NULL
        ")->row();

        $data->id_encrypt = encrypt_data($data->id_trx_spm);
        $data->id_spm_encrypt = encrypt_data($data->id_spm);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
