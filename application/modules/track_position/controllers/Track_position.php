<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Track_position extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('trx_spm_track_position_model');
    }

    public function index()
    {

        $data['breadcrumb'] = [['link' => false, 'content' => 'Track Position', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function act_spm_track_position()
    {
        $tanggal_jam_position = explode(" ", $this->ipost("tanggal_jam_position"));
        $tanggal_position = explode("-", $tanggal_jam_position[0]);

        if ($this->ipost("position")) {
            $last_data = $this->trx_spm_track_position_model->get(
                array(
                    "where" => array(
                        "trx_spm_id" => decrypt_data($this->ipost("id_trx_spm"))
                    ),
                    "order_by" => array(
                        "id_trx_spm_track_position" => "DESC"
                    ),
                    "limit" => "1"
                ),
                "row"
            );

            if ($last_data && ($last_data->position_spm != $this->ipost("position"))) {
                $data = array(
                    "trx_spm_id" => decrypt_data($this->ipost("id_trx_spm")),
                    "position_spm" => $this->ipost("position"),
                    "tanggal_jam_position" => $tanggal_position[2] . "-" . $tanggal_position[1] . "-" . $tanggal_position[0] . " " . $tanggal_jam_position[1],
                    "created_at" => $this->datetime(),
                    "id_user_created" => $this->session->userdata("id_user")
                );

                $status = $this->trx_spm_track_position_model->save($data);
            } else {
                $status = true;
            }
        } else {
            $status = true;
        }



        $param = false;
        if ($status) {
            $param = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($param));
    }
}
