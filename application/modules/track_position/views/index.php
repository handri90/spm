<style>
    .rght-border {
        border-right: 1px solid #ddd;
        padding-right: 20px;
    }

    #AnyTime--anytime-both {
        left: 330px !important;
        top: 50px !important;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>SKPD : </label>
                        <select class="form-control select-search" name="skpd" onChange="get_data_spm_masuk()">
                            <option value="">-- PILIH --</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <table id="datatableSPMMasuk" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Tanggal Masuk</th>
                    <th>Nomor</th>
                    <th>Nomor SPM</th>
                    <th>SKPD</th>
                    <th>Nilai</th>
                    <th>Posisi</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalFormulirSPM" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Position</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">SKPD :</label>
                    <div class="col-lg-9">
                        <span class="nama_skpd"></span>
                        <input type="hidden" name="id_trx_spm" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Nomor :</label>
                    <div class="col-lg-9">
                        <span class="nomor_spm"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tanggal/Jam :</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" name="tanggal_jam_position" id="anytime-both" readonly placeholder="Tanggal/Jam">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Posisi Terakhir :</label>
                    <div class="col-lg-9">
                        <select class="form-control select-search" name="position">
                            <option value="">-- PILIH --</option>
                        </select>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_spm()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const IDR = value => currency(
        value, {
            symbol: "Rp. ",
            precision: 0,
            separator: "."
        });

    let datatableSPMMasuk = $("#datatableSPMMasuk").DataTable();

    function get_data_spm_masuk() {

        let skpd = $("select[name=skpd]").val();
        datatableSPMMasuk.clear().draw();
        if (skpd) {
            $.ajax({
                url: base_url + 'track_position/request/get_data_spm_masuk',
                data: {
                    skpd: skpd
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        datatableSPMMasuk.row.add([
                            value.tanggal_spm_trx,
                            value.no_register,
                            value.no_spm,
                            value.nama,
                            IDR(value.nilai).format(true),
                            value.position_spm,
                            "<a href='#' onClick=\"edit_posisi_spm('" + value.id_encrypt + "')\" class='btn btn-danger btn-icon'><i class='icon-location3'></i></a>"
                        ]).draw(false);
                    });
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function confirm_delete(id_trx_spm) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'track_position/delete_spm_masuk',
                    data: {
                        id_trx_spm: id_trx_spm
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_data_spm_masuk();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_data_spm_masuk();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_data_spm_masuk();
                    }
                });
            }
        });
    }

    $(function() {
        $("#formulirSPMMasuk").on("click", function() {
            $("#modalRegisterSPM").modal("show");
            get_data_spm();
        });

        $('.modal').on("hidden.bs.modal", function(e) { //fire on closing modal box
            if ($('.modal:visible').length) { // check whether parent modal is opend after child modal close
                $('body').addClass('modal-open'); // if open mean length is 1 then add a bootstrap css class to body of the page
            }
        });

        $('#anytime-both').AnyTime_picker({
            format: '%d-%m-%Y %H:%i:%s',
        });

        $('#anytime-both2').AnyTime_picker({
            format: '%d-%m-%Y %H:%i:%s',
        });

        $('.daterange-single').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'), 10)
        });
    });

    function get_data_spm() {
        let datatableRegisterSPM = $("#datatableRegisterSPM").DataTable();

        let skpd = $("select[name='skpd_register_spm']").val();

        if (!skpd) {
            skpd = $("select[name='skpd']").val();
            list_skpd_register_spm($("select[name='skpd']").find("option:selected").attr("data-id"));
        } else {
            list_skpd($("select[name='skpd_register_spm']").find("option:selected").attr("data-id"));
        }

        datatableRegisterSPM.clear().draw();

        if (skpd) {
            $.ajax({
                url: base_url + 'track_position/request/get_data_spm',
                data: {
                    skpd: skpd
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        datatableRegisterSPM.row.add([
                            value.no_spm,
                            value.nama,
                            value.uraian,
                            value.program,
                            value.kegiatan,
                            IDR(value.nilai).format(true),
                            "<a href='#' onClick=\"show_formulir_spm('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-touch'></i></a>"
                        ]).draw(false);
                    });
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function action_form_spm() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let tanggal_jam_position = $("input[name='tanggal_jam_position']").val();
        let position = $("select[name='position']").val();
        let id_trx_spm = $("input[name='id_trx_spm']").val();

        $.ajax({
            url: base_url + 'track_position/act_spm_track_position',
            data: {
                tanggal_jam_position: tanggal_jam_position,
                position: position,
                id_trx_spm: id_trx_spm
            },
            type: 'POST',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("input[name='id_trx_spm']").val("");
                $("input[name='tanggal_jam_position']").val(moment().format('DD-MM-YYYY HH:mm:ss'));
                $("select[name='position']").val("");
                $("#modalFormulirSPM").modal("toggle");
                if (response) {
                    get_data_spm_masuk();
                    swalInit(
                        'Berhasil',
                        'Data berhasil diubah',
                        'success'
                    );
                } else {
                    get_data_spm_masuk();
                    swalInit(
                        'Gagal',
                        'Data gagal diubah',
                        'error'
                    );
                }
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    list_skpd();

    function list_skpd(param_selected = "") {
        let html = "<option value=''>-- PILIH --</option>";
        $.ajax({
            url: base_url + 'track_position/request/get_data_skpd',
            type: 'GET',
            success: function(response) {
                $.each(response, function(index, value) {
                    let selected = "";
                    if (value.id_master_satker == param_selected) {
                        selected = "selected";
                    }
                    html += "<option data-id='" + value.id_master_satker + "' " + selected + " value='" + value.id_encrypt + "'>" + value.nama + "</option>";
                });

                $("select[name='skpd']").html(html);
            }
        });
    }

    function list_position(param_selected = "") {
        let html = "<option value=''>-- PILIH --</option>";
        let arr_position = [];
        arr_position[1] = "Loket Pelayanan";
        arr_position[2] = "Bidang Belanja";
        arr_position[3] = "Selesai";
        $.each(arr_position, function(index, value) {
            if (index != 0) {
                let selected = "";
                if (index == param_selected) {
                    selected = "selected";
                }

                let disable_opt = "";
                if (index == 1 || index == 3) {
                    disable_opt = "disabled";
                }
                html += "<option " + disable_opt + " " + selected + " value='" + index + "'>" + value + "</option>";
            }
        });

        $("select[name='position']").html(html);
    }

    function edit_posisi_spm(id_trx_spm) {
        $.ajax({
            url: base_url + 'track_position/request/get_detail_spm',
            data: {
                id_trx_spm: id_trx_spm
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("#modalFormulirSPM").modal("show");
                list_position(response.position_spm);
                $(".nomor_spm").html(response.no_spm);
                $(".nama_skpd").html(response.nama);
                $("input[name='id_trx_spm']").val(response.id_encrypt);
                $("input[name='tanggal_jam_position']").val((response.tanggal_jam_position ? response.tanggal_jam_position : moment().format('DD-MM-YYYY HH:mm:ss')));
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }
</script>