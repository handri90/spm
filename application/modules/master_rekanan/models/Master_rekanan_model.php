<?php

class Master_rekanan_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_rekanan";
        $this->primary_id = "id_master_rekanan";
    }
}
