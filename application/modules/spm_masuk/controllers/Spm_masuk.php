<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Spm_masuk extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('trx_spm_model');
        $this->load->model('track_position/trx_spm_track_position_model', 'trx_spm_track_position_model');
    }

    public function index()
    {

        $data['breadcrumb'] = [['link' => false, 'content' => 'SPM Masuk', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function act_spm_masuk()
    {
        $tanggal_register = explode("-", $this->ipost("tanggal_register"));
        $tanggal_jam_menyerahkan = explode(" ", $this->ipost("tanggal_jam_menyerahkan"));
        $tanggal_menyerahkan = explode("-", $tanggal_jam_menyerahkan[0]);
        // $tanggal_jam_perbend = explode(" ", $this->ipost("tanggal_jam_perbend"));
        // $tanggal_perbend = explode("-", $tanggal_jam_perbend[0]);

        if ($this->ipost("id_trx_spm")) {
            $data = array(
                "tanggal_spm_trx" => $tanggal_register[2] . "-" . $tanggal_register[1] . "-" . $tanggal_register[0],
                "catatan" => $this->ipost("catatan"),
                "user_penyerahan_spm_masuk" => $this->ipost("nama_menyerahkan"),
                "tanggal_jam_penyerahan_spm_masuk" => $tanggal_menyerahkan[2] . "-" . $tanggal_menyerahkan[1] . "-" . $tanggal_menyerahkan[0] . " " . $tanggal_jam_menyerahkan[1],
                // "user_perbendaharaan_spm_masuk" => $this->ipost("nama_bidang_perbend"),
                // "tanggal_jam_perbendaharaan_spm_masuk" => $tanggal_perbend[2] . "-" . $tanggal_perbend[1] . "-" . $tanggal_perbend[0] . " " . $tanggal_jam_perbend[1],
                "updated_at" => $this->datetime(),
                "id_user_updated" => $this->session->userdata("id_user")
            );

            $status = $this->trx_spm_model->edit(decrypt_data($this->ipost("id_trx_spm")), $data);
        } else {

            $data = array(
                "spm_id" => decrypt_data($this->ipost("id_spm")),
                "tanggal_spm_trx" => $tanggal_register[2] . "-" . $tanggal_register[1] . "-" . $tanggal_register[0],
                "no_register" => $this->ipost("nomor_register_depan") . "/SPM-MSK/" . date("Y"),
                "catatan" => $this->ipost("catatan"),
                "status_spm" => "1",
                "user_penyerahan_spm_masuk" => $this->ipost("nama_menyerahkan"),
                "tanggal_jam_penyerahan_spm_masuk" => $tanggal_menyerahkan[2] . "-" . $tanggal_menyerahkan[1] . "-" . $tanggal_menyerahkan[0] . " " . $tanggal_jam_menyerahkan[1],
                // "user_perbendaharaan_spm_masuk" => $this->ipost("nama_bidang_perbend"),
                // "tanggal_jam_perbendaharaan_spm_masuk" => $tanggal_perbend[2] . "-" . $tanggal_perbend[1] . "-" . $tanggal_perbend[0] . " " . $tanggal_jam_perbend[1],
                "created_at" => $this->datetime(),
                "id_user_created" => $this->session->userdata("id_user")
            );

            $status = $this->trx_spm_model->save($data);

            $data_track_position = array(
                "trx_spm_id" => $status,
                "tanggal_jam_position" => $tanggal_menyerahkan[2] . "-" . $tanggal_menyerahkan[1] . "-" . $tanggal_menyerahkan[0] . " " . $tanggal_jam_menyerahkan[1],
                "position_spm" => "1",
                "created_at" => $this->datetime(),
                "id_user_created" => $this->session->userdata("id_user")
            );

            $this->trx_spm_track_position_model->save($data_track_position);
        }


        $param = false;
        if ($status) {
            $param = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($param));
    }

    public function delete_spm_masuk()
    {
        $id_trx_spm = $this->iget('id_trx_spm');
        $data_master = $this->trx_spm_model->get_by(decrypt_data($id_trx_spm));

        if (!$data_master) {
            $this->page_error();
        }

        $data_trx_spm = $this->trx_spm_model->get(
            array(
                "fields" => "status_spm",
                "where" => array(
                    "spm_id" => $data_master->spm_id
                ),
                "order_by" => array(
                    "trx_spm.created_at" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        if (in_array($data_trx_spm->status_spm, array('2', '3', '4'))) {
            $data = false;
        } else {
            $data = true;
            $status = $this->trx_spm_model->remove(decrypt_data($id_trx_spm));
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function cetak_tanda_terima($id_trx_spm)
    {

        $data['content'] = $this->trx_spm_model->get(
            array(
                "fields" => "trx_spm.*,no_spm,nilai,uraian,nama_program,nama_kegiatan,nama_sub_kegiatan,nama,tanggal_spm_trx,DATE_FORMAT(tanggal_spm,'%d-%m-%Y') as tanggal_spm,id_spm,nama_lengkap,nama_sumber_pendanaan,DATE_FORMAT(tanggal_jam_penyerahan_spm_masuk,'%d-%m-%Y %H:%i:%s') as tanggal_jam_penyerahan_spm_masuk,nama_rekanan",
                "join" => array(
                    "spm" => "id_spm=spm_id AND spm.deleted_at IS NULL",
                    "master_satker" => "id_master_satker=spm.master_satker_id",
                    "user" => "trx_spm.id_user_created=id_user",
                    "master_sumber_pendanaan" => "id_master_sumber_pendanaan=master_sumber_pendanaan_id"
                ),
                "left_join" => array(
                    "referensi_spm" => "id_referensi_spm=referensi_spm_id",
                    "master_program" => "id_master_program=referensi_spm.master_program_id",
                    "master_kegiatan" => "id_master_kegiatan=referensi_spm.master_kegiatan_id",
                    "master_sub_kegiatan" => "id_master_sub_kegiatan=referensi_spm.master_sub_kegiatan_id",
                    "master_rekanan" => "id_master_rekanan=spm.master_rekanan_id",
                ),
                "where" => array(
                    "id_trx_spm" => decrypt_data($id_trx_spm)
                )
            ),
            "row"
        );

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'tempDir' => '/tmp']);
        $data = $this->load->view('cetak_tanda_terima', $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }
}
