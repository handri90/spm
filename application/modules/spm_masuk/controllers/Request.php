<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('monitoring_spm/spm_model', 'spm_model');
        $this->load->model('trx_spm_model');
        $this->load->model('master_satker/master_satker_model', 'master_satker_model');
    }

    public function get_data_spm_masuk()
    {
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $skpd = decrypt_data($this->iget("skpd"));
        $wh = array();
        if ($skpd) {
            $wh = array(
                "master_satker_id" => $skpd
            );
        }

        $wh = array(
            "YEAR(tanggal_spm)" => $this->session->userdata('tahun')
        );

        $data_trx_spm = $this->trx_spm_model->get(
            array(
                "fields" => "id_trx_spm,DATE_FORMAT(tanggal_spm_trx,'%Y-%m-%d') as tanggal_spm_trx,no_register,no_spm,nama,nilai,uraian",
                "join" => array(
                    "spm" => "spm_id=id_spm AND spm.deleted_at IS NULL",
                    "master_satker" => "id_master_satker=master_satker_id"
                ),
                "where" => $wh,
                "where_false" => "status_spm IN ('1','2') AND DATE_FORMAT(trx_spm.created_at,'%Y-%m-%d') BETWEEN '${start_date}' AND '${end_date}'",
                "order_by" => array(
                    "trx_spm.created_at" => "DESC"
                )
            )
        );

        $templist = array();
        foreach ($data_trx_spm as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_spm);
            $templist[$key]['tanggal_spm_trx'] = date_indo($row->tanggal_spm_trx);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_spm()
    {
        $id_spm = decrypt_data($this->iget("id_spm"));

        $data = $this->spm_model->get(
            array(
                "fields" => "spm.*,nama,nama_sumber_pendanaan,DATE_FORMAT(tanggal_spm,'%d-%m-%Y') as tanggal_spm,nama_program,nama_kegiatan,nama_sub_kegiatan",
                "join" => array(
                    "master_satker" => "id_master_satker=master_satker_id",
                    "master_sumber_pendanaan" => "id_master_sumber_pendanaan=master_sumber_pendanaan_id",
                ),
                "left_join" => array(
                    "referensi_spm" => "id_referensi_spm=referensi_spm_id",
                    "master_program" => "id_master_program=referensi_spm.master_program_id",
                    "master_kegiatan" => "id_master_kegiatan=referensi_spm.master_kegiatan_id",
                    "master_sub_kegiatan" => "id_master_sub_kegiatan=referensi_spm.master_sub_kegiatan_id",
                ),
                "where" => array(
                    "id_spm" => $id_spm
                )
            ),
            "row"
        );

        $data_trx_spm = $this->trx_spm_model->query(
            "
            SELECT `no_register` 
            FROM `trx_spm` 
            WHERE user_penyerahan_spm_masuk IS NOT NULL AND RIGHT(no_register,4) = '{$this->session->userdata('tahun')}'
            ORDER BY `trx_spm`.`created_at` DESC LIMIT 1
            "
        )->row();

        if ($data_trx_spm) {
            $expl = explode("/", $data_trx_spm->no_register);
            $data->jumlah_trx_spm = (int) $expl[0] + 1;
        } else {
            $data->jumlah_trx_spm = 1;
        }

        if (strlen((string)$data->jumlah_trx_spm) == '1') {
            $data->jumlah_trx_spm = "0000" . (string)$data->jumlah_trx_spm;
        } else if (strlen((string)$data->jumlah_trx_spm) == '2') {
            $data->jumlah_trx_spm = "000" . (string)$data->jumlah_trx_spm;
        } else if (strlen((string)$data->jumlah_trx_spm) == '3') {
            $data->jumlah_trx_spm = "00" . (string)$data->jumlah_trx_spm;
        } else if (strlen((string)$data->jumlah_trx_spm) == '4') {
            $data->jumlah_trx_spm = "0" . (string)$data->jumlah_trx_spm;
        } else if (strlen((string)$data->jumlah_trx_spm) == '5') {
            $data->jumlah_trx_spm = (string)$data->jumlah_trx_spm;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_spm()
    {
        $skpd = decrypt_data($this->iget("skpd"));

        $data_spm = $this->spm_model->get(
            array(
                "fields" => "spm.*,nama,nama_program,nama_kegiatan,nama_sub_kegiatan",
                "left_join" => array(
                    "trx_spm" => "id_spm=spm_id AND trx_spm.deleted_at IS NULL",
                    "referensi_spm" => "id_referensi_spm=referensi_spm_id",
                    "master_program" => "id_master_program=referensi_spm.master_program_id",
                    "master_kegiatan" => "id_master_kegiatan=referensi_spm.master_kegiatan_id",
                    "master_sub_kegiatan" => "id_master_sub_kegiatan=referensi_spm.master_sub_kegiatan_id",
                ),
                "where" => array(
                    "spm.master_satker_id" => $skpd,
                    "YEAR(tanggal_spm)" => $this->session->userdata('tahun')
                ),
                "where_false" => "id_spm NOT IN (select spm_id from trx_spm where trx_spm.deleted_at is null and status_spm IN ('1','2','3','4'))",
                "join" => array(
                    "master_satker" => "id_master_satker=spm.master_satker_id",
                )
            )
        );

        $templist = array();
        foreach ($data_spm as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_spm);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_skpd()
    {
        $data_skpd = $this->master_satker_model->get(
            array(
                "order_by" => array(
                    "nama" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_skpd as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_satker);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_detail_spm_for_edit()
    {
        $id_trx_spm = decrypt_data($this->iget("id_trx_spm"));

        $data = $this->trx_spm_model->get(
            array(
                "fields" => "trx_spm.*,no_spm,nilai,uraian,nama_program,nama_kegiatan,nama_sub_kegiatan,nama,DATE_FORMAT(tanggal_spm_trx,'%d-%m-%Y') as tanggal_spm_trx,DATE_FORMAT(tanggal_spm,'%d-%m-%Y') as tanggal_spm,DATE_FORMAT(tanggal_jam_penyerahan_spm_masuk,'%d-%m-%Y %H:%i:%s') as tanggal_jam_penyerahan_spm_masuk,DATE_FORMAT(tanggal_jam_perbendaharaan_spm_masuk,'%d-%m-%Y %H:%i:%s') as tanggal_jam_perbendaharaan_spm_masuk,id_spm,nama_sumber_pendanaan",
                "join" => array(
                    "spm" => "id_spm=spm_id AND spm.deleted_at IS NULL",
                    "master_satker" => "id_master_satker=spm.master_satker_id",
                    "master_sumber_pendanaan" => "id_master_sumber_pendanaan=master_sumber_pendanaan_id"
                ),
                "left_join" => array(
                    "referensi_spm" => "id_referensi_spm=referensi_spm_id",
                    "master_program" => "id_master_program=referensi_spm.master_program_id",
                    "master_kegiatan" => "id_master_kegiatan=referensi_spm.master_kegiatan_id",
                    "master_sub_kegiatan" => "id_master_sub_kegiatan=referensi_spm.master_sub_kegiatan_id",
                ),
                "where" => array(
                    "id_trx_spm" => $id_trx_spm
                )
            ),
            "row"
        );

        $data->id_encrypt = encrypt_data($data->id_trx_spm);
        $data->id_spm_encrypt = encrypt_data($data->id_spm);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function cek_trx_spm()
    {
        $id_trx_spm = decrypt_data($this->iget("id_trx_spm"));

        $data_trx = $this->trx_spm_model->get_by($id_trx_spm);

        $data_trx_spm = $this->trx_spm_model->get(
            array(
                "fields" => "status_spm",
                "where" => array(
                    "spm_id" => $data_trx->spm_id
                ),
                "order_by" => array(
                    "trx_spm.created_at" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        if (in_array($data_trx_spm->status_spm, array('2', '3', '4'))) {
            $data = false;
        } else {
            $data = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
