<html>

<head>
    <style>
        body {
            font-family: Georgia, 'Times New Roman', serif;
            font-size: 12pt;
            font-weight: normal;
        }

        .head_print {
            text-align: center;
            font-weight: 100;
            line-height: 0.6;
        }

        .wd-number {
            width: 10px;
        }

        .wd-label {
            width: 100px;
        }

        .wd-label-2 {
            width: 60px;

        }

        .wd-content {
            width: 200px;
        }

        .wd-content-2 {
            width: 310px;
        }

        .wd-titik {
            width: 2px;
        }

        table {
            margin: 10px 0;
        }

        .tbl-sign {
            margin-top: 20px;
            text-align: center;
        }

        .ruler {
            border-bottom: 2px double black;
        }

        .logo {
            float: left;
            width: 20%;
        }

        .deskripsi {
            float: left;
            width: 60%;
        }

        .alamat {
            line-height: 0.5;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>

<body>
    <table style="width:100%;margin-top:20px">
        <tr>
            <td colspan="3" height="90">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" class="text-center">TANDA TERIMA SPM</td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%" style="border-collapse:collapse;letter-spacing: 1px;">
                    <tr>
                        <td width="120">NOMOR</td>
                        <td width="20">:</td>
                        <td><?php echo isset($content) ? $content->no_register : ""; ?></td>
                    </tr>
                    <tr>
                        <td>NAMA SKPD</td>
                        <td>:</td>
                        <td><?php echo isset($content) ? $content->nama : ""; ?></td>
                    </tr>
                    <tr>
                        <td>CATATAN</td>
                        <td>:</td>
                        <td><?php echo isset($content) ? $content->catatan : ""; ?></td>
                    </tr>
                    <tr>
                        <td colspan="3" height="20">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">SURAT PERINTAH MEMBAYAR (SPM)</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%" style="border-collapse:collapse;">
                                <tr>
                                    <td width="20">1.</td>
                                    <td width="120">Tanggal</td>
                                    <td width="20">:</td>
                                    <td><?php echo isset($content) ? $content->tanggal_spm : ""; ?></td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>Nomor</td>
                                    <td>:</td>
                                    <td><?php echo isset($content) ? $content->no_spm : ""; ?></td>
                                </tr>
                                <tr>
                                    <td>3.</td>
                                    <td>Sub Kegiatan</td>
                                    <td>:</td>
                                    <td><?php echo isset($content) ? $content->nama_sub_kegiatan : ""; ?></td>
                                </tr>
                                <tr>
                                    <td>4.</td>
                                    <td>Nilai</td>
                                    <td>:</td>
                                    <td>
                                        <?php if (isset($content)) {
                                            $str_nilai = "";
                                            $spl_nilai = explode(",", $content->nilai);
                                            $str_nilai = nominal($spl_nilai[0]) . ($spl_nilai[1] ? "," . $spl_nilai[1] : "");
                                            echo $str_nilai;
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5.</td>
                                    <td>Sumber Dana</td>
                                    <td>:</td>
                                    <td><?php echo isset($content) ? $content->nama_sumber_pendanaan : ""; ?></td>
                                </tr>
                                <tr>
                                    <td>6.</td>
                                    <td>Penerima</td>
                                    <td>:</td>
                                    <td><?php echo isset($content) ? $content->nama_rekanan : ""; ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">YANG MENYERAHKAN</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%" style="border-collapse:collapse;">
                                <tr>
                                    <td width="20">1.</td>
                                    <td width="120">Nama</td>
                                    <td width="20">:</td>
                                    <td><?php echo isset($content) ? $content->user_penyerahan_spm_masuk : ""; ?></td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>Tanggal, Jam</td>
                                    <td>:</td>
                                    <td><?php echo isset($content) ? $content->tanggal_jam_penyerahan_spm_masuk : ""; ?></td>
                                </tr>
                                <tr>
                                    <td>3.</td>
                                    <td>Tanda Tangan</td>
                                    <td>:</td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="tbl-sign" width="100%">
        <tr>
            <td width="320">UNTUK DITELITI SESUAI KETENTUAN</td>
            <td>Pangkalan Bun, <?php echo isset($content) ? date_indo($content->tanggal_spm_trx) : ""; ?></td>
            <td></td>
        </tr>
        <tr>
            <td>SEBELUM DIPROSES SP2D</td>
            <td>Yang Menerima</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding-top:40px;">(UDL)</td>
            <td style="padding-top:40px;"><?php echo isset($content) ? $content->nama_lengkap : ""; ?></td>
            <td></td>
        </tr>
    </table>
</body>

</html>