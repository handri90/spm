<style>
    .rght-border {
        border-right: 1px solid #ddd;
        padding-right: 20px;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light daterange-predefined">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date" />
                    <input type="hidden" name="end_date" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">SKPD</label>
                <div class="col-lg-4">
                    <select class="form-control select-search" name="skpd" onChange="reload_datatable()">
                        <option value="">-- PILIH --</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <table id="datatableSPMMasuk" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Tanggal Masuk</th>
                    <th>Nomor</th>
                    <th>Nomor SPM</th>
                    <th>SKPD</th>
                    <th>Nilai</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalApproveBerkas" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Approve Berkas</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">User <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select name="user_perbendaharaan" class="form-control select-search">
                            <option value=''>-- PILIH --</option>
                        </select>
                        <input type="hidden" name="id_trx_spm" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-3">Tanggal <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" id="anytime-both" name="tanggal_jam_perbendaharaan" readonly placeholder="Tanggal">
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="save_approve_berkas()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const IDR = value => currency(
        value, {
            symbol: "Rp. ",
            precision: 0,
            separator: "."
        });

    function reload_datatable() {
        $('#datatableSPMMasuk').DataTable().ajax.reload();
    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            $('#datatableSPMMasuk').DataTable().ajax.reload();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));
    get_data_spm_masuk();

    function get_data_spm_masuk() {

        $("#datatableSPMMasuk").DataTable({
            ajax: {
                "url": base_url + 'approve_berkas/request/get_data_spm',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "start_date": $("input[name='start_date']").val(),
                        "end_date": $("input[name='end_date']").val(),
                        "skpd": $("select[name=skpd]").val(),
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "tanggal_spm_trx"
            }, {
                data: "no_register"
            }, {
                data: "no_spm"
            }, {
                data: "nama"
            }, {
                "render": function(data, type, full, meta) {
                    let str_nilai = "";
                    let spl_nilai = full.nilai.split(",");
                    str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

                    return str_nilai;
                }
            }, {
                "render": function(data, type, full, meta) {
                    return (full.user_perbendaharaan_spm_masuk ? "<span class='badge badge-success'>Diterima</span>" : "<span class='badge badge-warning'>Belum Diterima</span>");
                }
            }, {
                "width": "15%",
                "render": function(data, type, full, meta) {
                    return (full.user_perbendaharaan_spm_masuk ? "<a href='#' class='btn btn-info btn-icon' onclick=\"show_modal_approve('" + full.id_encrypt + "')\"><i class='icon-pencil7'></i></a> <a href='#' class='btn btn-danger btn-icon' onclick=\"delete_approve_berkas('" + full.id_encrypt + "')\"><i class='icon-trash'></i></a>" : "<a href='#' class='btn btn-success btn-icon' onclick=\"show_modal_approve('" + full.id_encrypt + "')\"><i class='icon-check'></i></a>");
                }
            }]
        });
    }

    list_skpd();

    function list_skpd(param_selected = "") {
        let html = "<option value=''>-- SEMUA --</option>";
        $.ajax({
            url: base_url + 'spm_masuk/request/get_data_skpd',
            type: 'GET',
            success: function(response) {
                $.each(response, function(index, value) {
                    let selected = "";
                    if (value.id_master_satker == param_selected) {
                        selected = "selected";
                    }
                    html += "<option data-id='" + value.id_master_satker + "' " + selected + " value='" + value.id_encrypt + "'>" + value.nama + "</option>";
                });

                $("select[name='skpd']").html(html);
                if (param_selected) {
                    $('#datatableSPMMasuk').DataTable().ajax.reload();
                }
            }
        });
    }

    function show_modal_approve(id) {
        $.ajax({
            url: base_url + 'approve_berkas/request/get_user_perbend',
            data: {
                id: id
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $("#modalApproveBerkas").modal("show");
                $(".alert_form").html("");
                $("input[name='id_trx_spm']").val(id);
                $("input[name='tanggal_jam_perbendaharaan']").val((response.user_perbendaharaan_spm_masuk ? response.tanggal_jam_perbendaharaan_spm_masuk : moment().format('DD-MM-YYYY HH:mm:ss')));
                list_user_perbend((response.user_perbendaharaan_spm_masuk ? response.user_perbendaharaan_spm_masuk : ""));
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function list_user_perbend(param_selected = "") {
        let user_arr = [];
        user_arr[1] = "Subbid 1";
        user_arr[2] = "Subbid 2";
        let html = "<option value=''>-- PILIH --</option>";
        $.each(user_arr, function(index, value) {
            if (index != 0) {
                let selected = "";
                if (index == param_selected) {
                    selected = "selected";
                }
                html += "<option " + selected + " value='" + index + "'>" + value + "</option>";
            }
        });
        $("select[name='user_perbendaharaan']").html(html);
    }

    function save_approve_berkas() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let id_trx_spm = $("input[name='id_trx_spm']").val();
        let user_perbend = $("select[name='user_perbendaharaan']").val();
        let tanggal_perbend = $("input[name='tanggal_jam_perbendaharaan']").val();

        if (!user_perbend) {
            $(".alert_form").html("<div class='alert alert-danger'>User Perbendaharaan tidak boleh kosong.</div>");
        } else if (!tanggal_perbend) {
            $(".alert_form").html("<div class='alert alert-danger'>Tanggal/Jam tidak boleh kosong.</div>");
        } else {
            $.ajax({
                url: base_url + 'approve_berkas/save_user_perbend',
                data: {
                    user_perbend: user_perbend,
                    tanggal_perbend: tanggal_perbend,
                    id: id_trx_spm
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $("#modalApproveBerkas").modal("toggle");
                    $(".alert_form").html("");
                    $("input[name='id_trx_spm']").val("");
                    $("select[name='user_perbendaharaan']").html("");
                    $("input[name='tanggal_jam_perbendaharaan']").val("");
                    if (response) {
                        $('#datatableSPMMasuk').DataTable().ajax.reload();
                        swalInit(
                            'Berhasil',
                            'Data berhasil diubah',
                            'success'
                        );
                    } else {
                        $('#datatableSPMMasuk').DataTable().ajax.reload();
                        swalInit(
                            'Gagal',
                            'Data gagal diubah',
                            'error'
                        );
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function delete_approve_berkas(id) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'approve_berkas/delete_user_perbend',
                    data: {
                        id: id
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $('#datatableSPMMasuk').DataTable().ajax.reload();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            $('#datatableSPMMasuk').DataTable().ajax.reload();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        $('#datatableSPMMasuk').DataTable().ajax.reload();
                    }
                });
            }
        });
    }

    $(function() {
        $('#anytime-both').AnyTime_picker({
            format: '%d-%m-%Y %H:%i:%s',
        });
    })
</script>