<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('trx_spm_model');
    }

    public function get_data_spm()
    {
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $skpd = decrypt_data($this->iget("skpd"));

        $wh = array();
        if ($skpd) {
            $wh = array(
                "master_satker_id" => $skpd
            );
        }

        $wh = array(
            "YEAR(tanggal_spm)" => $this->session->userdata('tahun')
        );

        $data_trx_spm = $this->trx_spm_model->get(
            array(
                "fields" => "id_trx_spm,DATE_FORMAT(tanggal_spm_trx,'%Y-%m-%d') as tanggal_spm_trx,no_register,no_spm,nama,nilai,uraian,status_spm,user_perbendaharaan_spm_masuk",
                "join" => array(
                    "spm" => "spm_id=id_spm AND spm.deleted_at IS NULL",
                    "master_satker" => "id_master_satker=master_satker_id"
                ),
                "where" => $wh,
                "where_false" => "(status_spm = '1' OR user_perbendaharaan_spm_masuk IS NOT NULL) AND DATE_FORMAT(trx_spm.created_at,'%Y-%m-%d') BETWEEN '${start_date}' AND '${end_date}'",
                "order_by" => array(
                    "trx_spm.created_at" => "DESC"
                )
            )
        );

        $templist = array();
        foreach ($data_trx_spm as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_trx_spm);
            $templist[$key]['tanggal_spm_trx'] = date_indo($row->tanggal_spm_trx);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_user_perbend()
    {
        $id_trx_spm = decrypt_data($this->iget("id"));

        $data = $this->trx_spm_model->get(
            array(
                "fields" => "trx_spm.*,DATE_FORMAT(tanggal_jam_perbendaharaan_spm_masuk,'%d-%m-%Y %H:%i:%s') as tanggal_jam_perbendaharaan_spm_masuk",
                "where" => array(
                    "id_trx_spm" => $id_trx_spm
                )
            ),
            "row"
        );

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
