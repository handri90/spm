<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Approve_berkas extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("trx_spm_model");
        $this->load->model("track_position/trx_spm_track_position_model", "trx_spm_track_position_model");
    }

    public function index()
    {

        $data['breadcrumb'] = [['link' => false, 'content' => 'Approve Berkas', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function save_user_perbend()
    {
        $id_trx = decrypt_data($this->ipost("id"));
        $user_perbend = $this->ipost("user_perbend");
        $tanggal_jam_perbend = explode(" ", $this->ipost("tanggal_perbend"));
        $tanggal_perbend = explode("-", $tanggal_jam_perbend[0]);

        $data_perbend = array(
            "user_perbendaharaan_spm_masuk" => $user_perbend,
            "tanggal_jam_perbendaharaan_spm_masuk" => $tanggal_perbend[2] . "-" . $tanggal_perbend[1] . "-" . $tanggal_perbend[0] . " " . $tanggal_jam_perbend[1],
            "status_spm" => "2",
        );

        $status = $this->trx_spm_model->edit($id_trx, $data_perbend);

        $data_track_position = $this->trx_spm_track_position_model->get(
            array(
                "where" => array(
                    "trx_spm_id" => $id_trx
                ),
                "order_by" => array(
                    "id_trx_spm_track_position" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        if ($data_track_position->position_spm == "2") {
            $data = array(
                "tanggal_jam_position" => $tanggal_perbend[2] . "-" . $tanggal_perbend[1] . "-" . $tanggal_perbend[0] . " " . $tanggal_jam_perbend[1],
                "updated_at" => $this->datetime(),
                "id_user_updated" => $this->session->userdata("id_user")
            );

            $this->trx_spm_track_position_model->edit($data_track_position->id_trx_spm_track_position, $data);
        } else {
            $data = array(
                "trx_spm_id" => $id_trx,
                "position_spm" => "2",
                "tanggal_jam_position" => $tanggal_perbend[2] . "-" . $tanggal_perbend[1] . "-" . $tanggal_perbend[0] . " " . $tanggal_jam_perbend[1],
                "created_at" => $this->datetime(),
                "id_user_created" => $this->session->userdata("id_user")
            );

            $this->trx_spm_track_position_model->save($data);
        }



        $param = false;
        if ($status) {
            $param = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($param));
    }

    public function delete_user_perbend()
    {
        $id_trx = decrypt_data($this->iget("id"));

        $data_master = $this->trx_spm_model->get_by($id_trx);

        if (!$data_master) {
            $this->page_error();
        }

        $data_trx_spm = $this->trx_spm_model->get(
            array(
                "fields" => "status_spm",
                "where" => array(
                    "spm_id" => $data_master->spm_id
                ),
                "order_by" => array(
                    "trx_spm.created_at" => "DESC"
                ),
                "limit" => "1"
            ),
            "row"
        );

        if (in_array($data_trx_spm->status_spm, array('3', '4'))) {
            $status = false;
        } else {
            $data_update = array(
                "user_perbendaharaan_spm_masuk" => NULL,
                "tanggal_jam_perbendaharaan_spm_masuk" => NULL,
                "status_spm" => "1",

            );

            $status = $this->trx_spm_model->edit($id_trx, $data_update);

            $data_track_position = $this->trx_spm_track_position_model->get(
                array(
                    "where" => array(
                        "trx_spm_id" => $id_trx,
                        "position_spm" => "2"
                    )
                ),
                "row"
            );

            $data_delete_track_position = array(
                "deleted_at" => $this->datetime(),
                "id_user_deleted" => $this->session->userdata("id_user")
            );

            $this->trx_spm_track_position_model->edit($data_track_position->id_trx_spm_track_position, $data_delete_track_position);
            $status = true;
        }


        $param = false;
        if ($status) {
            $param = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($param));
    }
}
