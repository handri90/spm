<style>
    .rght-border {
        border-right: 1px solid #ddd;
        padding-right: 20px;
    }

    #AnyTime--anytime-both {
        left: 330px !important;
        top: 50px !important;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>SKPD : </label>
                        <select class="form-control select-search" name="skpd" onChange="get_data_spm_pengembalian()">
                            <option value="">-- PILIH --</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="#formulirSPMPengembalian" id="formulirSPMPengembalian" class="btn btn-info">Formulir SPM Pengembalian</a>
            </div>
        </div>
        <table id="datatableSPMPengembalian" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Tanggal Pengembalian</th>
                    <th>Nomor</th>
                    <th>Nomor SPM</th>
                    <th>SKPD</th>
                    <th>Nilai</th>
                    <th>Jenis Pengembalian</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div id="modalRegisterSPM" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> List SPM</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="card border-top-success">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>SKPD : </label>
                                    <select class="form-control select-search" name="skpd_register_spm" onChange="get_data_spm()">
                                        <option value="">-- PILIH --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-table">
                    <table id="datatableRegisterSPM" class="table datatable-save-state table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>NO SPM</th>
                                <th>SKPD</th>
                                <th>Uraian</th>
                                <th>Program</th>
                                <th>Kegiatan</th>
                                <th>Sub Kegiatan</th>
                                <th>Nilai</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalFormulirSPM" class="modal fade">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Formulir SPM Pengembalian</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="row">
                    <div class="col-md-6 rght-border">
                        <div class="card-body">

                            <table class="table table-dark bg-slate-600">
                                <tr>
                                    <td colspan="3">Data Terima SPM</td>
                                </tr>
                                <tr>
                                    <td>Nomor</td>
                                    <td>:</td>
                                    <td><span class="terima_spm_nomor_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>Tanggal</td>
                                    <td>:</td>
                                    <td><span class="terima_spm_tanggal_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>Nilai SPM</td>
                                    <td>:</td>
                                    <td><span class="terima_spm_nilai_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>Catatan</td>
                                    <td>:</td>
                                    <td><span class="terima_spm_catatan"></span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="card-body">

                            <table class="table table-dark bg-slate-600">
                                <tr>
                                    <td colspan="3">Data SPM</td>
                                </tr>
                                <tr>
                                    <td>Nomor</td>
                                    <td>:</td>
                                    <td><span class="nomor_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>Tanggal</td>
                                    <td>:</td>
                                    <td><span class="tanggal_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>SKPD</td>
                                    <td>:</td>
                                    <td><span class="nama_skpd"></span></td>
                                </tr>
                                <tr>
                                    <td>Program</td>
                                    <td>:</td>
                                    <td><span class="nama_program"></span></td>
                                </tr>
                                <tr>
                                    <td>Kegiatan</td>
                                    <td>:</td>
                                    <td><span class="nama_kegiatan"></span></td>
                                </tr>
                                <tr>
                                    <td>Sub Kegiatan</td>
                                    <td>:</td>
                                    <td><span class="nama_sub_kegiatan"></span></td>
                                </tr>
                                <tr>
                                    <td>Nilai SPM</td>
                                    <td>:</td>
                                    <td><span class="nilai_spm"></span></td>
                                </tr>
                                <tr>
                                    <td>Sumber Dana</td>
                                    <td>:</td>
                                    <td><span class="sumber_dana"></span></td>
                                </tr>
                                <tr>
                                    <td>Uraian</td>
                                    <td>:</td>
                                    <td><span class="uraian"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-4">Tanggal <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <input type="hidden" class="form-control" name="id_spm">
                                <input type="hidden" class="form-control" name="id_trx_spm">
                                <input type="text" class="form-control daterange-single" name="tanggal_registrasi" readonly placeholder="Tanggal">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-4">Nomor</label>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-3">
                                        <input type="text" class="form-control" readonly name="nomor_registrasi_pertama" placeholder="XXX">
                                    </div>
                                    <div class="col-2">
                                        <input type="text" class="form-control" readonly name="nomor_registrasi_kedua" placeholder="XXX">
                                    </div>
                                    <div class="col-2">
                                        <input type="text" class="form-control" readonly name="nomor_registrasi_ketiga" placeholder="III">
                                    </div>
                                    <div class="col-3">
                                        <input type="text" class="form-control" readonly name="nomor_registrasi_keempat" placeholder="BPKAD">
                                    </div>
                                    <div class="col-2">
                                        <input type="text" class="form-control" readonly name="nomor_registrasi_kelima" placeholder="Tahun">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-4">Kuasa Bendahara Umum <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <select class="form-control select-search" name="list_kbu">
                                    <option value="">-- PILIH --</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-lg-4">Jenis Pengembalian <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <span class="list_jenis_pengembalian"></span>
                            </div>
                        </div>
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">Catatan <span class="text-danger">*</span>
                            </legend>

                            <div class="form-group row">
                                <label class="col-form-label col-lg-1">1 :</label>
                                <div class="col-lg-11">
                                    <input type="text" class="form-control" name="catatan_1" placeholder="Catatan">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-1">2 :</label>
                                <div class="col-lg-11">
                                    <input type="text" class="form-control" name="catatan_2" placeholder="Catatan">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label col-lg-1">3 :</label>
                                <div class="col-lg-11">
                                    <input type="text" class="form-control" name="catatan_3" placeholder="Catatan">
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="action_form_spm()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const IDR = value => currency(
        value, {
            symbol: "Rp. ",
            precision: 0,
            separator: "."
        });

    let datatableSPMPengembalian = $("#datatableSPMPengembalian").DataTable({
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            {
                "width": "200"
            }
        ]
    });

    get_data_spm_pengembalian();

    function get_data_spm_pengembalian() {
        let skpd = $("select[name=skpd]").val();
        datatableSPMPengembalian.clear().draw();
        $.ajax({
            url: base_url + 'spm_pengembalian/request/get_data_spm_pengembalian',
            data: {
                skpd: skpd
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    let str_nilai = "";
                    let spl_nilai = value.nilai.split(",");
                    str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

                    datatableSPMPengembalian.row.add([
                        value.tanggal_spm_trx,
                        value.no_register,
                        value.no_spm,
                        value.nama,
                        str_nilai,
                        (value.jenis_pengembalian == '1' ? '<span class="badge badge-danger">Permanen</span>' : (value.jenis_pengembalian == '2' ? '<span class="badge badge-warning">Perbaikan</span>' : (value.jenis_pengembalian == '3' ? '<span class="badge badge-info">Perbaikan Disetujui</span>' : ''))),
                        "<a href='#' onClick=\"edit_spm_pengembalian('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + value.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a> " + (value.jenis_pengembalian == '1' ? '' : "<a href='#' onClick=\"approve_pengembalian_perbaikan('" + value.id_encrypt + "')\" class='btn btn-success btn-icon'><i class='icon-check'></i></a> ") + "<a target='_blank' class='btn btn-warning btn-icon' href='" + base_url + "spm_pengembalian/cetak_spm_pengembalian/" + value.id_encrypt + "'><i class='icon-printer'></i></a>"
                    ]).draw(false);
                });
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function confirm_delete(id_trx_spm) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'spm_pengembalian/delete_spm_pengembalian',
                    data: {
                        id_trx_spm: id_trx_spm
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_data_spm_pengembalian();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            get_data_spm_pengembalian();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_data_spm_pengembalian();
                    }
                });
            }
        });
    }

    function approve_pengembalian_perbaikan(id_trx_spm) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menyetujui data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'spm_pengembalian/approve_spm_pengembalian_perbaikan',
                    data: {
                        id_trx_spm: id_trx_spm
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            get_data_spm_pengembalian();
                            swalInit(
                                'Berhasil',
                                'Data sudah disetujui',
                                'success'
                            );
                        } else {
                            get_data_spm_pengembalian();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa disetujui',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        get_data_spm_pengembalian();
                    }
                });
            }
        });
    }

    $(function() {
        $("#formulirSPMPengembalian").on("click", function() {
            $("#modalRegisterSPM").modal("show");
            get_data_spm();
        });

        $('.modal').on("hidden.bs.modal", function(e) { //fire on closing modal box
            if ($('.modal:visible').length) { // check whether parent modal is opend after child modal close
                $('body').addClass('modal-open'); // if open mean length is 1 then add a bootstrap css class to body of the page
            }
        });

        $('.daterange-single').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'), 10)
        });
    });

    function get_data_spm() {
        let datatableRegisterSPM = $("#datatableRegisterSPM").DataTable();

        let skpd = $("select[name='skpd_register_spm']").val();

        if (!skpd) {
            skpd = $("select[name='skpd']").val();
            list_skpd_register_spm($("select[name='skpd']").find("option:selected").attr("data-id"));
        } else {
            list_skpd($("select[name='skpd_register_spm']").find("option:selected").attr("data-id"));
        }

        datatableRegisterSPM.clear().draw();

        if (skpd) {
            $.ajax({
                url: base_url + 'spm_pengembalian/request/get_data_spm',
                data: {
                    skpd: skpd
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        let str_nilai = "";
                        let spl_nilai = value.nilai.split(",");
                        str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

                        datatableRegisterSPM.row.add([
                            value.no_spm,
                            value.nama,
                            value.uraian,
                            value.nama_program,
                            value.nama_kegiatan,
                            value.nama_sub_kegiatan,
                            str_nilai,
                            "<a href='#' onClick=\"show_formulir_spm('" + value.id_encrypt + "')\" class='btn btn-primary btn-icon'><i class='icon-touch'></i></a>"
                        ]).draw(false);
                    });
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    function get_data_kbu(param_selected = "") {
        let html = "<option value=''>-- PILIH --</option>";
        $.ajax({
            url: base_url + 'spm_pengembalian/request/get_data_kbu',
            type: 'GET',
            success: function(response) {
                $.each(response, function(index, value) {
                    let selected = "";
                    if (value.id_user == param_selected) {
                        selected = "selected";
                    }
                    html += "<option " + selected + " value='" + value.id_encrypt + "'>" + value.nama_lengkap + "</option>";
                });

                $("select[name='list_kbu']").html(html);
            }
        });
    }

    function show_formulir_spm(id_spm) {
        $("#modalRegisterSPM").modal("toggle");
        $.ajax({
            url: base_url + 'spm_pengembalian/request/get_detail_spm',
            data: {
                id_spm: id_spm
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let str_nilai = "";
                let spl_nilai = response.nilai.split(",");
                str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

                $("#modalFormulirSPM").modal("show");
                $(".alert_form").html("");
                $(".terima_spm_tanggal_spm").html(response.tanggal_jam_penyerahan_spm_masuk);
                $(".terima_spm_nomor_spm").html(response.no_register);
                $(".terima_spm_nilai_spm").html(str_nilai);
                $(".terima_spm_catatan").html(response.catatan);
                $(".tanggal_spm").html(response.tanggal_spm);
                $(".nomor_spm").html(response.no_spm);
                $(".nama_skpd").html(response.nama);
                $(".nama_program").html(response.nama_program);
                $(".nama_kegiatan").html(response.nama_kegiatan);
                $(".nama_sub_kegiatan").html(response.nama_sub_kegiatan);
                $(".nilai_spm").html(str_nilai);
                $(".sumber_dana").html(response.nama_sumber_pendanaan);
                $(".uraian").html(response.uraian);
                $("input[name='id_spm']").val(id_spm);
                $("input[name='id_trx_spm']").val("");
                $("input[name='nomor_registrasi_pertama']").val(response.jumlah_trx_spm);
                $("input[name='nomor_registrasi_kedua']").val("BLK");
                $("input[name='nomor_registrasi_ketiga']").val("III");
                $("input[name='nomor_registrasi_keempat']").val("BPKAD");
                $("input[name='nomor_registrasi_kelima']").val(moment().format('YYYY'));
                $("input[name='catatan_1']").val("");
                $("input[name='catatan_2']").val("");
                $("input[name='catatan_3']").val("");
                get_data_kbu();
                list_jenis_pengembalian();
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function action_form_spm() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        let tanggal_register = $("input[name='tanggal_registrasi']").val();
        let nomor_registrasi_pertama = $("input[name='nomor_registrasi_pertama']").val();
        let nomor_registrasi_kedua = $("input[name='nomor_registrasi_kedua']").val();
        let nomor_registrasi_ketiga = $("input[name='nomor_registrasi_ketiga']").val();
        let nomor_registrasi_keempat = $("input[name='nomor_registrasi_keempat']").val();
        let nomor_registrasi_kelima = $("input[name='nomor_registrasi_kelima']").val();
        let jenis_pengembalian = $("input[name='jenis_pengembalian']:checked").val();
        let catatan_1 = $("input[name='catatan_1']").val();
        let catatan_2 = $("input[name='catatan_2']").val();
        let catatan_3 = $("input[name='catatan_3']").val();
        let kbu = $("select[name='list_kbu']").val();
        let id_spm = $("input[name='id_spm']").val();
        let id_trx_spm = $("input[name='id_trx_spm']").val();

        if (!tanggal_register) {
            $(".alert_form").html("<div class='alert alert-danger'>Tanggal tidak boleh kosong.</div>");
        } else if (!catatan_1 && !catatan_2 && !catatan_3) {
            $(".alert_form").html("<div class='alert alert-danger'>Catatan tidak boleh kosong.</div>");
        } else if (!kbu) {
            $(".alert_form").html("<div class='alert alert-danger'>Kuasa Bendahara Umum tidak boleh kosong.</div>");
        } else if (typeof(jenis_pengembalian) === 'undefined' || jenis_pengembalian === 'false') {
            $(".alert_form").html("<div class='alert alert-danger'>Jenis Pengembalian tidak boleh kosong.</div>");
        } else {
            $.ajax({
                url: base_url + 'spm_pengembalian/act_spm_pengembalian',
                data: {
                    tanggal_register: tanggal_register,
                    nomor_registrasi_pertama: nomor_registrasi_pertama,
                    nomor_registrasi_kedua: nomor_registrasi_kedua,
                    nomor_registrasi_ketiga: nomor_registrasi_ketiga,
                    nomor_registrasi_keempat: nomor_registrasi_keempat,
                    nomor_registrasi_kelima: nomor_registrasi_kelima,
                    jenis_pengembalian: jenis_pengembalian,
                    catatan_1: catatan_1,
                    catatan_2: catatan_2,
                    catatan_3: catatan_3,
                    kbu: kbu,
                    id_spm: id_spm,
                    id_trx_spm: id_trx_spm
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $(".alert_form").html("");
                    $(".terima_spm_tanggal_spm").html("");
                    $(".terima_spm_nomor_spm").html("");
                    $(".terima_spm_nilai_spm").html("");
                    $(".terima_spm_catatan").html("");
                    $(".tanggal_spm").html("");
                    $(".nomor_spm").html("");
                    $(".nama_skpd").html("");
                    $(".nama_program").html("");
                    $(".nama_kegiatan").html("");
                    $(".nama_sub_kegiatan").html("");
                    $(".nilai_spm").html("");
                    $(".sumber_dana").html("");
                    $(".uraian").html("");
                    $("input[name='id_spm']").val("");
                    $("input[name='id_trx_spm']").val("");
                    $("input[name='nomor_registrasi_pertama']").val("");
                    $("input[name='nomor_registrasi_kedua']").val("");
                    $("input[name='nomor_registrasi_ketiga']").val("");
                    $("input[name='nomor_registrasi_keempat']").val("");
                    $("input[name='nomor_registrasi_kelima']").val(moment().format('YYYY'));
                    $("input[name='catatan_1']").val("");
                    $("input[name='catatan_2']").val("");
                    $("input[name='catatan_3']").val("");
                    $("#modalFormulirSPM").modal("toggle");
                    if (response) {
                        get_data_spm_pengembalian();
                        if (id_trx_spm) {
                            swalInit(
                                'Berhasil',
                                'Data berhasil diubah',
                                'success'
                            );
                        } else {
                            swalInit(
                                'Berhasil',
                                'Data berhasil ditambahkan',
                                'success'
                            );
                        }
                    } else {
                        get_data_spm_pengembalian();
                        if (id_trx_spm) {
                            swalInit(
                                'Gagal',
                                'Data gagal diubah',
                                'error'
                            );
                        } else {
                            swalInit(
                                'Gagal',
                                'Data gagal ditambahkan',
                                'error'
                            );
                        }
                    }
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    list_skpd();

    function list_skpd(param_selected = "") {
        let html = "<option value=''>-- SEMUA --</option>";
        $.ajax({
            url: base_url + 'spm_pengembalian/request/get_data_skpd',
            type: 'GET',
            success: function(response) {
                $.each(response, function(index, value) {
                    let selected = "";
                    if (value.id_master_satker == param_selected) {
                        selected = "selected";
                    }
                    html += "<option data-id='" + value.id_master_satker + "' " + selected + " value='" + value.id_encrypt + "'>" + value.nama + "</option>";
                });

                $("select[name='skpd']").html(html);
                if (param_selected) {
                    get_data_spm_pengembalian();
                }
            }
        });
    }

    function list_skpd_register_spm(param_selected = "") {
        let html = "<option value=''>-- PILIH --</option>";
        $.ajax({
            url: base_url + 'spm_pengembalian/request/get_data_skpd',
            type: 'GET',
            success: function(response) {
                $.each(response, function(index, value) {
                    let selected = "";
                    if (value.id_master_satker == param_selected) {
                        selected = "selected";
                    }
                    html += "<option data-id='" + value.id_master_satker + "' " + selected + " value='" + value.id_encrypt + "'>" + value.nama + "</option>";
                });

                $("select[name='skpd_register_spm']").html(html);
            }
        });
    }

    function edit_spm_pengembalian(id_trx_spm) {
        $.ajax({
            url: base_url + 'spm_pengembalian/request/get_detail_spm_for_edit',
            data: {
                id_trx_spm: id_trx_spm
            },
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                let str_nilai_terima = "";
                let spl_nilai_terima = response.nilai_terima_spm.split(",");
                str_nilai_terima = IDR(spl_nilai_terima[0]).format(true) + (spl_nilai_terima[1] ? "," + spl_nilai_terima[1] : "");

                let str_nilai = "";
                let spl_nilai = response.nilai_data_spm.split(",");
                str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

                $("#modalFormulirSPM").modal("show");
                $(".alert_form").html("");
                $(".terima_spm_tanggal_spm").html(response.tanggal_terima_spm);
                $(".terima_spm_nomor_spm").html(response.nomor_terima_spm);
                $(".terima_spm_nilai_spm").html(str_nilai_terima);
                $(".terima_spm_catatan").html(response.catatan_terima_spm);
                $(".tanggal_spm").html(response.tanggal_data_spm);
                $(".nomor_spm").html(response.nomor_data_spm);
                $(".nama_skpd").html(response.skpd_data_spm);
                $(".nama_program").html(response.nama_program);
                $(".nama_kegiatan").html(response.nama_kegiatan);
                $(".nama_sub_kegiatan").html(response.nama_sub_kegiatan);
                $(".nilai_spm").html(str_nilai);
                $(".sumber_dana").html(response.nama_sumber_pendanaan);
                $(".uraian").html(response.uraian_data_spm);
                $("input[name='id_spm']").val(response.id_spm_encrypt);
                $("input[name='id_trx_spm']").val(response.id_encrypt);
                $("input[name='tanggal_registrasi']").val(response.tanggal_spm_trx);
                let spl_no_register = response.no_register.split("/");
                $("input[name='nomor_registrasi_pertama']").val(spl_no_register[0]);
                $("input[name='nomor_registrasi_kedua']").val(spl_no_register[1]);
                $("input[name='nomor_registrasi_ketiga']").val(spl_no_register[2]);
                $("input[name='nomor_registrasi_keempat']").val(spl_no_register[3]);
                $("input[name='nomor_registrasi_kelima']").val(spl_no_register[4]);
                let spl_catatan_spm = response.catatan.split("|");
                $("input[name='catatan_1']").val(spl_catatan_spm[0]);
                $("input[name='catatan_2']").val(spl_catatan_spm[1]);
                $("input[name='catatan_3']").val(spl_catatan_spm[2]);
                get_data_kbu(response.id_user_kbud_spm_pengembalian);
                list_jenis_pengembalian(response.jenis_pengembalian);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function list_jenis_pengembalian(param_checked = "") {
        let arr = [];
        arr[1] = "Permanen";
        arr[2] = "Perbaikan";

        let html = "";
        $.each(arr, function(index, value) {
            if (index != 0) {
                let checked = "";
                if (param_checked == index) {
                    checked = "checked";
                }
                html += "<div class='form-check form-check-inline'>" +
                    "<label class='form-check-label'>" +
                    "<input type='radio' class='form-input-styled' name='jenis_pengembalian' value='" + index + "' " + checked + ">" +
                    value +
                    "</label>" +
                    "</div>";
            }
        });
        $(".list_jenis_pengembalian").html(html);
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-pink-400'
        });
    }
</script>