<html>

<head>
    <style>
        body {
            font-family: Georgia, 'Times New Roman', serif;
            font-size: 10pt;
            font-weight: normal;
        }

        .head_print {
            text-align: center;
            font-weight: 100;
            line-height: 0.6;
        }

        .wd-number {
            width: 10px;
        }

        .wd-label {
            width: 100px;
        }

        .wd-label-2 {
            width: 60px;

        }

        .wd-content {
            width: 200px;
        }

        .wd-content-2 {
            width: 310px;
        }

        .wd-titik {
            width: 2px;
        }

        table {
            margin: 10px 0;
        }

        .tbl-sign {
            margin-top: 20px;
            text-align: center;
        }

        .ruler {
            border-bottom: 2px double black;
        }

        .logo {
            float: left;
            width: 20%;
        }

        .deskripsi {
            float: left;
            width: 60%;
        }

        .alamat {
            line-height: 0.5;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>

<body>
    <table style="width:100%;margin-top:20px">
        <tr>
            <td rowspan="5" width="100"><img src="./assets/template_admin/smallogo.gif" width="60" height="80" /> </td>
            <td class="text-center">Pemerintah Kabupaten Kotawaringin Barat</td>
            <td rowspan="5" width="100"></td>
        </tr>
        <tr>
            <td class="text-center">BADAN PENGELOLA KEUANGAN DAN ASET DAERAH</td>
        </tr>
        <tr>
            <td class="text-center">Jl. Sutan Syahrir Nomor 41 Pangkalan Bun 74112 Telp. (0532) 21412</td>
        </tr>
        <tr>
            <td class="text-center">E-mail : bpkad.kotawaringinbarat@gmail.com</td>
        </tr>
        <tr>
            <td class="text-center">Website : www.dpkd.kotawaringinbarat.go.id</td>
        </tr>
        <tr>
            <td colspan="3" height="10">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" style="border-top:3px double #000000;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" class="text-center">SURAT PENOLAKAN PENERBITAN SP2D</td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%">
                    <tr>
                        <td width="80">&nbsp;</td>
                        <td width="20">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td colspan="2" valign="top">Pangkalan Bun, <?php echo date_indo(date("Y-m-d")); ?></td>
                    </tr>
                    <tr>
                        <td>Nomor</td>
                        <td>:</td>
                        <td><?php echo isset($content) ? $content->no_register : ""; ?></td>
                        <td width="30">&nbsp;</td>
                        <td width="250">&nbsp;&nbsp;&nbsp;&nbsp;K e p a d a</td>
                    </tr>
                    <tr>
                        <td>Lampiran</td>
                        <td>:</td>
                        <td>1 (satu) berkas</td>
                        <td>Yth.</td>
                        <td>Pengguna Anggaran</td>
                    </tr>
                    <tr>
                        <td>Perihal</td>
                        <td>:</td>
                        <td><u>Pengembalian SPM</u></td>
                        <td>&nbsp;</td>
                        <td><?php echo isset($content) ? $content->nama : ""; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Di-</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;PANGKALAN BUN</td>
                    </tr>
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td colspan="4">Bersama ini terlampir :</td>
                                </tr>
                                <tr>
                                    <td>a.</td>
                                    <td>SPM Nomor</td>
                                    <td>:</td>
                                    <td><?php echo isset($content) ? $content->no_spm : ""; ?></td>
                                </tr>
                                <tr>
                                    <td>b.</td>
                                    <td>Tanggal</td>
                                    <td>:</td>
                                    <td><?php echo isset($content) ? $content->tanggal_spm : ""; ?></td>
                                </tr>
                                <tr>
                                    <td>c.</td>
                                    <td>Nilai</td>
                                    <td>:</td>
                                    <td>
                                        <?php
                                        if (isset($content)) {
                                            $str_nilai = "";
                                            $spl_nilai = explode(",", $content->nilai);
                                            $str_nilai = nominal($spl_nilai[0]) . ($spl_nilai[1] ? "," . $spl_nilai[1] : "");
                                            echo $str_nilai;
                                        } else {
                                            echo "";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>d.</td>
                                    <td>Sumber Dana</td>
                                    <td>:</td>
                                    <td><?php echo isset($content) ? $content->nama_sumber_pendanaan : ""; ?></td>
                                </tr>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="3">Dikembalikan karena tidak memenuhi syarat untuk diproses. Adapun kekurangannya sebagai berikut :</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="3">
                            <?php
                            $str = "";
                            $expl_catatan = explode("|", $content->catatan);
                            if ($expl_catatan) {
                                $n = 1;
                                for ($i = 0; $i < count($expl_catatan); $i++) {
                                    if ($expl_catatan[$i]) {
                                        $str .= "- " . $expl_catatan[$i] . "<br>";
                                    }
                                    $n++;
                                }
                            }
                            echo $str;
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" height="40">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="3">
                            SPM dapat diajukan kembali setelah dilakukan perbaikan sesuai ketentuan.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="3">
                            Demikian disampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="tbl-sign" width="100%">
        <tr>
            <td>&nbsp;</td>
            <td style="width:40px"></td>
            <td>Kuasa Bendahara Umum Daerah</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td>(KBUD)</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="padding-top:100px;">&nbsp;</td>
            <td></td>
            <td style="padding-top:100px;"><?php echo isset($content) ? $content->nama_lengkap : ""; ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>NIP. <?php echo isset($content) ? $content->nip : ""; ?></td>
        </tr>
    </table>
</body>

</html>