<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Spm_pengembalian extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('trx_spm_model');
    }

    public function index()
    {

        $data['breadcrumb'] = [['link' => false, 'content' => 'SPM Pengembalian', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function act_spm_pengembalian()
    {
        $tanggal_register = explode("-", $this->ipost("tanggal_register"));

        if ($this->ipost("id_trx_spm")) {
            $data = array(
                "tanggal_spm_trx" => $tanggal_register[2] . "-" . $tanggal_register[1] . "-" . $tanggal_register[0],
                "catatan" => $this->ipost("catatan_1") . "|" . $this->ipost("catatan_2") . "|" . $this->ipost("catatan_3"),
                "status_spm" => "3",
                "jenis_pengembalian" => $this->ipost("jenis_pengembalian"),
                "id_user_kbud_spm_pengembalian" => decrypt_data($this->ipost("kbu")),
                "tanggal_ditolak" => $this->datetime(),
                "updated_at" => $this->datetime(),
                "id_user_updated" => $this->session->userdata("id_user")
            );

            $status = $this->trx_spm_model->edit(decrypt_data($this->ipost("id_trx_spm")), $data);
        } else {

            $data = array(
                "spm_id" => decrypt_data($this->ipost("id_spm")),
                "tanggal_spm_trx" => $tanggal_register[2] . "-" . $tanggal_register[1] . "-" . $tanggal_register[0],
                "no_register" => $this->ipost("nomor_registrasi_pertama") . "/" . $this->ipost("nomor_registrasi_kedua") . "/" . $this->ipost("nomor_registrasi_ketiga") . "/" . $this->ipost("nomor_registrasi_keempat") . "/" . $this->ipost("nomor_registrasi_kelima"),
                "catatan" => $this->ipost("catatan_1") . "|" . $this->ipost("catatan_2") . "|" . $this->ipost("catatan_3"),
                "status_spm" => "3",
                "jenis_pengembalian" => $this->ipost("jenis_pengembalian"),
                "id_user_kbud_spm_pengembalian" => decrypt_data($this->ipost("kbu")),
                "tanggal_ditolak" => $this->datetime(),
                "created_at" => $this->datetime(),
                "id_user_created" => $this->session->userdata("id_user")
            );

            $status = $this->trx_spm_model->save($data);
        }


        $param = false;
        if ($status) {
            $param = true;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($param));
    }

    public function delete_spm_pengembalian()
    {
        $id_trx_spm = $this->iget('id_trx_spm');
        $data_master = $this->trx_spm_model->get_by(decrypt_data($id_trx_spm));

        if (!$data_master) {
            $this->page_error();
        }

        $status = $this->trx_spm_model->remove(decrypt_data($id_trx_spm));
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function approve_spm_pengembalian_perbaikan()
    {
        $id_trx_spm = $this->iget('id_trx_spm');
        $data_master = $this->trx_spm_model->get_by(decrypt_data($id_trx_spm));

        if (!$data_master) {
            $this->page_error();
        }

        $data = array(
            "jenis_pengembalian" => "3",
            "tanggal_perbaikan_diterima" => $this->datetime()
        );

        $status = $this->trx_spm_model->edit(decrypt_data($id_trx_spm), $data);
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function cetak_spm_pengembalian($id_trx_spm)
    {
        $data['content'] = $this->trx_spm_model->get(
            array(
                "fields" => "trx_spm.*,no_spm,nilai,uraian,nama_program,nama_kegiatan,nama_sub_kegiatan,nama,tanggal_spm_trx,DATE_FORMAT(tanggal_spm,'%d-%m-%Y') as tanggal_spm,id_spm,nama_lengkap,nama_sumber_pendanaan,nip",
                "join" => array(
                    "spm" => "id_spm=spm_id AND spm.deleted_at IS NULL",
                    "master_satker" => "id_master_satker=spm.master_satker_id",
                    "user" => "trx_spm.id_user_kbud_spm_pengembalian=id_user",
                    "master_sumber_pendanaan" => "id_master_sumber_pendanaan=master_sumber_pendanaan_id",
                ),
                "left_join" => array(
                    "referensi_spm" => "id_referensi_spm=referensi_spm_id",
                    "master_program" => "id_master_program=referensi_spm.master_program_id",
                    "master_kegiatan" => "id_master_kegiatan=referensi_spm.master_kegiatan_id",
                    "master_sub_kegiatan" => "id_master_sub_kegiatan=referensi_spm.master_sub_kegiatan_id",
                ),
                "where" => array(
                    "id_trx_spm" => decrypt_data($id_trx_spm)
                )
            ),
            "row"
        );

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'Legal', 'tempDir' => '/tmp']);
        $data = $this->load->view('cetak_spm_pengembalian', $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }
}
