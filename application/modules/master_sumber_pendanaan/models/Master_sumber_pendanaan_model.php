<?php

class Master_sumber_pendanaan_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "master_sumber_pendanaan";
        $this->primary_id = "id_master_sumber_pendanaan";
    }
}
