<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Monitoring_spm extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('spm_model');
        $this->load->model('import_spm/master_sub_kegiatan_model', 'master_sub_kegiatan_model');
        $this->load->model('master_sumber_pendanaan/master_sumber_pendanaan_model', 'master_sumber_pendanaan_model');
        $this->load->model('master_rekanan/master_rekanan_model', 'master_rekanan_model');
        $this->load->model('import_spm/referensi_spm_model', 'referensi_spm_model');
        $this->load->model('trx_spm_model');
    }

    public function index()
    {

        $data['breadcrumb'] = [['link' => false, 'content' => 'Monitoring SPM', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function tambah_spm()
    {
        if (empty($_POST)) {
            $data['list_sub_kegiatan'] = $this->master_sub_kegiatan_model->get(
                array(
                    "fields" => "master_sub_kegiatan.*,nama_program,nama_kegiatan",
                    "join"   => array(
                        "master_kegiatan" => "id_master_kegiatan=master_kegiatan_id",
                        "master_program"  => "id_master_program=master_program_id"
                    ),
                    'order_by' => array(
                        'nama_program'      => 'ASC',
                        'nama_kegiatan'     => 'ASC',
                        'nama_sub_kegiatan' => 'ASC'
                    ),
                    "where_false" => "id_master_sub_kegiatan IN (select master_sub_kegiatan_id from referensi_spm where (master_satker_id='" . $this->session->userdata('master_satker_id') . "' OR master_satker_id IN (select id_master_satker from master_satker where skpd_induk_id = '{$this->session->userdata('master_satker_id')}')) and referensi_spm.deleted_at is null group by master_sub_kegiatan_id)"
                )
            );

            $data['list_sumber_pendanaan'] = $this->master_sumber_pendanaan_model->get(
                array(
                    'order_by' => array(
                        'urutan' => 'ASC'
                    )
                )
            );

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'monitoring_spm', 'content' => 'Monitoring SPM', 'is_active' => false], ['link' => false, 'content' => 'Tambah SPM', 'is_active' => true]];
            $this->execute('form_spm', $data);
        } else {

            $expl_tanggal_spm = explode('-', $this->ipost("tanggal_spm"));

            $data_referensi_spm = $this->referensi_spm_model->get(
                array(
                    "where" => array(
                        "master_sub_kegiatan_id"           => decrypt_data($this->ipost("sub_kegiatan")),
                        "master_kode_sub_rincian_objek_id" => decrypt_data($this->ipost("sub_rincian_objek")),
                    )
                ),
                "row"
            );

            $data = array(
                "master_satker_id"                 => $this->session->userdata('master_satker_id'),
                "master_sumber_pendanaan_id"       => decrypt_data($this->ipost('sumber_pendanaan')),
                "master_rekanan_id"                => decrypt_data($this->ipost('nama_rekanan')),
                'no_spm'                           => $this->ipost('no_spm'),
                'tanggal_spm'                      => $expl_tanggal_spm[2] . "-" . $expl_tanggal_spm[1] . "-" . $expl_tanggal_spm[0],
                'nilai'                            => replace_dot($this->ipost("nilai")),
                'uraian'                           => $this->ipost("uraian"),
                'uraian'                           => $this->ipost("uraian"),
                'referensi_spm_id'                 => $data_referensi_spm->id_referensi_spm,
                'master_sub_kegiatan_id'           => decrypt_data($this->ipost("sub_kegiatan")),
                'master_kode_sub_rincian_objek_id' => decrypt_data($this->ipost("sub_rincian_objek")),
                'status_up_gu'                       => '1',
                'created_at'                       => $this->datetime(),
                'id_user_created'                  => $this->session->userdata("id_user")
            );

            $status = $this->spm_model->save($data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }


            redirect('monitoring_spm');
        }
    }

    public function tambah_spm_up_gu()
    {
        if (empty($_POST)) {
            $data['list_sumber_pendanaan'] = $this->master_sumber_pendanaan_model->get(
                array(
                    'order_by' => array(
                        'urutan' => 'ASC'
                    )
                )
            );

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'monitoring_spm', 'content' => 'Monitoring SPM', 'is_active' => false], ['link' => false, 'content' => 'Tambah SPM', 'is_active' => true]];
            $this->execute('form_spm_up_gu', $data);
        } else {

            $expl_tanggal_spm = explode('-', $this->ipost("tanggal_spm"));

            $data = array(
                "master_satker_id"           => $this->session->userdata('master_satker_id'),
                "master_sumber_pendanaan_id" => decrypt_data($this->ipost('sumber_pendanaan')),
                'no_spm'                     => $this->ipost('no_spm'),
                'tanggal_spm'                => $expl_tanggal_spm[2] . "-" . $expl_tanggal_spm[1] . "-" . $expl_tanggal_spm[0],
                'nilai'                      => replace_dot($this->ipost("nilai")),
                'uraian'                     => $this->ipost("uraian"),
                'status_up_gu'                 => '2',
                'created_at'                 => $this->datetime(),
                'id_user_created'            => $this->session->userdata("id_user")
            );

            $status = $this->spm_model->save($data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data baru berhasil ditambahkan');
            } else {
                $this->session->set_flashdata('message', 'Data baru gagal ditambahkan');
            }


            redirect('monitoring_spm');
        }
    }

    public function edit_spm($id_spm)
    {
        $data_master = $this->spm_model->get(
            array(
                "fields" => "spm.*,DATE_FORMAT(tanggal_spm,'%d-%m-%Y') as tanggal_spm",
                "where"  => array(
                    "id_spm" => decrypt_data($id_spm)
                )
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }

        //cek trx

        $cek_trx = $this->trx_spm_model->get(
            array(
                "where" => array(
                    "spm_id" => decrypt_data($id_spm)
                )
            ),
            "row"
        );
        if ($cek_trx) {
            $this->session->set_flashdata('message', 'Data tidak bisa diubah. Sudah Terjadi Transaksi');
            redirect('monitoring_spm');
        }

        if (empty($_POST)) {
            $data['content']           = $data_master;
            $data['list_sub_kegiatan'] = $this->master_sub_kegiatan_model->get(
                array(
                    "fields" => "master_sub_kegiatan.*,nama_program,nama_kegiatan",
                    "join"   => array(
                        "master_kegiatan" => "id_master_kegiatan=master_kegiatan_id",
                        "master_program"  => "id_master_program=master_program_id"
                    ),
                    'order_by' => array(
                        'nama_program'      => 'ASC',
                        'nama_kegiatan'     => 'ASC',
                        'nama_sub_kegiatan' => 'ASC'
                    ),
                    "where_false" => "id_master_sub_kegiatan IN (select master_sub_kegiatan_id from referensi_spm where (master_satker_id='" . $this->session->userdata('master_satker_id') . "' OR master_satker_id IN (select id_master_satker from master_satker where skpd_induk_id = '{$this->session->userdata('master_satker_id')}')) and referensi_spm.deleted_at is null group by master_sub_kegiatan_id)"
                )
            );

            $data['list_sumber_pendanaan'] = $this->master_sumber_pendanaan_model->get(
                array(
                    'order_by' => array(
                        'urutan' => 'ASC'
                    )
                )
            );

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'monitoring_spm', 'content' => 'Monitoring SPM', 'is_active' => false], ['link' => false, 'content' => 'Ubah SPM', 'is_active' => true]];
            $this->execute('form_spm', $data);
        } else {

            $expl_tanggal_spm = explode('-', $this->ipost("tanggal_spm"));

            $data_referensi_spm = $this->referensi_spm_model->get(
                array(
                    "where" => array(
                        "master_sub_kegiatan_id"           => decrypt_data($this->ipost("sub_kegiatan")),
                        "master_kode_sub_rincian_objek_id" => decrypt_data($this->ipost("sub_rincian_objek")),
                    )
                ),
                "row"
            );

            $data = array(
                "master_satker_id"                 => $this->session->userdata('master_satker_id'),
                "master_sumber_pendanaan_id"       => decrypt_data($this->ipost('sumber_pendanaan')),
                "master_rekanan_id"                => decrypt_data($this->ipost('nama_rekanan')),
                'no_spm'                           => $this->ipost('no_spm'),
                'tanggal_spm'                      => $expl_tanggal_spm[2] . "-" . $expl_tanggal_spm[1] . "-" . $expl_tanggal_spm[0],
                'nilai'                            => replace_dot($this->ipost("nilai")),
                'uraian'                           => $this->ipost("uraian"),
                'referensi_spm_id'                 => $data_referensi_spm->id_referensi_spm,
                'master_sub_kegiatan_id'           => decrypt_data($this->ipost("sub_kegiatan")),
                'master_kode_sub_rincian_objek_id' => decrypt_data($this->ipost("sub_rincian_objek")),
                'updated_at'                       => $this->datetime(),
                'id_user_updated'                  => $this->session->userdata("id_user")
            );

            $status = $this->spm_model->edit(decrypt_data($id_spm), $data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('monitoring_spm');
        }
    }

    public function edit_spm_up_gu($id_spm)
    {
        $data_master = $this->spm_model->get(
            array(
                "fields" => "spm.*,DATE_FORMAT(tanggal_spm,'%d-%m-%Y') as tanggal_spm",
                "where"  => array(
                    "id_spm" => decrypt_data($id_spm)
                )
            ),
            "row"
        );

        if (!$data_master) {
            $this->page_error();
        }

        //cek trx

        $cek_trx = $this->trx_spm_model->get(
            array(
                "where" => array(
                    "spm_id" => decrypt_data($id_spm)
                )
            ),
            "row"
        );
        if ($cek_trx) {
            $this->session->set_flashdata('message', 'Data tidak bisa diubah. Sudah Terjadi Transaksi');
            redirect('monitoring_spm');
        }

        if (empty($_POST)) {
            $data['content']           = $data_master;
            $data['list_sumber_pendanaan'] = $this->master_sumber_pendanaan_model->get(
                array(
                    'order_by' => array(
                        'urutan' => 'ASC'
                    )
                )
            );

            $data['breadcrumb'] = [['link' => true, 'url' => base_url() . 'monitoring_spm', 'content' => 'Monitoring SPM', 'is_active' => false], ['link' => false, 'content' => 'Ubah SPM', 'is_active' => true]];
            $this->execute('form_spm_up_gu', $data);
        } else {

            $expl_tanggal_spm = explode('-', $this->ipost("tanggal_spm"));

            $data = array(
                "master_satker_id"           => $this->session->userdata('master_satker_id'),
                "master_sumber_pendanaan_id" => decrypt_data($this->ipost('sumber_pendanaan')),
                'no_spm'                     => $this->ipost('no_spm'),
                'tanggal_spm'                => $expl_tanggal_spm[2] . "-" . $expl_tanggal_spm[1] . "-" . $expl_tanggal_spm[0],
                'nilai'                      => replace_dot($this->ipost("nilai")),
                'uraian'                     => $this->ipost("uraian"),
                'updated_at'                 => $this->datetime(),
                'id_user_updated'            => $this->session->userdata("id_user")
            );

            $status = $this->spm_model->edit(decrypt_data($id_spm), $data);

            if ($status) {
                $this->session->set_flashdata('message', 'Data berhasil diubah');
            } else {
                $this->session->set_flashdata('message', 'Data gagal diubah');
            }

            redirect('monitoring_spm');
        }
    }

    public function delete_spm()
    {
        $id_spm      = $this->iget('id_spm');
        $data_master = $this->spm_model->get_by(decrypt_data($id_spm));

        if (!$data_master) {
            $this->page_error();
        }

        $cek_trx = $this->trx_spm_model->get(
            array(
                "where" => array(
                    "spm_id" => decrypt_data($id_spm)
                )
            ),
            "row"
        );
        if ($cek_trx) {
            $status = false;
        } else {
            $status = $this->spm_model->remove(decrypt_data($id_spm));
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }

    public function save_rekanan()
    {
        $data = array(
            "nama_rekanan"     => $this->ipost("nama_rekanan"),
            "master_satker_id" => $this->session->userdata("master_satker_id")
        );

        $status = $this->master_rekanan_model->save($data);

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($status));
    }
}
