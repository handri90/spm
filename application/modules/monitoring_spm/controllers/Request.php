<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->is_login();
        $this->load->model('trx_spm_model');
        $this->load->model('import_spm/master_kode_sub_rincian_objek_model', 'master_kode_sub_rincian_objek_model');
        $this->load->model('master_rekanan/master_rekanan_model', 'master_rekanan_model');
    }

    public function get_data_spm()
    {
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $skpd = decrypt_data($this->iget("skpd"));

        $data_spm = $this->trx_spm_model->query("
        SELECT id_spm,no_spm,nilai,no_register,tanggal_spm_trx,catatan,no_sp2d_penyelesaian,
        user_penyerahan_spm_masuk,
        DATE_FORMAT(tanggal_jam_penyerahan_spm_masuk,'%Y-%m-%d %H:%i:%s') AS tanggal_jam_penyerahan_spm_masuk,
        user_perbendaharaan_spm_masuk,
        DATE_FORMAT(tanggal_jam_perbendaharaan_spm_masuk,'%Y-%m-%d %H:%i:%s') AS tanggal_jam_perbendaharaan_spm_masuk,
        user_perbendaharaan_penyelesaian_sp2d,
        DATE_FORMAT(tanggal_jam_perbendaharaan_penyelesaian_sp2d,'%Y-%m-%d %H:%i:%s') AS tanggal_jam_perbendaharaan_penyelesaian_sp2d,
        a.status_spm,
        a.jenis_pengembalian,
        a.catatan,
        b.position_spm,
        a.id_trx_spm,
        status_up_gu
        FROM spm
        LEFT JOIN
        (
            SELECT *
            FROM trx_spm
            WHERE id_trx_spm IN 
                (
                SELECT MAX(id_trx_spm)
                FROM trx_spm
                WHERE trx_spm.deleted_at IS NULL
                GROUP BY spm_id
                )
            AND trx_spm.deleted_at IS NULL
        ) AS a ON a.spm_id=id_spm
        LEFT JOIN 
        (
                SELECT *
                FROM trx_spm_track_position
                WHERE id_trx_spm_track_position IN (
                SELECT MAX(id_trx_spm_track_position)
                FROM trx_spm_track_position
                WHERE trx_spm_track_position.deleted_at IS NULL
                GROUP BY trx_spm_id
                ) AND trx_spm_track_position.deleted_at IS NULL
        ) AS b ON b.trx_spm_id=id_trx_spm
        WHERE master_satker_id = {$this->session->userdata('master_satker_id')} AND spm.deleted_at IS NULL AND DATE_FORMAT(spm.created_at,'%Y-%m-%d') BETWEEN '${start_date}' AND '${end_date}' AND YEAR(tanggal_spm) = {$this->session->userdata('tahun')}
        ")->result();

        $templist = array();
        foreach ($data_spm as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_spm);
            if ($row->tanggal_jam_penyerahan_spm_masuk) {
                $exp_tggl_spm_masuk = explode(" ", $row->tanggal_jam_penyerahan_spm_masuk);
                $templist[$key]['tanggal_jam_penyerahan_spm_masuk'] = date_indo($exp_tggl_spm_masuk[0]) . ", " . $exp_tggl_spm_masuk[1];
            } else {
                $templist[$key]['tanggal_jam_penyerahan_spm_masuk'] = "";
            }

            if ($row->tanggal_jam_perbendaharaan_spm_masuk) {
                $exp_tggl_spm_masuk = explode(" ", $row->tanggal_jam_perbendaharaan_spm_masuk);
                $templist[$key]['tanggal_jam_perbendaharaan_spm_masuk'] = date_indo($exp_tggl_spm_masuk[0]) . ", " . $exp_tggl_spm_masuk[1];
            } else {
                $templist[$key]['tanggal_jam_perbendaharaan_spm_masuk'] = "";
            }

            $str = "";
            $expl_catatan = explode("|", $row->catatan);
            if ($expl_catatan) {
                $n = 1;
                for ($i = 0; $i < count($expl_catatan); $i++) {
                    if ($expl_catatan[$i]) {
                        $delimiter = "";

                        if ($i < count($expl_catatan) - 1 && $expl_catatan[$n]) {
                            $delimiter = ",";
                        }
                        $str .= $expl_catatan[$i] . $delimiter;
                    }
                    $n++;
                }
            }
            $templist[$key]['catatan'] = $str;
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_sub_rincian_objek()
    {
        $id = decrypt_data($this->iget("id"));

        $data_rincian = $this->master_kode_sub_rincian_objek_model->get(
            array(
                "order_by" => array(
                    "uraian_sub_rincian_objek" => "ASC"
                ),
                "where_false" => "id_master_kode_sub_rincian_objek IN (select master_kode_sub_rincian_objek_id from referensi_spm where master_satker_id = '" . $this->session->userdata('master_satker_id') . "' and master_sub_kegiatan_id = '" . $id . "' and referensi_spm.deleted_at is null)"
            )
        );

        $templist = array();
        foreach ($data_rincian as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_kode_sub_rincian_objek);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function get_data_rekanan()
    {
        $data_rekanan = $this->master_rekanan_model->get(
            array(
                "where" => array(
                    "master_satker_id" => $this->session->userdata("master_satker_id")
                ),
                "order_by" => array(
                    "nama_rekanan" => "ASC"
                )
            )
        );

        $templist = array();
        foreach ($data_rekanan as $key => $row) {
            foreach ($row as $keys => $rows) {
                $templist[$key][$keys] = $rows;
            }
            $templist[$key]['id_encrypt'] = encrypt_data($row->id_master_rekanan);
        }

        $data = $templist;
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
