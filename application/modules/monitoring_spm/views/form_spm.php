<style>
    .user-image-custom {
        margin-bottom: 10px;
    }

    .input-group {
        flex-wrap: initial;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Tambah SPM</legend>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nomor SPM <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="hidden" name="sub_kegiatan" class="form-control" value="<?php echo !empty($content) ? encrypt_data($content->master_sub_kegiatan_id) : ""; ?>">
                        <input type="hidden" name="sub_rincian_objek" class="form-control" value="<?php echo !empty($content) ? $content->master_kode_sub_rincian_objek_id : ""; ?>">
                        <input type="hidden" name="rekanan" class="form-control" value="<?php echo !empty($content) ? $content->master_rekanan_id : ""; ?>">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->no_spm : ""; ?>" name="no_spm" required placeholder="Nomor SPM">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal SPM <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control daterange-single" value="<?php echo !empty($content) ? $content->tanggal_spm : ""; ?>" name="tanggal_spm" placeholder="Tanggal">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Sumber Pendanaan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="sumber_pendanaan" required>
                            <option value="">-- Pilih Sumber Pendanaan --</option>
                            <?php
                            foreach ($list_sumber_pendanaan as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row->id_master_sumber_pendanaan == $content->master_sumber_pendanaan_id) {
                                        $selected = 'selected="selected"';
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_sumber_pendanaan); ?>"><?php echo $row->nama_sumber_pendanaan; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Sub Kegiatan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="sub_kegiatan" required onchange="get_sub_rincian_objek()">
                            <option value="">-- Pilih Sub Kegiatan --</option>
                            <?php
                            foreach ($list_sub_kegiatan as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row->id_master_sub_kegiatan == $content->master_sub_kegiatan_id) {
                                        $selected = 'selected="selected"';
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_sub_kegiatan); ?>"><?php echo $row->nama_program . " - " . $row->nama_kegiatan . " - " . $row->nama_sub_kegiatan; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Sub Rincian Objek <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="sub_rincian_objek" required>
                            <option value="">-- Pilih Sub Rincian Objek --</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nilai <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control input-currency" value="<?php echo !empty($content) ? $content->nilai : ""; ?>" name="nilai" required placeholder="Nilai">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Uraian <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <textarea placeholder="Uraian" name="uraian" class="form-control"><?php echo !empty($content) ? $content->uraian : ""; ?></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Rekanan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <div class="input-group">
                            <select class="form-control select-search border-right-0" name="nama_rekanan" required>
                                <option value="">-- Pilih Rekanan --</option>
                            </select>
                            <span class="input-group-append" onclick="show_form_rekanan()">
                                <button class="btn bg-teal" type="button"><i class="icon icon-database-add"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->

</div>

<div id="modalRekanan" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title_modal"></span> Tambah Rekanan</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span class="alert_form"></span>
                <div class="col-md-12">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Nama Rekanan <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="nama_rekanan" placeholder="Nama Rekanan">
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" onclick="save_rekanan()">Simpan <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function get_sub_rincian_objek(sub_kegiatan, sub_rincian) {
        let id = $("select[name='sub_kegiatan']").val();
        if (sub_kegiatan) {
            id = sub_kegiatan;
        }
        let html = "<option value=''>-- Pilih Sub Rincian Objek --</option>";
        if (id) {
            $.ajax({
                url: base_url + 'monitoring_spm/request/get_data_sub_rincian_objek',
                data: {
                    id: id
                },
                type: 'GET',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $.each(response, function(index, value) {
                        let selected = "";
                        if (value.id_master_kode_sub_rincian_objek == sub_rincian) {
                            selected = "selected";
                        }
                        html += "<option " + selected + " value='" + value.id_encrypt + "'>" + value.uraian_sub_rincian_objek + "</option>";
                    });
                    $("select[name='sub_rincian_objek']").html(html);
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        } else {
            $("select[name='sub_rincian_objek']").html(html);
        }
    }

    function get_data_rekanan(param_selected) {
        let html = "<option value=''>-- Pilih Rekanan --</option>";
        $.ajax({
            url: base_url + 'monitoring_spm/request/get_data_rekanan',
            type: 'GET',
            beforeSend: function() {
                HoldOn.open(optionsHoldOn);
            },
            success: function(response) {
                $.each(response, function(index, value) {
                    let selected = "";
                    if (value.id_master_rekanan == param_selected) {
                        selected = "selected";
                    }
                    html += "<option " + selected + " value='" + value.id_encrypt + "'>" + value.nama_rekanan + "</option>";
                });
                $("select[name='nama_rekanan']").html(html);
            },
            complete: function() {
                HoldOn.close();
            }
        });
    }

    function show_form_rekanan() {
        $("#modalRekanan").modal("show");
        $("input[name='nama_rekanan']").val("");
    }

    function save_rekanan() {
        let nama_rekanan = $("input[name='nama_rekanan']").val();

        if (!nama_rekanan) {
            $(".alert_form").html("<div class='alert alert-danger'>Nama Rekanan tidak boleh kosong.</div>");
        } else {
            $.ajax({
                url: base_url + 'monitoring_spm/save_rekanan',
                data: {
                    nama_rekanan: nama_rekanan
                },
                type: 'POST',
                beforeSend: function() {
                    HoldOn.open(optionsHoldOn);
                },
                success: function(response) {
                    $("input[name='nama_rekanan']").val("");
                    $("#modalRekanan").modal("toggle");
                    get_data_rekanan(response);
                },
                complete: function() {
                    HoldOn.close();
                }
            });
        }
    }

    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    var cleave = new Cleave('.input-currency', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand',
        numeralDecimalMark: ',',
        delimiter: '.',
    });

    $(function() {
        let sub_rincian_objek = $("input[name='sub_rincian_objek']").val();
        let sub_kegiatan = $("input[name='sub_kegiatan']").val();
        if (sub_rincian_objek) {
            get_sub_rincian_objek(sub_kegiatan, sub_rincian_objek);
        }

        let rekanan = $("input[name='rekanan']").val();
        get_data_rekanan(rekanan);
    })
</script>