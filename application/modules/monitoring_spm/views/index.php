<style>
    .rght-border {
        border-right: 1px solid #ddd;
        padding-right: 20px;
    }

    #AnyTime--anytime-both {
        left: 330px !important;
        top: 50px !important;
    }

    .badge {
        display: block !important;
        width: max-content;
    }

    .info-penyerahan {
        text-align: left;
        line-height: 2;
    }
</style>
<div class="content">
    <div class="card border-top-success">
        <div class="card-body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tanggal</label>
                <div class="col-lg-10">
                    <button type="button" class="btn btn-light daterange-predefined">
                        <i class="icon-calendar22 mr-2"></i>
                        <span></span>
                    </button>

                    <input type="hidden" name="start_date" />
                    <input type="hidden" name="end_date" />
                </div>
            </div>
        </div>
    </div>
    <div class="card card-table">
        <div class="card-body">
            <div class="text-right">
                <a href="<?php echo base_url() . 'monitoring_spm/tambah_spm_up_gu'; ?>" class="btn btn-info">Tambah UP/GU</a>
                <a href="<?php echo base_url() . 'monitoring_spm/tambah_spm'; ?>" class="btn btn-info">Tambah SPM</a>
            </div>
        </div>
        <table id="datatableSPM" class="table datatable-save-state table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nomor SPM</th>
                    <th>Nilai</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    const IDR = value => currency(
        value, {
            symbol: "Rp. ",
            precision: 0,
            separator: "."
        });

    function reload_datatable() {
        $('#datatableSPM').DataTable().ajax.reload();
    }

    $('.daterange-predefined').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            applyClass: 'btn-sm btn-primary',
            cancelClass: 'btn-sm btn-light'
        },
        function(start, end) {
            $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            $('input[name=start_date]').val(start.format('YYYY-MM-DD'));
            $('input[name=end_date]').val(end.format('YYYY-MM-DD'));
            $('#datatableSPM').DataTable().ajax.reload();
        }
    );

    // Display date format
    $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    $('input[name=start_date]').val(moment().subtract(29, 'days').format('YYYY-MM-DD'));
    $('input[name=end_date]').val(moment().format('YYYY-MM-DD'));

    get_data_spm();

    function get_data_spm() {
        $("#datatableSPM").DataTable({
            ajax: {
                "url": base_url + 'monitoring_spm/request/get_data_spm',
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "beforeSend": function() {
                    HoldOn.open(optionsHoldOn);
                },
                "data": function(d) {
                    return $.extend({}, d, {
                        "start_date": $("input[name='start_date']").val(),
                        "end_date": $("input[name='end_date']").val(),
                    });
                },
                "dataSrc": '',
                "complete": function(response) {
                    HoldOn.close();
                }
            },
            "ordering": false,
            "columns": [{
                data: "no_spm"
            }, {
                "render": function(data, type, full, meta) {
                    let str_nilai = "";
                    let spl_nilai = full.nilai.split(",");
                    str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

                    return str_nilai;
                }
            }, {
                "render": function(data, type, full, meta) {
                    return (full.id_trx_spm ? (
                        full.position_spm ?
                        (full.position_spm == "1" ? "<div class='badge badge-info mb-2'>Posisi : Loket Pelayanan</div><div class='badge badge-info info-penyerahan'><div>User yang menyerahkan SPM : " + full.user_penyerahan_spm_masuk + "</div><div>Tanggal Penyerahan SPM : " + full.tanggal_jam_penyerahan_spm_masuk + "</div></div>" :
                            (full.position_spm == "2" ? "<div class='badge badge-warning info-penyerahan'><div>Posisi : Bidang Belanja</div><div>Tanggal/Jam : " + full.tanggal_jam_perbendaharaan_spm_masuk + "</div></div>" :
                                (full.position_spm == "3" ? "<div class='badge badge-success mb-2'>Posisi : Selesai</div><div class='badge badge-success'>Nomor SP2D : " + (full.no_sp2d_penyelesaian ? full.no_sp2d_penyelesaian : "") + "</div>" : "")
                            )
                        ) : (full.status_spm == "3" ? "<div class='badge badge-danger info-penyerahan'><div>Posisi : Dikembalikan</div><div>Sifat Pengembalian : " + (full.jenis_pengembalian == '1' ? 'Permanent' : 'Perbaikan') + "</div><div>Catatan : " + full.catatan + "</div></div>" : "")
                    ) : "<span class='badge badge-light badge-striped badge-striped-left border-left-primary'>Berkas masih berada di dinas</span>");
                }
            }, {
                "width": "15%",
                "render": function(data, type, full, meta) {
                    return "<a href='" + base_url + "monitoring_spm/" + (full.status_up_gu == '1' ? 'edit_spm' : 'edit_spm_up_gu') + "/" + full.id_encrypt + "' class='btn btn-primary btn-icon'><i class='icon-pencil7'></i></a> <a class='btn btn-danger btn-icon' onClick=\"confirm_delete('" + full.id_encrypt + "')\" href='#'><i class='icon-trash'></i></a>";
                }
            }]
        });
    }

    // setInterval(get_data_spm, 5000);

    function confirm_delete(id_spm) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit({
            title: 'Apakah anda yakin menghapus data ini?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: base_url + 'monitoring_spm/delete_spm',
                    data: {
                        id_spm: id_spm
                    },
                    type: 'GET',
                    beforeSend: function() {
                        HoldOn.open(optionsHoldOn);
                    },
                    success: function(response) {
                        if (response) {
                            $('#datatableSPM').DataTable().ajax.reload();
                            swalInit(
                                'Berhasil',
                                'Data sudah dihapus',
                                'success'
                            );
                        } else {
                            $('#datatableSPM').DataTable().ajax.reload();
                            swalInit(
                                'Gagal',
                                'Data tidak bisa dihapus',
                                'error'
                            );
                        }
                    },
                    complete: function(response) {
                        HoldOn.close();
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swalInit(
                    'Batal',
                    'Data masih tersimpan!',
                    'error'
                ).then(function(results) {
                    HoldOn.close();
                    if (result.results) {
                        $('#datatableSPM').DataTable().ajax.reload();
                    }
                });
            }
        });
    }
</script>