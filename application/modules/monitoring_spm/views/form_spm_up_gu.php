<style>
    .user-image-custom {
        margin-bottom: 10px;
    }

    .input-group {
        flex-wrap: initial;
    }
</style>
<div class="content">
    <!-- Form inputs -->
    <div class="card">
        <div class="card-body">
            <?php echo form_open_multipart(current_url(), array('class' => 'form-validate-jquery')); ?>
            <fieldset class="mb-3">
                <legend class="text-uppercase font-size-sm font-weight-bold">Tambah SPM</legend>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nomor SPM <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" value="<?php echo !empty($content) ? $content->no_spm : ""; ?>" name="no_spm" required placeholder="Nomor SPM">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tanggal SPM <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control daterange-single" value="<?php echo !empty($content) ? $content->tanggal_spm : ""; ?>" name="tanggal_spm" placeholder="Tanggal">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Sumber Pendanaan <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <select class="form-control select-search" name="sumber_pendanaan" required>
                            <option value="">-- Pilih Sumber Pendanaan --</option>
                            <?php
                            foreach ($list_sumber_pendanaan as $key => $row) {
                                $selected = "";
                                if (!empty($content)) {
                                    if ($row->id_master_sumber_pendanaan == $content->master_sumber_pendanaan_id) {
                                        $selected = 'selected="selected"';
                                    }
                                }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo encrypt_data($row->id_master_sumber_pendanaan); ?>"><?php echo $row->nama_sumber_pendanaan; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nilai <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control input-currency" value="<?php echo !empty($content) ? $content->nilai : ""; ?>" name="nilai" required placeholder="Nilai">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Uraian <span class="text-danger">*</span></label>
                    <div class="col-lg-10">
                        <textarea placeholder="Uraian" name="uraian" class="form-control"><?php echo !empty($content) ? $content->uraian : ""; ?></textarea>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <!-- /form inputs -->

</div>

<script>
    $('.daterange-single').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'), 10)
    });

    var cleave = new Cleave('.input-currency', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand',
        numeralDecimalMark: ',',
        delimiter: '.',
    });
</script>