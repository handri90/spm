<?php

class Spm_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->table = "spm";
        $this->primary_id = "id_spm";
    }
}
