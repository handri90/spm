<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("master_satker/master_satker_model", "master_satker_model");
        $this->load->model("user_model");
        $this->load->model("trx_spm_model");
    }

    public function index()
    {
        $data['list_format'] = array("1" => "Laporan SPM Masuk", "2" => "Laporan SPM Yang Ditolak/Dikembalikan", "3" => "Laporan Penyelesaian SP2D", "4" => "Rekapitulasi SPM Masuk, Penolakan dan Penyelesaian SP2D", "5" => "Laporan Tanda Terima Penyelesaian SP2D");

        $data['list_skpd'] = $this->master_satker_model->get(
            array(
                "order_by" => array(
                    "nama" => "ASC"
                )
            )
        );

        $data['list_kbud'] = $this->user_model->get(
            array(
                "order_by" => array(
                    "nama_lengkap" => "ASC"
                ),
                "where" => array(
                    "level_user_id" => "5"
                )
            )
        );

        $data['breadcrumb'] = [['link' => false, 'content' => 'Laporan', 'is_active' => true]];
        $this->execute('index', $data);
    }

    public function cetak_laporan()
    {
        $format_laporan = $this->iget("format_laporan");
        $skpd = decrypt_data($this->iget("skpd"));
        $start_date = $this->iget("start_date");
        $end_date = $this->iget("end_date");
        $tanggal_cetak = $this->iget("tanggal_cetak");
        $kbud_user = decrypt_data($this->iget("kbud_user"));

        $user_kbud = $this->user_model->get_by($kbud_user);

        $user_pelayanan = $this->user_model->get(
            array(
                "where" => array(
                    "level_user_id" => "3"
                ),
                "limit" => "1"
            ),
            "row"
        );

        if ($skpd) {
            $data_skpd = $this->master_satker_model->get_by($skpd);
        }

        $data = array();
        $data['detail']->nama = ($skpd) ? $data_skpd->nama : "SEMUA DINAS";
        $data['detail']->tanggal = date_indo($start_date) . " s.d " . date_indo($end_date);
        $data['detail']->tanggal_cetak = date_indo(date("Y-m-d", strtotime($tanggal_cetak)));
        $data['detail']->nama_kbud = $user_kbud->nama_lengkap;
        $data['detail']->nip = $user_kbud->nip;
        $data['detail']->user_pelayanan = $user_pelayanan->nama_lengkap;

        $template = "";

        $data_trx_spm = "";
        if ($format_laporan == "1") {
            $wh_arr = array();
            if ($skpd) {
                $wh_arr = array(
                    "master_satker_id" => $skpd,
                );
            }

            $data_trx_spm = $this->trx_spm_model->get(
                array(
                    "fields" => "id_trx_spm,DATE_FORMAT(tanggal_spm_trx,'%d/%m/%Y') as tanggal_spm_trx,no_register,no_spm,nama,nilai,uraian,nama_sumber_pendanaan",
                    "join" => array(
                        "spm" => "spm_id=id_spm AND spm.deleted_at IS NULL",
                        "master_satker" => "id_master_satker=master_satker_id",
                        "master_sumber_pendanaan" => "id_master_sumber_pendanaan=master_sumber_pendanaan_id",
                    ),
                    "where" => $wh_arr,
                    "where_false" => "status_spm IN ('1','2') AND tanggal_spm_trx BETWEEN '{$start_date}' AND '{$end_date}'",
                    "order_by" => array(
                        "trx_spm.tanggal_spm_trx" => "DESC"
                    )
                )
            );

            $template = "cetak_spm_masuk";
        } else if ($format_laporan == "2") {
            $wh_arr = array();
            if ($skpd) {
                $wh_arr = array(
                    "master_satker_id" => $skpd,
                    "status_spm" => "3"
                );
            } else {
                $wh_arr = array(
                    "status_spm" => "3"
                );
            }

            $data_trx_spm = $this->trx_spm_model->get(
                array(
                    "fields" => "id_trx_spm,DATE_FORMAT(tanggal_spm_trx,'%d-%m-%Y') as tanggal_spm_trx,no_register,no_spm,nama,nilai,uraian,catatan,nama_sumber_pendanaan",
                    "join" => array(
                        "spm" => "spm_id=id_spm AND spm.deleted_at IS NULL",
                        "master_satker" => "id_master_satker=master_satker_id",
                        "master_sumber_pendanaan" => "id_master_sumber_pendanaan=master_sumber_pendanaan_id",
                    ),
                    "where" => $wh_arr,
                    "where_false" => "tanggal_spm_trx BETWEEN '{$start_date}' AND '{$end_date}'",
                    "order_by" => array(
                        "trx_spm.tanggal_spm_trx" => "DESC"
                    )
                )
            );

            $template = "cetak_spm_pengembalian";
        } else if ($format_laporan == "3") {
            $wh = "";
            if ($skpd) {
                $wh = "`master_satker_id` = '{$skpd}' AND `status_spm` = '4' AND";
            } else {
                $wh = "`status_spm` = '4' AND";
            }

            $data_trx_spm = $this->trx_spm_model->query("
                SELECT `id_trx_spm`, DATE_FORMAT(trx_spm.tanggal_spm_trx, '%d-%m-%Y') AS tanggal_spm_trx, trx_spm.`no_register`, `no_spm`, `nama`, `nilai`, `uraian`, `no_sp2d_penyelesaian`,a.no_register AS nomor_berkas,DATE_FORMAT(trx_spm.tanggal_spm_trx, '%d-%m-%Y') AS tanggal_spm_trx_range,DATE_FORMAT(trx_spm.tanggal_jam_perbendaharaan_penyelesaian_sp2d, '%d-%m-%Y') AS tanggal_jam_perbendaharaan_penyelesaian_sp2d,DATE_FORMAT(a.tanggal_spm_trx,'%d-%m-%Y') as tanggal_berkas,DATE_FORMAT(a.tanggal_jam_penyerahan_spm_masuk,'%d-%m-%Y') as tanggal_jam_penyerahan_spm_masuk,DATE_FORMAT(tanggal_spm, '%d-%m-%Y') AS tanggal_spm,nama_sumber_pendanaan
                FROM `trx_spm` 
                JOIN `spm` ON `spm_id`=`id_spm` AND `spm`.`deleted_at` IS NULL 
                JOIN `master_satker` ON `id_master_satker`=`master_satker_id`
                JOIN `master_sumber_pendanaan` ON `id_master_sumber_pendanaan`=`master_sumber_pendanaan_id` 
                LEFT JOIN (
                    SELECT no_register,spm_id,tanggal_spm_trx,tanggal_jam_penyerahan_spm_masuk
                    FROM trx_spm
                    WHERE status_spm IN ('1','2') AND trx_spm.deleted_at IS NULL
                ) AS a ON a.spm_id=id_spm
                WHERE {$wh} trx_spm.tanggal_spm_trx BETWEEN '{$start_date}' AND '{$end_date}' AND `trx_spm`.`deleted_at` IS NULL 
                ORDER BY `trx_spm`.`tanggal_spm_trx` DESC
                ")->result();

            $template = "cetak_sp2d_penyelesaian";
        } else if ($format_laporan == "4") {
            $wh_arr = array();
            if ($skpd) {
                $wh_arr = array(
                    "master_satker_id" => $skpd,
                );
            }

            $data_trx_spm = $this->trx_spm_model->query("
            SELECT IFNULL(COUNT(*),0) AS jumlah_spm_masuk
            FROM trx_spm
            WHERE trx_spm.deleted_at IS NULL AND status_spm IN ('1','2') AND tanggal_spm_trx BETWEEN '{$start_date}' AND '{$end_date}'
            UNION ALL
            SELECT IFNULL(COUNT(*),0) AS jumlah_spm_masuk
            FROM trx_spm
            WHERE trx_spm.deleted_at IS NULL AND (status_spm = '3' AND jenis_pengembalian NOT IN ('3')) AND tanggal_spm_trx BETWEEN '{$start_date}' AND '{$end_date}'
            UNION ALL
            SELECT IFNULL(COUNT(*),0) AS jumlah_spm_masuk
            FROM trx_spm
            WHERE trx_spm.deleted_at IS NULL AND status_spm IN ('4') AND tanggal_spm_trx BETWEEN '{$start_date}' AND '{$end_date}'
            ")->result();

            $template = "cetak_spm_rekap";
        } else if ($format_laporan == "5") {
            $wh = "";
            if ($skpd) {
                $wh = "`master_satker_id` = '{$skpd}' AND `status_spm` = '4' AND";
            } else {
                $wh = "`status_spm` = '4' AND";
            }

            $data_trx_spm = $this->trx_spm_model->query("
                SELECT `id_trx_spm`, DATE_FORMAT(trx_spm.tanggal_spm_trx, '%d-%m-%Y') AS tanggal_spm_trx, trx_spm.`no_register`, `no_spm`, `nama`, `nilai`, `uraian`, `no_sp2d_penyelesaian`,a.no_register AS nomor_berkas,DATE_FORMAT(trx_spm.tanggal_spm_trx, '%d-%m-%Y') AS tanggal_spm_trx_range,DATE_FORMAT(a.tanggal_spm_trx,'%d-%m-%Y') as tanggal_berkas,nama_sumber_pendanaan,DATE_FORMAT(trx_spm.tanggal_jam_perbendaharaan_penyelesaian_sp2d, '%d-%m-%Y') AS tanggal_jam_perbendaharaan_penyelesaian_sp2d
                FROM `trx_spm` 
                JOIN `spm` ON `spm_id`=`id_spm` AND `spm`.`deleted_at` IS NULL 
                JOIN `master_satker` ON `id_master_satker`=`master_satker_id`
                JOIN `master_sumber_pendanaan` ON `id_master_sumber_pendanaan`=`master_sumber_pendanaan_id` 
                LEFT JOIN (
                    SELECT no_register,spm_id,tanggal_spm_trx
                    FROM trx_spm
                    WHERE status_spm IN ('1','2') AND trx_spm.deleted_at IS NULL
                ) AS a ON a.spm_id=id_spm
                WHERE {$wh} trx_spm.tanggal_spm_trx BETWEEN '{$start_date}' AND '{$end_date}' AND `trx_spm`.`deleted_at` IS NULL 
                ORDER BY `trx_spm`.`tanggal_spm_trx` DESC
                ")->result();

            $template = "cetak_tanda_terima_sp2d_penyelesaian";
        }

        $data['content'] = $data_trx_spm;

        $mpdf = new \Mpdf\Mpdf(['format' => 'Legal', 'orientation' => 'L','tempDir' => '/tmp']);
        $data = $this->load->view($template, $data, TRUE);
        $mpdf->WriteHTML($data);
        $mpdf->Output();
    }
}
