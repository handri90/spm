<html>

<head>
    <style>
        body {
            font-family: Arial;
            font-size: 10pt;
            font-weight: normal;
        }

        .wd-number {
            width: 30px;
        }

        .wd-label {
            width: 120px;
        }

        .wd-content {
            width: 220px;
        }

        table {
            margin: 10px 0;
        }

        .tbl-sign {
            margin-top: 20px;
            text-align: center;
        }

        h4 {
            line-height: 0.4;
        }

        .head_print {
            text-decoration: underline;
        }

        .tbl-border {
            border: 0.3px solid #000;
            font-weight: 100;
            font-size: 10pt;
        }

        .tbl-content {
            border-collapse: collapse;
        }

        .tbl-border.cstm-width {
            width: 140px;
        }

        .tbl-border.cstm-width-no {
            width: 30px;
        }

        .td-center {
            text-align: center;
        }

        .td-right {
            text-align: right;
        }

        .td-justify {
            text-align: justify;
            padding: 0 5px;
        }

        .cstm-width-text {
            width: 800px;
        }
    </style>
</head>

<body>
    <h4>PEMERINTAH KABUPATEN KOTAWARINGIN BARAT</h4>
    <h4>BADAN PENGELOLA KEUANGAN DAN ASET DAERAH</h4>
    <h4 class="head_print">REKAPITULASI SPM MASUK, PENOLAKAN, DAN PENYELESAIAN SP2D</h4>
    <table class="detail">
        <tr>
            <td class="wd-label">SKPD</td>
            <td class="wd-number">:</td>
            <td><?php echo isset($detail) ? $detail->nama : ""; ?></td>
        </tr>
        <tr>
            <td>TANGGAL</td>
            <td>:</td>
            <td><?php echo isset($detail) ? $detail->tanggal : ""; ?></td>
        </tr>
    </table>
    <table class="tbl-content" style="width:100%;">
        <tr>
            <th class="tbl-border cstm-width">SPM MASUK</th>
            <th class="tbl-border cstm-width">DITOLAK / DIKEMBALIKAN</th>
            <th class="tbl-border cstm-width">DIPROSES KE SP2D</th>
            <th class="tbl-border cstm-width">SISA (BELUM DIPROSES)</th>
        </tr>
        <tr>
            <td class="tbl-border td-center"><?php echo $content[0]->jumlah_spm_masuk; ?></td>
            <td class="tbl-border td-center"><?php echo $content[1]->jumlah_spm_masuk; ?></td>
            <td class="tbl-border td-center"><?php echo $content[2]->jumlah_spm_masuk; ?></td>
            <td class="tbl-border td-center"><?php echo (int)$content[0]->jumlah_spm_masuk - ((int)$content[1]->jumlah_spm_masuk + (int)$content[2]->jumlah_spm_masuk); ?></td>
        </tr>
    </table>
    <table class="tbl-sign" style="width:100%">
        <tr>
            <td>&nbsp;</td>
            <td style="width:600px"></td>
            <td>Pangkalan Bun, <?php echo isset($detail) ? $detail->tanggal_cetak : ""; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td>Kuasa Umum Bendahara Umum Daerah</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>(KBUD)</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="padding-top:100px;font-weight:bold;text-decoration:underline;text-transform:uppercase;"><?php echo isset($detail) ? $detail->nama_kbud : ""; ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>NIP. <?php echo isset($detail) ? $detail->nip : ""; ?></td>
        </tr>
    </table>
</body>

</html>