<html>

<head>
    <style>
        body {
            font-family: Arial;
            font-size: 10pt;
            font-weight: normal;
        }

        .wd-number {
            width: 30px;
        }

        .wd-label {
            width: 120px;
        }

        .wd-content {
            width: 220px;
        }

        table {
            margin: 10px 0;
        }

        .tbl-sign {
            margin-top: 20px;
            text-align: center;
        }

        h4 {
            line-height: 0.4;
        }

        .head_print {
            text-decoration: underline;
        }

        .tbl-border {
            border: 0.3px solid #000;
            font-weight: 100;
            font-size: 10pt;
        }

        .tbl-content {
            border-collapse: collapse;
        }

        .tbl-border.cstm-width {
            width: 140px;
        }

        .tbl-border.cstm-width-no {
            width: 30px;
        }

        .td-center {
            text-align: center;
        }

        .td-right {
            text-align: right;
        }

        .td-justify {
            text-align: justify;
            padding: 0 5px;
        }

        .cstm-width-text {
            width: 800px;
        }

        .nama_kbud {
            font-weight: 600 !important;
            text-decoration: underline;
            text-transform: uppercase;
        }
    </style>
</head>

<body>
    <h4>PEMERINTAH KABUPATEN KOTAWARINGIN BARAT</h4>
    <h4>BADAN PENGELOLA KEUANGAN DAN ASET DAERAH</h4>
    <h4 class="head_print">LAPORAN SPM MASUK</h4>
    <table class="detail">
        <tr>
            <td class="wd-label">SKPD</td>
            <td class="wd-number">:</td>
            <td><?php echo isset($detail) ? $detail->nama : ""; ?></td>
        </tr>
        <tr>
            <td>TANGGAL</td>
            <td>:</td>
            <td><?php echo isset($detail) ? $detail->tanggal : ""; ?></td>
        </tr>
    </table>
    <table class="tbl-content" style="width:100%;">
        <tr>
            <th class="tbl-border cstm-width-no" rowspan="2">NO</th>
            <th class="tbl-border cstm-width" rowspan="2">TANGGAL</th>
            <th class="tbl-border cstm-width" rowspan="2">NOMOR</th>
            <th class="tbl-border" colspan="4">SPM</th>
        </tr>
        <tr>
            <th class="tbl-border cstm-width">NOMOR</th>
            <th class="tbl-border cstm-width">NILAI</th>
            <th class="tbl-border cstm-width">SUMBER DANA</th>
            <th class="tbl-border cstm-width-text">URAIAN</th>
        </tr>
        <?php
        if ($content) {
            $no = 1;
            foreach ($content as $key => $value) {
        ?>
                <tr>
                    <td class="tbl-border td-center"><?php echo $no; ?></td>
                    <td class="tbl-border td-center"><?php echo $value->tanggal_spm_trx; ?></td>
                    <td class="tbl-border td-center"><?php echo $value->no_register; ?></td>
                    <td class="tbl-border td-center"><?php echo $value->no_spm; ?></td>
                    <td class="tbl-border td-right">
                        <?php
                        $str_nilai = "";
                        $spl_nilai = explode(",", $value->nilai);
                        $str_nilai = nominal($spl_nilai[0]) . ($spl_nilai[1] ? "," . $spl_nilai[1] : "");
                        echo $str_nilai;
                        ?>
                    </td>
                    <td class="tbl-border td-center"><?php echo $value->nama_sumber_pendanaan; ?></td>
                    <td class="tbl-border td-justify"><?php echo $value->uraian; ?></td>
                </tr>
        <?php
                $no++;
            }
        }
        ?>
    </table>
    <table class="tbl-sign" style="width:100%">
        <tr>
            <td>&nbsp;</td>
            <td style="width:600px"></td>
            <td>Pangkalan Bun, <?php echo isset($detail) ? $detail->tanggal_cetak : ""; ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td>Kuasa Umum Bendahara Umum Daerah</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>(KBUD)</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="padding-top:100px;"><span class="nama_kbud"><?php echo isset($detail) ? $detail->nama_kbud : ""; ?></span></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>NIP. <?php echo isset($detail) ? $detail->nip : ""; ?></td>
        </tr>
    </table>
</body>

</html>