<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">


        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">


                <?php echo $sidebar; ?>

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->

<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="<?php echo base_url(); ?>dashboard" class="breadcrumb-item">Dashboard</a>
                    <?php
                    $str = "";
                    foreach ($breadcrumb as $key => $row) {
                        $active = "";
                        if ($row['is_active']) {
                            $active = "active";
                        }

                        if ($row['link']) {
                            $str .= "<a href='" . $row['url'] . "' class='breadcrumb-item " . $active . "'>" . $row['content'] . "</a>";
                        } else {
                            $str .= "<span class='breadcrumb-item " . $active . "'>" . $row['content'] . "</span>";
                        }
                    ?>
                    <?php
                    }
                    echo $str;
                    ?>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>
    </div>
    <!-- /page header -->