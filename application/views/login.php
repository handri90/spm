<!-- ======= Hero Section ======= -->
<section id="hero" class="hero d-flex align-items-center">

	<div class="container">
		<div class="row" style="margin-top:60px">
			<div class="col-lg-6 d-flex flex-column justify-content-center">
				<h1 data-aos="fade-up">Sistem Informasi Pelayanan Perbendaharaan Kotawaringin Barat</h1>
			</div>
			<div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
				<img src="<?php echo base_url(); ?>assets/template_portal/img/hero-img.svg" class="img-fluid" alt="">
			</div>
		</div>
		<div class="row" style="margin-top:100px;">
			<div class="col-lg-12 d-flex flex-column text-center">
				<h1 class="custom-title" data-aos="fade-up">SIMANIS KOBAR</h1>
				<h1 class="custom" data-aos="fade-up">SISTEM INFORMASI TERINTEGRASI SP2D ONLINE</h1>
				<h1 class="custom" data-aos="fade-up">PEMERINTAH KABUPATEN KOTAWARINGIN BARAT</h1>
			</div>
		</div>
	</div>

</section><!-- End Hero -->