<!-- ======= Hero Section ======= -->
<style>
    .inner-page {
        margin-top: 120px;
        padding: 0;
    }

    .footer-custom {
        height: 40vh;
    }

    .form input[type="text"] {
        border: 0;
        padding: 8px;
        width: calc(100% - 140px);
        margin-right: 90px;
    }

    .form input[type="submit"] {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        border: 0;
        background: none;
        font-size: 16px;
        padding: 0 30px;
        margin: 3px;
        background: #4154f1;
        color: #fff;
        transition: 0.3s;
        border-radius: 4px;
    }

    .form {
        margin-top: 20px;
        background: #fff;
        padding: 16px 10px;
        position: relative;
        border-radius: 4px;
        border: 1px solid #e1ecff;
    }

    .box-tracking {
        margin-top: 30px;
        margin-left: auto;
        margin-right: auto;
        width: 80%;
        padding: 30px;
        box-shadow: 0 4px 16px rgb(0 0 0 / 10%);
    }

    .bg-header {
        background-color: #00bce4;
    }
</style>
<section class="inner-page">


    <div id="footer" class="footer footer-custom">

        <div class="footer-newsletter">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 text-center">
                        <h4>Tracking SPM</h4>
                    </div>
                    <div class="col-lg-8 mt-4">
                        <span class="form">
                            <input type="text" name="no_tracking"><input onclick="tracking_spm()" type="submit" value="Cari">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-tracking">

            <div class="card card-table table-responsive shadow-0 mb-0">
                <table class="table">
                    <tbody>
                        <tr class="bg-header text-white">
                            <th colspan="3">RINCIAN SPM</th>
                        </tr>
                        <tr>
                            <th style="width:180px;">Tanggal</th>
                            <td style="width:40px;">:</td>
                            <td><span class="tanggal_rincian_spm"></span></td>
                        </tr>
                        <tr>
                            <th>Nomor</th>
                            <td>:</td>
                            <td><span class="nomor_rincian_spm"></span></td>
                        </tr>
                        <tr>
                            <th>Kegiatan</th>
                            <td>:</td>
                            <td><span class="kegiatan_rincian_spm"></span></td>
                        </tr>
                        <tr>
                            <th>Nilai</th>
                            <td>:</td>
                            <td><span class="nilai_rincian_spm"></span></td>
                        </tr>
                        <tr>
                            <th>Sumber Dana</th>
                            <td>:</td>
                            <td><span class="sumber_dana_rincian_spm"></span></td>
                        </tr>
                        <tr>
                            <th>Uraian</th>
                            <td>:</td>
                            <td><span class="uraian_rincian_spm"></span></td>
                        </tr>
                    </tbody>
                </table>

                <table class="table is_n_transaksi">
                    <tbody>
                        <tr class="bg-header text-white text-center">
                            <th colspan="3">Berkas Masih Berada Di Dinas Bersangkutan</th>
                        </tr>
                    </tbody>
                </table>

                <div class="is_transaksi">
                    <table class="table">
                        <tbody>
                            <tr class="bg-header text-white">
                                <th colspan="3">REGISTER MASUK</th>
                            </tr>
                            <tr>
                                <th style="width:180px;">Tanggal</th>
                                <td style="width:40px;">:</td>
                                <td><span class="tanggal_register_masuk"></span></td>
                            </tr>
                            <tr>
                                <th>Jam</th>
                                <td>:</td>
                                <td><span class="jam_register_masuk"></span></td>
                            </tr>
                            <tr>
                                <th>Nomor</th>
                                <td>:</td>
                                <td><span class="nomor_register_masuk"></span></td>
                            </tr>
                            <tr>
                                <th>Catatan</th>
                                <td>:</td>
                                <td><span class="catatan_register_masuk"></span></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table">
                        <tbody>
                            <tr class="bg-header text-white">
                                <th colspan="3">Bidang Perbendaharaan</th>
                            </tr>
                            <tr>
                                <th style="width:180px;">Tanggal</th>
                                <td style="width:40px;">:</td>
                                <td><span class="tanggal_bidang_perbend"></span></td>
                            </tr>
                            <tr>
                                <th>Jam</th>
                                <td>:</td>
                                <td><span class="jam_bidang_perbend"></span></td>
                            </tr>
                            <tr>
                                <th>Nama</th>
                                <td>:</td>
                                <td><span class="nama_bidang_perbend"></span></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table">
                        <tbody>
                            <tr class="bg-header text-white">
                                <th colspan="3">Penyelesaian SP2D</th>
                            </tr>
                            <tr>
                                <th style="width:180px;">Tanggal</th>
                                <td style="width:40px;">:</td>
                                <td><span class="tanggal_penyelesaian_sp2d"></span></td>
                            </tr>
                            <tr>
                                <th>Jam</th>
                                <td>:</td>
                                <td><span class="jam_penyelesaian_sp2d"></span></td>
                            </tr>
                            <tr>
                                <th>Nomor</th>
                                <td>:</td>
                                <td><span class="nomor_penyelesaian_sp2d"></span></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table">
                        <tbody>
                            <tr class="bg-header text-white">
                                <th colspan="3">Status</th>
                            </tr>
                            <tr>
                                <th style="width:180px;">Ditolak</th>
                                <td style="width:40px;">:</td>
                                <td><span class="status_ditolak"></span></td>
                            </tr>
                            <tr>
                                <th>Dalam Proses</th>
                                <td>:</td>
                                <td><span class="status_dalam_proses"></span></td>
                            </tr>
                            <tr>
                                <th>Selesai</th>
                                <td>:</td>
                                <td><span class="status_selesai"></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</section>