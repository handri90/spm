<!-- ======= Footer ======= -->
<footer id="footer" class="footer">

	<div class="footer-top">
		<div class="container">
			<div class="row gy-4">
				<div class="col-lg-5 col-md-12 footer-info">
					<a href="index.html" class="logo d-flex align-items-center">
						<span>SIMPELBEN</span>
					</a>
					<p>Situs ini dibuat dan dikelola oleh Dinas Komunikasi Informatika Statistik dan Persandian Kabupaten Kotawaringin Barat.</p>
				</div>

				<div class="col-lg-4 col-6 footer-links">
					<h4>Halaman Terkait</h4>
					<ul>
						<li><i class="bi bi-chevron-right"></i> <a target="_blank" href="http://portal.kotawaringinbaratkab.go.id/">Kabupaten Kotawaringin Barat</a></li>
						<li><i class="bi bi-chevron-right"></i> <a target="_blank" href="https://mmc.kotawaringinbaratkab.go.id/">MMC Kotawaringin Barat</a></li>
					</ul>
				</div>

				<div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
					<h4>Alamat</h4>
					<p>
						Jalan Sutan Syahrir No. 62 <br>
						Arut Selatan, 74112<br>
						Kotawaringin Barat <br><br>
					</p>

				</div>

			</div>
		</div>
	</div>

	<div class="container">
		<div class="copyright">
			Copyright @ 2021 <strong><span>Diskominfo Kotawaringin Barat</span></strong>
		</div>
	</div>
</footer><!-- End Footer -->

<script src="<?php echo base_url(); ?>assets/template_portal/vendor/jquery/jquery-3.5.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template_portal/vendor/bootstrap4/js/bootstrap.min.js"></script>
<!-- Vendor JS Files -->
<script src="<?php echo base_url(); ?>assets/template_portal/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<script src="<?php echo base_url(); ?>assets/template_portal/vendor/aos/aos.js"></script>

<!-- Template Main JS File -->
<script src="<?php echo base_url(); ?>assets/template_portal/js/main.js"></script>
<script src="<?php echo base_url(); ?>assets/template_admin/js/currency.min.js"></script>
<script src="<?php echo base_url(); ?>assets/template_admin/js/HoldOn.min.js"></script>

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<div id="myModal" class="modal fade">
	<div class="modal-dialog modal-login">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Login</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<span class='error_field'></span>
				<div class="form-group">
					<label>Tahun</label>
					<select name="tahun" class="form-control">
						<?php
						foreach ($tahun as $key => $value) {
							$selected = "";
							if ($value->tahun == date("Y")) {
								$selected = "selected";
							}
						?>
							<option <?php echo $selected; ?> value="<?php echo encrypt_data($value->id_tahun_anggaran); ?>"><?php echo $value->tahun ?></option>
						<?php
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label>Username</label>
					<input type="text" class="form-control" required="required" name="username">
				</div>
				<div class="form-group">
					<div class="clearfix">
						<label>Password</label>
					</div>

					<input type="password" class="form-control" required="required" name="password">
				</div>
			</div>
			<div class="modal-footer justify-content-between">
				<input type="submit" class="btn btn-primary" value="Login" onclick="check_auth()">
			</div>
		</div>
	</div>
</div>

</body>

</html>

<script>
	let optionsHoldOn = {
		theme: "sk-cube-grid",
		message: 'Loading',
		textColor: "white"
	};

	function check_auth() {
		let username = $("input[name='username']").val();
		let password = $("input[name='password']").val();
		let tahun = $("select[name='tahun']").val();
		$(".error_field").html("");

		if (!username) {
			$(".error_field").html("<div class='alert alert-danger'>Field username tidak boleh kosong</div>");
		} else if (!password) {
			$(".error_field").html("<div class='alert alert-danger'>Field Password tidak boleh kosong</div>");
		} else {
			$.ajax({
				url: base_url + 'login/act_login',
				data: {
					username: username,
					password: password,
					tahun: tahun,
				},
				type: 'POST',
				beforeSend: function() {
					HoldOn.open(optionsHoldOn);
				},
				success: function(response) {
					if (response == true) {
						$(".error_field").html("<div class='alert alert-success'>Berhasil...</div>");
						location.href = base_url;
					} else {
						$(".error_field").html("<div class='alert alert-danger'>Username dan Password salah</div>");
					}
				},
				complete: function() {
					HoldOn.close();
				}
			});
		}
	}

	$(function() {
		$('#myModal').on('keypress', function(event) {
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if (keycode == '13') {
				check_auth();
			}
		});

		$(".box-tracking").hide();
		$(".tanggal_rincian_spm").html("");
		$(".nomor_rincian_spm").html("");
		$(".kegiatan_rincian_spm").html("");
		$(".nilai_rincian_spm").html("");
		$(".sumber_dana_rincian_spm").html("");
		$(".uraian_rincian_spm").html("");
		$(".tanggal_register_masuk").html("");
		$(".jam_register_masuk").html("");
		$(".nomor_register_masuk").html("");
		$(".catatan_register_masuk").html("");
		$(".tanggal_bidang_perbend").html("");
		$(".jam_bidang_perbend").html("");
		$(".nama_bidang_perbend").html("");
		$(".tanggal_penyelesaian_sp2d").html("");
		$(".jam_penyelesaian_sp2d").html("");
		$(".nomor_penyelesaian_sp2d").html("");
		$(".status_dalam_proses").html("");
		$(".status_ditolak").html("");
		$(".status_selesai").html("");

		$('input[name="no_tracking"]').on('keypress', function(event) {
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if (keycode == '13') {
				tracking_spm();
			}
		});
	})

	const IDR = value => currency(
		value, {
			symbol: "Rp. ",
			precision: 0,
			separator: "."
		});

	function tracking_spm() {
		$(".tanggal_rincian_spm").html("");
		$(".nomor_rincian_spm").html("");
		$(".kegiatan_rincian_spm").html("");
		$(".nilai_rincian_spm").html("");
		$(".sumber_dana_rincian_spm").html("");
		$(".uraian_rincian_spm").html("");
		$(".tanggal_register_masuk").html("");
		$(".jam_register_masuk").html("");
		$(".nomor_register_masuk").html("");
		$(".catatan_register_masuk").html("");
		$(".tanggal_bidang_perbend").html("");
		$(".jam_bidang_perbend").html("");
		$(".nama_bidang_perbend").html("");
		$(".tanggal_penyelesaian_sp2d").html("");
		$(".jam_penyelesaian_sp2d").html("");
		$(".nomor_penyelesaian_sp2d").html("");
		$(".status_dalam_proses").html("");
		$(".status_ditolak").html("");
		$(".status_selesai").html("");

		let no_tracking = $("input[name='no_tracking']").val();

		if (no_tracking) {
			$.ajax({
				url: base_url + 'tracking_spm/tracking_spm_check',
				data: {
					no_tracking: no_tracking
				},
				type: 'GET',
				beforeSend: function() {
					HoldOn.open(optionsHoldOn);
				},
				success: function(response) {
					if (response) {
						$(".footer-custom").css("height", "auto");
						$(".box-tracking").show();
						if (response.tanggal_spm_register_masuk) {
							$(".is_n_transaksi").hide();
							$(".is_transaksi").show();
						} else {
							$(".is_n_transaksi").show();
							$(".is_transaksi").hide();
						}

						let str_nilai = "";
						let spl_nilai = response.nilai.split(",");
						str_nilai = IDR(spl_nilai[0]).format(true) + (spl_nilai[1] ? "," + spl_nilai[1] : "");

						$(".tanggal_rincian_spm").html(response.tanggal_spm);
						$(".nomor_rincian_spm").html(response.no_spm);
						$(".kegiatan_rincian_spm").html(response.nama_kegiatan);
						$(".nilai_rincian_spm").html(str_nilai);
						$(".sumber_dana_rincian_spm").html(response.nama_sumber_pendanaan);
						$(".uraian_rincian_spm").html(response.uraian);
						$(".tanggal_register_masuk").html(response.tanggal_spm_register_masuk);
						$(".jam_register_masuk").html(response.jam_spm_register_masuk);
						$(".nomor_register_masuk").html(response.no_register_masuk);
						$(".catatan_register_masuk").html(response.catatan);
						$(".tanggal_bidang_perbend").html(response.tanggal_spm_perbendaharaan);
						$(".jam_bidang_perbend").html(response.jam_spm_perbendaharaan);
						$(".nama_bidang_perbend").html((response.user_perbendaharaan_spm_masuk == "1" ? "Subbid 1" : ((response.user_perbendaharaan_spm_masuk == "2" ? "Subbid 2" : ""))));
						$(".tanggal_penyelesaian_sp2d").html(response.tanggal_spm_penyelesaian);
						$(".jam_penyelesaian_sp2d").html(response.jam_spm_penyelesaian);
						$(".nomor_penyelesaian_sp2d").html(response.no_sp2d_penyelesaian);

						if (response.status_spm == "1" || response.status_spm == "2" || (response.status_spm == "3" && response.jenis_pengembalian == "3")) {
							$(".status_dalam_proses").html("<span class='badge badge-info ft-cstm'><i class='bi bi-check'></i></span> " + (response.status_spm == "1" ? "<span class='badge badge-info'>Berkas berada di loket pelayanan BPKAD</span>" : (response.status_spm == "2" ? "<span class='badge badge-info'>Berkas berada di bidang belanja BPKAD</span>" : (response.status_spm == "3" && response.jenis_pengembalian == "3" ? "<span class='badge badge-info'>Berkas Sudah Diperbaiki dan Berada di bidang belanja BPKAD</span><div class='row mt-2'><div class='col-12'><span class='badge badge-light badge-striped badge-striped-left border-left-danger'>Jenis Pengembalian : Perbaikan</span></div><div class='col-12'><span class='badge badge-light badge-striped badge-striped-left border-left-danger'>Tanggal Ditolak : " + response.tanggal_ditolak_custom + " " + response.jam_ditolak + "</span></div>" + (response.jenis_pengembalian == '3' ? (response.tanggal_perbaikan_diterima ? "<div class='col-12'><span class='badge badge-light badge-striped badge-striped-left border-left-success'>Tanggal Perbaikan Diterima : " + response.tanggal_perbaikan_diterima + " " + response.jam_perbaikan_diterima + "</span></div>" : "") : "") + "</div>" : ""))));
						} else if (response.status_spm == "3" && response.jenis_pengembalian == "1") {
							$(".status_ditolak").html("<span class='badge badge-danger ft-cstm'><i class='bi bi-check'></i></span><div class='row mt-2'><div class='col-12'><span class='badge badge-light badge-striped badge-striped-left border-left-danger'>Jenis Pengembalian : Permanen</span></div><div class='col-12'><span class='badge badge-light badge-striped badge-striped-left border-left-danger'>Tanggal Ditolak : " + response.tanggal_ditolak_custom + " " + response.jam_ditolak + "</span></div>" + "</div>");
						} else if (response.status_spm == "3" && response.jenis_pengembalian == "2") {
							$(".status_ditolak").html("<span class='badge badge-danger ft-cstm'><i class='bi bi-check'></i></span><div class='row mt-2'><div class='col-12'><span class='badge badge-light badge-striped badge-striped-left border-left-danger'>Jenis Pengembalian : Perbaikan</span></div><div class='col-12'><span class='badge badge-light badge-striped badge-striped-left border-left-danger'>Tanggal Ditolak : " + response.tanggal_ditolak_custom + " " + response.jam_ditolak + "</span></div>" + (response.jenis_pengembalian == '3' ? (response.tanggal_perbaikan_diterima ? "<div class='col-12'><span class='badge badge-light badge-striped badge-striped-left border-left-success'>Tanggal Perbaikan Diterima : " + response.tanggal_perbaikan_diterima + " " + response.jam_perbaikan_diterima + "</span></div>" : "") : "") + "</div>");
						} else if (response.status_spm == "4") {
							$(".status_selesai").html("<span class='badge badge-success ft-cstm'><i class='bi bi-check'></i></span>");
						}
					} else {
						$(".tanggal_rincian_spm").html("");
						$(".nomor_rincian_spm").html("");
						$(".kegiatan_rincian_spm").html("");
						$(".nilai_rincian_spm").html("");
						$(".sumber_dana_rincian_spm").html("");
						$(".uraian_rincian_spm").html("");
						$(".tanggal_register_masuk").html("");
						$(".jam_register_masuk").html("");
						$(".nomor_register_masuk").html("");
						$(".catatan_register_masuk").html("");
						$(".tanggal_bidang_perbend").html("");
						$(".jam_bidang_perbend").html("");
						$(".nama_bidang_perbend").html("");
						$(".tanggal_penyelesaian_sp2d").html("");
						$(".jam_penyelesaian_sp2d").html("");
						$(".nomor_penyelesaian_sp2d").html("");
						$(".status_dalam_proses").html("");
						$(".status_ditolak").html("");
						$(".status_selesai").html("");
						$(".box-tracking").hide();
						$(".footer-custom").css("height", "40vh");
					}
				},
				complete: function() {
					HoldOn.close();
				}
			});
		} else {
			$(".tanggal_rincian_spm").html("");
			$(".nomor_rincian_spm").html("");
			$(".kegiatan_rincian_spm").html("");
			$(".nilai_rincian_spm").html("");
			$(".sumber_dana_rincian_spm").html("");
			$(".uraian_rincian_spm").html("");
			$(".tanggal_register_masuk").html("");
			$(".jam_register_masuk").html("");
			$(".nomor_register_masuk").html("");
			$(".catatan_register_masuk").html("");
			$(".tanggal_bidang_perbend").html("");
			$(".jam_bidang_perbend").html("");
			$(".nama_bidang_perbend").html("");
			$(".tanggal_penyelesaian_sp2d").html("");
			$(".jam_penyelesaian_sp2d").html("");
			$(".nomor_penyelesaian_sp2d").html("");
			$(".status_dalam_proses").html("");
			$(".status_ditolak").html("");
			$(".status_selesai").html("");
			$(".box-tracking").hide();
			$(".footer-custom").css("height", "40vh");
		}
	}
</script>