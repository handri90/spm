<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>SIMPELBEN - Kotawaringin Barat</title>
	<meta content="" name="description">

	<meta content="" name="keywords">

	<!-- Favicons -->
	<link href="<?php echo base_url(); ?>assets/template_portal/img/favicon.png" rel="icon">

	<!-- Google Fonts -->
	<link href="<?php echo base_url(); ?>assets/template_portal/css/font-family.css" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template_portal/vendor/bootstrap4/css/bootstrap.min.css">
	<link href="<?php echo base_url(); ?>assets/template_portal/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/template_portal/vendor/aos/aos.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/template_admin/css/HoldOn.min.css" rel="stylesheet" />

	<!-- Template Main CSS File -->
	<link href="<?php echo base_url(); ?>assets/template_portal/css/style.css" rel="stylesheet">
	<script>
		var base_url = "<?php echo base_url(); ?>";
	</script>
</head>

<body data-bs-spy="scroll" data-bs-target="#navbar" data-bs-offset="100">

	<!-- ======= Header ======= -->
	<header id="header" class="header fixed-top">
		<div class="container-fluid container-xl d-flex align-items-center justify-content-between">

			<a href="index.html" class="logo d-flex align-items-center">
				<span>SIMPELBEN</span>
			</a>

			<nav id="navbar" class="navbar">
				<ul>
					<li><a class="nav-link scrollto" href="<?php echo base_url(); ?>tracking_spm">Tracking SPM</a></li>
					<li><a class="getstarted scrollto" href="#myModal" data-toggle="modal">Login</a></li>
				</ul>
				<i class="bi bi-list mobile-nav-toggle"></i>
			</nav><!-- .navbar -->

		</div>
	</header><!-- End Header -->